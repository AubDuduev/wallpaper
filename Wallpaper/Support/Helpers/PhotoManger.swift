
import UIKit
import Photos

class PhotoManger: NSObject {
  
  static let shared = PhotoManger()

  public var completionSavePhoto: Clousure<Bool>!
  public var completionSaveVideo: Clousure<Bool>!
  
  public func save(image: UIImage){
    if PHPhotoLibrary.authorizationStatus() != .authorized {
      
      PHPhotoLibrary.requestAuthorization({ (statueResponce) in
        
        switch statueResponce {
          case .authorized :
            print("authorized")
          default:
            break
        }
      })
    } else {
      let selector = #selector(completion)
      UIImageWriteToSavedPhotosAlbum(image, self, selector, nil)
    }
  }
  @objc
  private func completion(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer){
    //Ошибка сохронения фото
    if let error = error {
      AlertEK.customText(title  : .error,
                         message: .errorSaveWalppaer,
                         textEnd: error.localizedDescription){ [weak self] in
        guard let self = self else { return }
        self.completionSavePhoto?(false)
      }
     //Успешное сохронение фото
    } else {
      self.completionSavePhoto?(true)
    }
  }
  public func saveVideo(urlString: String){
    DispatchQueue.global(qos: .background).async { [weak self] in
      if let url = URL(string: urlString),
         let urlData = NSData(contentsOf: url) {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
        let filePath="\(documentsPath)/tempFile.mp4"
        DispatchQueue.main.async {
          urlData.write(toFile: filePath, atomically: true)
          PHPhotoLibrary.shared().performChanges({
            PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
          }) { [weak self] (completed, error) in
            if completed {
              DispatchQueue.main.async { [weak self] in
                self?.completionSaveVideo?(true)
              }
            }
          }
        }
      }
    }
  }
}
