import UIKit

class LifeCycleApp {
  
  private var isBackGraund = false
  
  func applicationDidEnterBackground(_ application: UIApplication) {
    self.isBackGraund = true
  }
  func applicationDidBecomeActive(_ application: UIApplication) {
    guard isBackGraund else { return }
    let gladSeeVC = UIStoryboard.createVCID(sbID: .GladSee, vcID: .GladSeeVC)
    if let navBarVC = application.windows.first?.rootViewController?.presentedViewController as? NavBarViewController {
      navBarVC.modalTransitionStyle   = .crossDissolve
      navBarVC.modalPresentationStyle = .formSheet
      navBarVC.present(gladSeeVC, animated: true)
    }
  }
}
