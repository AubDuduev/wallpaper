
import UIKit

enum AliasCategory: String, CaseIterable {
	
	case favoritesCat  = "favorites_cat"
	case categoryflag  = "flag_category"
	case categoryHot4K = "hot_4k_categories"
	case categoryLive  = "live_category"
	case category4k    = "4k_category"
	case categoriesAll = "categories_all"
	case podCategory
}
enum TypeCategory: CaseIterable {
	
	case category
	case categories
  case other
	
	static func type(count: Int?) -> Self {
		if let count = count, count == 1 {
			return .category
		} else {
			return .categories
		}
	}
}

enum TitleCategory: String {
	
	case hotWallpapers = "Hot wallpapers"
	
	static func set(category: GDCategory) -> String? {
		
		switch true {
			case category.typeCategory! == .category:
				return category.wallpapers.first?.title
			case category.typeCategory! == .categories where category.alias == .categoryHot4K:
				return TitleCategory.hotWallpapers.rawValue
			default:
				return ""
		}
	}
}

enum SortedCategory: Comparable {
	
	case favoritesCat
	case categoryflag
	case categoryHot4K
	case categoryLive
	case category4k
	case none
	case categoriesAll
	
	static func set(alias: AliasCategory?) -> Self {
		
		switch alias {
			
			case .favoritesCat:
				return .favoritesCat
				
			case .categoryflag:
				return .categoryflag
				
			case .categoryHot4K:
				return .categoryHot4K
				
			case .categoryLive:
				return .categoryLive
				
			case .category4k:
				return .category4k
				
			case .categoriesAll:
				return .categoriesAll
				
			default:
				return .none
		}
	}
}

enum LoadingVCPresentation {
  
  case Home
  case Interstitial
}
enum NativeAddVCPresentation {
  
  case Controller
  case CollectionCell
  case HomeBottomView
  case NoImage
  case Setting
  case HomeCenterView
}
