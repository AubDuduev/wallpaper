//
//  TabBarImages.swift
import Foundation

enum TabBarImage: String, CaseIterable {
  
  case Home
  case Live
  case Main
	case Image
  case Setting
}
enum NavBarText: String, CaseIterable {
  
  case oneTabBar  = ""
  case twoTabBar  = "Main"
  case treeTabBar = "Live"
  case foreTabBar = "Categories"
  case fiveTabBar = "Settings"
  
  private func localizedString() -> String {
    return NSLocalizedString(self.rawValue, comment: "")
  }
  
  static func getTextFor(title: NavBarText) -> String {
    return title.localizedString()
  }
}

enum TabBarText: String, CaseIterable {
  
  case oneTabBar  = "Главная"
  case twoTabBar  = "Скидки"
  case treeTabBar = "Лента"
  case foreTabBar = "Мои заказы"
  case fiveTabBar = "Я"
  
  private func localizedString() -> String {
    return NSLocalizedString(self.rawValue, comment: "")
  }
  
  static func getTextFor(title: TabBarText) -> String {
    return title.localizedString()
  }
}
