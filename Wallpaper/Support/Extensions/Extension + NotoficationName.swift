//
//  Extension + NotoficationName.swift
//  LebanonKebab
//
//  Created by DEVELOPER on 28/03/2020.
//  Copyright © 2020 DEVELOPER. All rights reserved.
//
import Foundation

extension Notification.Name {
	
	static func id(_ id: Notification.Name.ID) -> Notification.Name {
		let name = Notification.Name(id.rawValue)
		return name
	}
	
	enum ID: String {
		case comleted
		case notificationSusses
    case searchCity
	}
}

