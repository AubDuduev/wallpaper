//
//  Extension + UISegue.swift
//  PhotoGalery
//
//  Created by Aleksandr on 15.10.2019.
//  Copyright © 2019 Aleksandr. All rights reserved.
//
import UIKit

extension UIStoryboardSegue {
	
	func ID() -> SgID? {
		for sgID in SgID.allCases {
			
			if sgID.rawValue == self.identifier {
				return sgID
			}
		}
		return nil
	}
	
	enum SgID: String, CaseIterable {
		
		case WelcomeVC_NativeAddVC
    case SettingVC_NativeAddVC
    case HomeVC_NativeAddCenterVC
    case HomeVC_NativeAddBottomVC
	}
}
