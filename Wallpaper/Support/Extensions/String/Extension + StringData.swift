//
//  Extension + StringData.swift
//  Leap
//
//  Created by Senior Developer on 08.09.2020.
//  Copyright © 2020 Developer. All rights reserved.
//
import UIKit

//MARK: - Date format
extension String {

  func getDate(formatDate: FormatDate) -> Date? {
    let formater = DateFormatter()
    formater.dateFormat = formatDate.rawValue
    let date = formater.date(from: self)
  return date
  }
  enum FormatDate: String {
    case time             = "HH:mm"
    case monthDay         = "MM.dd"
    case monthDayYear     = "MM.dd.yy"
    case dayMonthText     = "dd MMMM"
    case long             = "MMMM d, yyyy"
    case yearMonthDayLine = "yyyy-MM-dd"
    case yearMonthLine    = "yyyy-MM"
  }
  public func getLanguageSystem() -> String {
    let languageSystem = (NSLocale.preferredLanguages.first ?? "") as String
    if let firstIndex = languageSystem.firstIndex(of: "-"){
      return String(languageSystem[..<firstIndex])
    }
  return languageSystem
  }
  public func localizedString() -> String {
    return NSLocalizedString(self, comment: "")
  }
}
