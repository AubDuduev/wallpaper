//
//  Extension + EstimateText.swift
//  DgBetTrip
//
//  Created by Senior Developer on 03.05.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import UIKit

extension String {
  
  public func heightText(plus: CGFloat = 0.0, font: UIFont = .systemFont(ofSize: 15)) -> CGSize {
     let width : CGFloat = 400
     let height: CGFloat = 200
     let size  = CGSize(width: width, height: height)
     let atributes  = [NSAttributedString.Key.font: font]
     let options    = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
     let rect       = self.boundingRect(with      : size,
                                        options   : options,
                                        attributes: atributes,
                                        context   : nil)
    return CGSize(width: rect.width + plus, height: rect.height)
   }
}

