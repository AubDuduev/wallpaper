//
//  Extension + SwiftEntryKit.swift
//  Leap
//
//  Created by Senior Developer on 26.09.2020.
//  Copyright © 2020 Developer. All rights reserved.
//
import SwiftEntryKit
import Foundation

extension EKAttributes {
  
  func popBehavior() -> EKAttributes.PopBehavior {
    
    let behavior = EKAttributes.PopBehavior.animated(animation: .init(translate: .init(duration      : 0.5,
                                                                                       anchorPosition: .bottom,
                                                                                       delay         : 0,
                                                                                       spring        : .init(damping: 0.7, initialVelocity: 1)),
                                                                      scale         : .init(from    : 0.7,
                                                                                            to      : 1,
                                                                                            duration: 1,
                                                                                            delay   : 0.5,
                                                                                            spring   : .init(damping: 0.7, initialVelocity: 1)),
                                                                      fade          : .init(from    : 0.5,
                                                                                            to      : 1,
                                                                                            duration: 1,
                                                                                            delay   : 0,
                                                                                            spring  : .init(damping: 0.5, initialVelocity: 1))))
    
    return behavior
  }
  func animation() -> EKAttributes.Animation {
    let animationSetup =  EKAttributes.Animation(translate: .init(duration      : 0.1,
                                                                  anchorPosition: .top,
                                                                  delay         : 0,
                                                                  spring        : .init(damping: 0.5, initialVelocity: 1)),
                                                 scale    : .init(from    : 0.0,
                                                                  to      : 1.0,
                                                                  duration: 1,
                                                                  delay   : 0.2,
                                                                  spring  : .init(damping: 0.7, initialVelocity: 1)),
                                                 fade     : .init(from    : 0,
                                                                  to      : 1.0,
                                                                  duration: 1,
                                                                  delay   : 0,
                                                                  spring  : .init(damping: 0.5, initialVelocity: 1)))
    return animationSetup
  }
}
