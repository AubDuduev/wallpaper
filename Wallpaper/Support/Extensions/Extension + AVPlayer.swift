//
//  Extension + AVPlayer.swift
//  Leap
//
//  Created by Senior Developer on 18.07.2020.
//  Copyright © 2020 Developer. All rights reserved.
//
import AVKit

extension AVPlayer {
  
  public func duration() -> (second: Int, minuts: Int, seconds: Float)? {
    if let duration = self.currentItem?.duration {
      let seconds = CMTimeGetSeconds(duration)
      guard !seconds.isNaN else { return nil}
      let second = Int(seconds) % 60
      let minuts = Int(seconds) / 60
      return (second, minuts, Float(seconds))
    }
    return nil
  }
}
