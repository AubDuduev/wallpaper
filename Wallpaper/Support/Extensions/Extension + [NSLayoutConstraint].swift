//
//  Extension + [NSLayoutConstraint].swift
//  Leap
//
//  Created by Senior Developer on 30.06.2020.
//  Copyright © 2020 Developer. All rights reserved.
//
import UIKit

extension Array where Element == NSLayoutConstraint {
  
  func set(_ name: Name) -> NSLayoutConstraint? {
    return self.filter{ $0.identifier == name.rawValue }.first
  }
  enum Name: String {
    case headerImageHomeTableHeight
    case searchViewWidth
  }
}
