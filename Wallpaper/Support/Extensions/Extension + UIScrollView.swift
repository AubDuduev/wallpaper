//
//  Extension + UIScrollView.swift
//  Leap
//
//  Created by Senior Developer on 30.06.2020.
//  Copyright © 2020 Developer. All rights reserved.
//
import UIKit

extension UIScrollView {
  
  func contentOffSetBig(_ y: CGFloat) -> Bool {
    if self.contentOffset.y > y {
      return true
    }
  return false
  }
  func contentOffSetSmall(_ y: CGFloat) -> Bool {
    if self.contentOffset.y < y {
      return true
    }
  return false
  }
  func contentOnSetBig(_ y: CGFloat) -> Bool {
    if -self.contentOffset.y > y {
      return true
    }
  return false
  }
  func contentOnSetSmall(_ y: CGFloat) -> Bool {
    if -self.contentOffset.y < y {
      return true
    }
  return false
  }
}
