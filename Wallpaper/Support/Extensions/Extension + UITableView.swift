//
//  Extension + UITableView.swift
//  DgBetTrip
//
//  Created by Senior Developer on 29.04.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import UIKit

extension UITableView {
  
  func scrollTo(_ to: ScrollPosition){
    switch to {
      case .bottom:
        let numberOfSections = self.numberOfSections
        let numberOfRows     = self.numberOfRows(inSection: numberOfSections - 1)
        if numberOfRows > 0 {
          let indexPath = IndexPath(row: numberOfRows - 1, section: (numberOfSections - 1))
          self.scrollToRow(at: indexPath, at: to, animated: true)
        }
      case .top:
        let indexPath = IndexPath(row: 0, section: 0)
        self.scrollToRow(at: indexPath, at: to, animated: true)
      default:
        break
    }
  }
  func countRows(section: Int, minus: Int = 0) -> Int {
    return (self.numberOfRows(inSection: section) - minus)
  }
  public func getLastCell(section: Int) -> UITableViewCell? {
   
    //1 - Получаем последнй индекс ячейки
    let lastRowIndex  = self.numberOfRows(inSection: section) - 1
    
    guard lastRowIndex > -1 else { return nil }
    //2 - Создаем индекс путь последней ячейки
    let lastIndexPath = IndexPath(row: lastRowIndex, section: section)
    //3 - Получаем последнюю ячейку и кастим ее до типа AllInterestsTableCell
    let lastCell      = self.cellForRow(at: lastIndexPath)
    
    return lastCell
  }
  public func getFirstCell(section: Int) -> UITableViewCell? {
   
    //1 - Получаем последнй индекс ячейки
    let firstIndexPath = IndexPath(row: 0, section: section)
    //3 - Получаем последнюю ячейку и кастим ее до типа AllInterestsTableCell
    let firstCell      = self.cellForRow(at: firstIndexPath)
    
    return firstCell
  }
}
