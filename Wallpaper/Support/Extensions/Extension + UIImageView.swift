//
//  Extension + UIImageView.swift
//  Inters
//
//  Created by DEVELOPER on 19/03/2020.
//  Copyright © 2020 DEVELOPER. All rights reserved.
//

import UIKit

extension UIImageView {
	
	func set(nameImage: NameImage){
		self.image = UIImage(named: nameImage.rawValue)
	}
	func set(name: String){
    self.image = UIImage(named: name)
  }
	enum NameImage: String {
		
    case bubbleMessageLeft
    case messageBubleRight
    case messageBubleLeft
		case userTextField
		case passwordTextField
		case passwordConfirmTextField
		case lastNameTextField
		case firstNameTextField
	}
}



