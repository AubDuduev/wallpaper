
import Foundation

class HeaderChoiceState {
	
	public var row  : Int!
	public var state: State!
	
	enum State {
		case open
		case clouse
	}
	init(row: Int, state: State) {
		self.row   = row
		self.state = state
	}
}
