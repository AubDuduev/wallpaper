
import Foundation

struct GDCategory {
	
	var title       : String!
	let wallpapers  : [DECWallpaper]!
	let flags       : [DECFlag]?
	let alias       : AliasCategory!
	let sorted      : SortedCategory!
  let typeCategory: TypeCategory!
	
	init(alias: String, wallpapers: [DECWallpaper]?, flags: [DECFlag]?) {
		self.wallpapers   = wallpapers
		self.flags        = flags
		self.alias        = AliasCategory(rawValue: alias)
		self.sorted       = SortedCategory.set(alias: self.alias)
    self.typeCategory = (flags != nil) ? .other : TypeCategory.type(count: wallpapers?.count)
		self.title        = TitleCategory.set(category: self)
	}
}

