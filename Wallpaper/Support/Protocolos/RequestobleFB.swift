//
//  RequestobleFB.swift
//  Leap
//
//  Created by Senior Developer on 27.07.2020.
//  Copyright © 2020 Developer. All rights reserved.
//
import Foundation

protocol RequestobleFB {
  
  func request(data: Any?, completion: @escaping ClousureRequest)
}
