//
//  CustomAlertView.swift
//  DgBetTrip
//
//  Created by Developer on 31.05.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import SwiftEntryKit
import UIKit

class AlertView: UIView {
  
  public var completion: ClousureEmpty!
  
  public var attributes = EKAttributes()
  
  @IBOutlet weak var fonView     : UIView!
  @IBOutlet weak var titleLabel  : UILabel!
  @IBOutlet weak var messageLabel: UILabel!
  
  private func setup(text: String){
    self.attributes.entranceAnimation = self.animation()
    //self.attributes.popBehavior       = self.popBehavior()
    self.attributes.name              = AlertView.identifier
    self.attributes.displayMode       = .dark
    self.attributes.border            = .value(color: .black, width: 0)
    self.attributes.shadow            = .active(with: .init(color: .init(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.25)), opacity: 1, radius: 7))
    self.attributes.position          = .center
    self.attributes.displayDuration   = .infinity
    let widthConstraint               = EKAttributes.PositionConstraints.Edge.ratio(value: 0.6)
    let textHeight                    = self.messageLabel.heightText(plus: 110)
    let heightConstraint              = EKAttributes.PositionConstraints.Edge.constant(value: textHeight)
    self.attributes.positionConstraints.size = .init(width: widthConstraint, height: heightConstraint)
    self.attributes.roundCorners      = .all(radius: 7)
    self.attributes.screenInteraction = .dismiss
    self.attributes.entryInteraction  = .absorbTouches
    self.attributes.scroll            = .enabled(swipeable: false, pullbackAnimation: .jolt)
    let offset = EKAttributes.PositionConstraints.KeyboardRelation.Offset(bottom: 70, screenEdgeResistance: 50)
    let keyboardRelation = EKAttributes.PositionConstraints.KeyboardRelation.bind(offset: offset)
    self.attributes.positionConstraints.keyboardRelation = keyboardRelation
  }
  public func hide(){
    SwiftEntryKit.dismiss()
  }
  public func show(){
    SwiftEntryKit.display(entry: self, using: self.attributes)
  }
  public func setup(title: String, message: String){
    self.messageLabel.text = message
    self.titleLabel.text   = title
    self.setup(text: message)
    self.setupConstarint()
  }
  private func setupConstarint(){
    self.fonView.cornerRadius(5, true)
  }
  private func popBehavior() -> EKAttributes.PopBehavior {
    
    let behavior = EKAttributes.PopBehavior.animated(animation: .init(translate: .init(duration      : 0.5,
                                                                                       anchorPosition: .bottom,
                                                                                       delay         : 0,
                                                                                       spring        : .init(damping: 0.7, initialVelocity: 1)),
                                                                      scale         : .init(from    : 0.7,
                                                                                            to      : 1,
                                                                                            duration: 1,
                                                                                            delay   : 0.5,
                                                                                            spring   : .init(damping: 0.7, initialVelocity: 1)),
                                                                      fade          : .init(from    : 0.5,
                                                                                            to      : 1,
                                                                                            duration: 1,
                                                                                            delay   : 0,
                                                                                            spring  : .init(damping: 0.5, initialVelocity: 1))))
    
    return behavior
  }
  private func animation() -> EKAttributes.Animation {
    let animationSetup =  EKAttributes.Animation(translate: .init(duration      : 0.1,
                                                                  anchorPosition: .top,
                                                                  delay         : 0,
                                                                  spring        : .init(damping: 0.5, initialVelocity: 1)),
                                                 scale    : .init(from    : 0.0,
                                                                  to      : 1.0,
                                                                  duration: 1,
                                                                  delay   : 0.2,
                                                                  spring  : .init(damping: 0.7, initialVelocity: 1)),
                                                 fade     : .init(from    : 0,
                                                                  to      : 1.0,
                                                                  duration: 1,
                                                                  delay   : 0,
                                                                  spring  : .init(damping: 0.5, initialVelocity: 1)))
    return animationSetup
  }
  @IBAction func okButton(button: UIButton){
    SwiftEntryKit.dismiss(.specific(entryName: AlertView.identifier))
    self.completion?()
  }
}
