//
//  Alert.swift
//  DgBetTrip
//
//  Created by Senior Developer on 19.04.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import Foundation

class AlertButtonOptions {
  
  public func change(type: AlertButtonOptions.Types) -> AlertButtonOptionsoble {
    
    switch type {
      case .InstallTG:
        return AlertButtonTG()
      case .TwoResponce:
        return TwoResponce()
      case .AddPhoto:
        return AddPhoto()
      case .SaveDateUser:
        return SaveDateUser()
      case .SaveInterests:
        return SaveInterests()
      case .NoNews:
        return NoNews()
      case .NoHomeContent:
        return NoHomeContent()
      case .NoOffers:
        return NoOffers()
      case .DeinitLocation:
        return DeinitLocation()
    }
  }
  enum Types {
    case InstallTG
    case TwoResponce
    case AddPhoto
    case SaveDateUser
    case SaveInterests
    case NoNews
    case NoHomeContent
    case NoOffers
    case DeinitLocation
  }
}

class AlertButtonTG: AlertButtonOptionsoble {
 
  var buttonsCount: Int {
    get {
      return buttonsText.count
    }
  }
  var buttonsText = ["Уже установлен", "Установить из App Store", "Отмена"]
}
class TwoResponce: AlertButtonOptionsoble {
 
  var buttonsCount: Int {
    get {
      return buttonsText.count
    }
  }
  var buttonsText = ["Да", "Нет"]
}

class AddPhoto: AlertButtonOptionsoble {
 
  var buttonsCount: Int {
    get {
      return buttonsText.count
    }
  }
  var buttonsText = ["Библиотека", "Фотопарат", "Отмена"]
}

class SaveDateUser: AlertButtonOptionsoble {
 
  var buttonsCount: Int {
    get {
      return buttonsText.count
    }
  }
  var buttonsText = ["Сохранить", "Выйти", "Отмена"]
}

class SaveInterests: AlertButtonOptionsoble {
 
  var buttonsCount: Int {
    get {
      return buttonsText.count
    }
  }
  var buttonsText = ["Сохранить", "Выйти", "Отмена"]
}

class NoNews: AlertButtonOptionsoble {
 
  var buttonsCount: Int {
    get {
      return buttonsText.count
    }
  }
  var buttonsText = ["Интересы", "Home"]
}

class NoHomeContent: AlertButtonOptionsoble {
 
  var buttonsCount: Int {
    get {
      return buttonsText.count
    }
  }
  var buttonsText = ["Переити", "Ок"]
}
class NoOffers: AlertButtonOptionsoble {
 
  var buttonsCount: Int {
    get {
      return buttonsText.count
    }
  }
  var buttonsText = ["Выйти", "Другие"]
}

class DeinitLocation: AlertButtonOptionsoble {
 
  var buttonsCount: Int {
    get {
      return buttonsText.count
    }
  }
  var buttonsText = ["Настройки", "Выйти"]
}
