
import UIKit
import SnapKit

class NativeAddCollectionCell: UICollectionViewCell, LoadNidoble {
  
  private var viewModal: NativeAdvertisingModal!
  private var nativeAddViewController: NativeAdvertisingViewController!
  
  public func configure(viewController: UIViewController){
    self.nativeAddViewController = NativeAdvertisingViewController()
    self.nativeAddViewController.presentationType = .CollectionCell
    viewController.addChild(nativeAddViewController)
    self.nativeAddViewController.didMove(toParent: viewController)
    self.contentView.addSubview(self.nativeAddViewController.view)
    self.nativeAddViewController.view.snp.makeConstraints { (nativeAddView) in
      nativeAddView.left.equalTo(self.contentView).offset(0)
      nativeAddView.right.equalTo(self.contentView).offset(0)
      nativeAddView.top.equalTo(self.contentView).offset(0)
      nativeAddView.bottom.equalTo(self.contentView).offset(0)
    }
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.nativeAddViewController?.view.snp.makeConstraints { (nativeAddView) in
      nativeAddView.left.equalTo(self.contentView).offset(0)
      nativeAddView.right.equalTo(self.contentView).offset(0)
      nativeAddView.top.equalTo(self.contentView).offset(0)
      nativeAddView.bottom.equalTo(self.contentView).offset(0)
    }
  }
}
