//
//  TabBarView.swift
//  UmmaSPB
//
//  Created by Aleksandr on 16.02.2020.
//  Copyright © 2020 Aleksandr. All rights reserved.
//
import UIKit

class TabBarView: UIView {
  
  public var actions: Clousure<Int>!
  
  //MARK: - Outlets
  @IBOutlet weak var commonStackView: UIStackView!
  
  //MARK: - Array outlets
  @IBOutlet var iconsImageViews  : [UIImageView]!
  @IBOutlet var iconsViews       : [UIView]!
 // @IBOutlet var titleLabels      : [UILabel]!
  @IBOutlet var buttonLabels     : [UIButton]!
  @IBOutlet var sectionStackViews: [UIStackView]!
  
  //MARK: - Outlets Constraint
  @IBOutlet weak var topCommonStackViewConstant: NSLayoutConstraint!
  @IBOutlet var heightLabelsConstant           : [NSLayoutConstraint]!
  
  public func setup(){
    //Stack view
    self.topCommonStackViewConstant.constant = 45
    self.sectionStackViews.forEach { $0.spacing = 0 }
    //Labels
    //self.titleLabels.forEach { $0.text = TabBarText.allCases[$0.tag].rawValue }
    //self.titleLabels.forEach { $0.font = .set(.rubikRegular, 15) }
    //self.heightLabelsConstant.forEach {$0.constant = 20 }
    //Images
    self.iconsImageViews.forEach{ $0.tintColor = .black}
    self.iconsImageViews.forEach { $0.image    = UIImage(named: TabBarImage.allCases[$0.tag].rawValue )}
    //Backgraund
    self.iconsImageViews.forEach{ $0.backgroundColor = .clear }
    //self.titleLabels.forEach{ $0.backgroundColor     = .set(.tabBarFon)}
    self.iconsViews.forEach{ $0.backgroundColor      = .clear }
    self.backgroundColor = .clear
  }
  private func animationClick(tag: Int){
    UIView.animate(withDuration: 1, delay: 0,
                   usingSpringWithDamping: 0.5,
                   initialSpringVelocity: 1.0,
                   options: .curveEaseInOut, animations: { [unowned self] in
      //Images set color
			self.iconsImageViews.forEach{ $0.tintColor = .white}
			self.iconsImageViews[tag].tintColor        = .set(.tabBarClick)
			//Label set color
			//self.titleLabels.forEach{ $0.textColor     = .white }
			//self.titleLabels[tag].textColor            = .set(.tabBarClick)
    })
  }
  
  @IBAction func actionButton(button: UIButton){
    self.actions?(button.tag)
    self.animationClick(tag: button.tag)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
}


