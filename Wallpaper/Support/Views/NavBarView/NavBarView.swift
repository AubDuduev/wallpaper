
import UIKit

class NavBarView: UIView {
	
	@IBOutlet weak var titleLabel: UILabel!
  
  public func setup(){
	
  }
	public func setTitleStatic(index: Int){
		self.titleLabel.text = NavBarText.allCases[index].rawValue
	}
	public func setTitleText(title: String){
		self.titleLabel.text = title
	}
  override init(frame: CGRect) {
    super.init(frame: .zero)
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
}
