
import UIKit
import AVKit

class VideoControll: NSObject {
  
  //MARK: - Private
  private var playerLayer = AVPlayerLayer()
  
  //MARK: - Public
  public var isPlay = false
  public var isMute = false
  public var player : AVPlayer!
  public var observerClousure  : Clousure<Any>!
  public var observerReady     : Clousure<Bool>!
  public var observerEnd       : Clousure<Bool>!
  public var observerReturnTime: Clousure<CMTime>!
  
  public func play(){
    self.player.play()
    self.isPlay = true
  }
  public func mute(){
    self.player.isMuted = true
  }
  public func pause(){
    self.player?.pause()
    self.isPlay = false
  }
  public func setup(player: AVPlayer, playerLayer: AVPlayerLayer) {
    self.player      = player
    self.playerLayer = playerLayer
    self.setupPlayerTimeObserver()
    self.setupReadyPlayObserver()
  }
  private func setupReadyPlayObserver(){
    Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
      if self.playerLayer.isReadyForDisplay {
        self.observerReady?(true)
        timer.invalidate()
      }
    self.observerReady?(false)
    }
  }
  private func setupPlayerTimeObserver(){
    player.addObserver(self, forKeyPath: "currentItem.loadedTimeRanges", options: .new, context: nil)
    let interval = CMTime(value: 1, timescale: 2)
    player.addPeriodicTimeObserver(forInterval: interval, queue: nil) { (time) in
      self.observerReturnTime?(time)
      self.end(time: time)
    }
  }
  private func end(time: CMTime){
    //получаем секунды из типа тимер
    let currentSeconds = Float(CMTimeGetSeconds(time))
    let tottalSeconds  = self.player.duration()?.seconds ?? 0
    let sliderValue    = Float(currentSeconds / tottalSeconds)
    //если время совпродает с конечным временем ролика ввыключаем его
    guard sliderValue == 1.0 else { return }
    self.observerEnd?(true)
  }
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    self.observerClousure?(change as Any)
  }
}
