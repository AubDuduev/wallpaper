
import UIKit
import AVKit

class VideoPlayerView: UIView {
  
  private let changeUrlVideo   = ChangeUrlVideo()
  private let setupVideoPlayer = SetupVideoPlayer()
  
  public var playerLayer = AVPlayerLayer()
  public var player      = AVPlayer()
  
  //MARK: -Public
  public let controll       = VideoControll()
  public var videoGravity   : AVLayerVideoGravity    = .resizeAspect
  public var contentsGravity: CALayerContentsGravity = .resizeAspect
  
  public func video(typeVideoURL: TypeVideoURL, typeVideo: TypeVideo = .mp4){
    //create url video
    guard let url = changeUrlVideo.change(typeVideoURL: typeVideoURL, typeVideo: typeVideo) else { return }
    //cretae video
    self.player      = AVPlayer(url: url)
    self.playerLayer = AVPlayerLayer(player: self.player)
    //add video player
    self.layer.addSublayer(self.playerLayer)
    //setup video controll
    self.controll.setup(player: self.player, playerLayer: self.playerLayer)
  }
  
  override func layoutSubviews() {
     super.layoutSubviews()
     self.playerLayer.frame           = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
     self.playerLayer.videoGravity    = self.videoGravity
     self.playerLayer.contentsGravity = self.contentsGravity
   }
}
