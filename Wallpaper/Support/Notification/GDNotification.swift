
import UIKit

class GDSetupNotification: NSObject {
  
  private var notificationCenter  = UNUserNotificationCenter.current()
  private var remoteNotifications : GDRemoteNotifications!
  
  //MARK: - Setup
  public func setup(){
    self.remoteNotifications = GDRemoteNotifications(notification: self)
    //delegates
    self.subscribeDelegates()
    //rerquest user
    self.requestUser(){}
  }
  private func subscribeDelegates(){
    //UNUserNotificationCenter.current().delegate = remoteNotifications
  }
  //MARK: - Request user notifications used
  public func requestUser(completion: @escaping ClousureEmpty){
    if #available(iOS 10.0, *) {
      let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
      UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (succes, error) in
        
        completion()
      }
    }
  }
  func getNotificationSettings(completion: @escaping Clousure<Bool>) {
    UNUserNotificationCenter.current().getNotificationSettings { (settings) in
      switch settings.authorizationStatus {
        case .authorized:    //авторизрван
          completion(true)
        case .denied:        //отказано
          completion(true)
        case .notDetermined: // не определено
          completion(false)
        default:
          completion(false)
      }
    }
  }
  //Создание кастомного уведомления
  public func createNotification(userInfo: [AnyHashable : Any]){
    removeNotification()
    let date        = Date(timeIntervalSinceNow: 5)
    let calendar    = Calendar(identifier: .gregorian)
    let components = calendar.dateComponents([.hour, .minute, .second], from: date)
    let content    = UNMutableNotificationContent()
    let trigger    = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
    
    content.title = userInfo["title"]   as? String ?? "title"
    content.body   = userInfo["message"] as? String ?? userInfo["body"] as? String ?? "body"
    content.sound = UNNotificationSound.default
    let request   = UNNotificationRequest(identifier: "", content: content, trigger: trigger)
    let center    = UNUserNotificationCenter.current()
    center.add(request)
  }
  //удаление уведомлений
  private func removeNotification(){
    let center = UNUserNotificationCenter.current()
    center.removePendingNotificationRequests(withIdentifiers: [""])
  }
}
