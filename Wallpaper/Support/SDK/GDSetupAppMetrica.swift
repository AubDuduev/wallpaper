
import YandexMobileMetrica
import UserNotifications
import YandexMobileMetricaPush
import UIKit

class GDSetupAppMetrica: NSObject {
  
  public  let apyKey = "c02b8773-608d-466a-a5fa-2a331be369e2"
  
  public func activate(){
    let configuration = YMMYandexMetricaConfiguration.init(apiKey: apyKey)
    YMMYandexMetrica.activate(with: configuration!)
  }
  public func setDeviceToken(deviceToken: Data){
    self.requestDiviceTokenID(deviceToken: deviceToken)
    YMPYandexMetricaPush.setDeviceTokenFrom(deviceToken)
    // If the AppMetrica SDK library was not initialized before this step,
    // calling the method causes the app to crash.
    #if DEBUG
    let pushEnvironment = YMPYandexMetricaPushEnvironment.development
    #else
    let pushEnvironment = YMPYandexMetricaPushEnvironment.production
    #endif
    YMPYandexMetricaPush.setDeviceTokenFrom(deviceToken, pushEnvironment: pushEnvironment)
    let dispatchQueue = DispatchQueue(label: "appmetrica_device_id")
    //get appmetrica_device_id and after patch server
    YMMYandexMetrica.requestAppMetricaDeviceID(withCompletionQueue: dispatchQueue) { (appmetricaDeviceID, error) in
      //error
      if let error = error {
        print(error.localizedDescription, "Error get appmetrica_device_id")
        //save appmetrica_device_id
      } else {
        guard let _ = appmetricaDeviceID else { return }
       
      }
    }
  }
  //MARK: - Request DiviceTokenID
  private func requestDiviceTokenID(deviceToken: Data){
    let deviceToken = deviceToken.getTokenID()
    print(deviceToken)
  }
  public func addNotification(application: UIApplication){
    
    // Register for push notifications
    if #available(iOS 8.0, *) {
      if #available(iOS 10.0, *) {
        // iOS 10.0 and above.
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
          //Enable or disable features based on authorization.
        }
      } else {
        // iOS 8 and iOS 9.
        let settings = UIUserNotificationSettings(types: [.badge, .alert, .sound], categories: nil)
        application.registerUserNotificationSettings(settings)
      }
      application.registerForRemoteNotifications()
    } else {
      // iOS 7
      application.registerForRemoteNotifications(matching: [.badge, .alert, .sound])
    }
  }
  public func handlePushNotification(_ userInfo: [AnyHashable : Any]){
    // Track received remote notification.
    // Method [YMMYandexMetrica activateWithApiKey:] should be called before using this method.
    YMPYandexMetricaPush.handleRemoteNotification(userInfo)
  }
}


