
import Foundation
import AppLovinSDK
import GoogleMobileAds
import AppLovinAdapter

class GDAppLovin {
	
	private let sdkKey = "onKeuKnZVziUfuyfOPuumSDUM23G9FEy0TbvuNSNKAt_2peQDXJz-WRLZVbklqgmNGeFd5dzFUyIHbkU71S5Tv"
	
	public func setup(){
		
		let sdk = ALSdk.shared(withKey: self.sdkKey)
		sdk?.initializeSdk()
		ALPrivacySettings.setHasUserConsent(true)
		ALPrivacySettings.setIsAgeRestrictedUser(true)
	}

	public func mute(){
		let request = GADRequest()
		let extras = GADMAdapterAppLovinExtras()
		extras.muteAudio = false
		request.register(extras)

	}
}
