
import Foundation
import UserNotifications
import FirebaseAnalytics
import FirebaseCore
import FirebaseRemoteConfig

class GDSetupFirebase: NSObject {
	
	public func setup(){
		FirebaseApp.configure()
	}
	
	private func remoteConfig() -> RemoteConfig {
		let remoteConfig = RemoteConfig.remoteConfig()
		let settings     = RemoteConfigSettings()
		settings.minimumFetchInterval = 0
		remoteConfig.configSettings = settings
		return remoteConfig
	}
	public func getGeo(){
		self.remoteConfig().fetchAndActivate { (status, error) in
			if status != .error {
				print(self.remoteConfig()["geo"].boolValue, " - remote configuration geo")

			}
		}
	}
}
