
import GoogleMobileAds
import Foundation
import SnapKit

class GDAddMob: NSObject {
	
	
	static let shared = GDAddMob()
	
	private let banerID        = "ca-app-pub-4473652063394919/5104117284"
	private let nativeID       = "ca-app-pub-1071618994582157/4665524855"
  private let interstitialID = "ca-app-pub-1071618994582157/2997219960"
	
	private let testBanerID        = "ca-app-pub-3940256099942544/2934735716"
	private let testNativeID       = "ca-app-pub-3940256099942544/3986624511"
  private let testInterstitialID = "ca-app-pub-3940256099942544/4411468910"
	
  private var timer: Timer!
  
	private var bannerView: GADBannerView! = {
		let baner = GADBannerView()
		baner.adUnitID = "ca-app-pub-3940256099942544/2934735716"
		baner.load(GADRequest())
		return baner
	}()
  
  private var interstitial: GADInterstitial!
  private var adLoader = GADAdLoader()
  
	public func setup(){
		GADMobileAds.sharedInstance().start { (statusInitialization) in
			//Print status initilization
      if !statusInitialization.adapterStatusesByClassName.isEmpty {
      statusInitialization.adapterStatusesByClassName.forEach({print($0)})
      }
      print(statusInitialization.debugDescription)
      print(statusInitialization.description)
		}
		//GADMobileAds.sharedInstance().requestConfiguration.testDeviceIdentifiers = ["2077ef9a63d2b398840261c8221a0c9b"]
	}
	public func setupBannerView(viewController: UIViewController) -> GADBannerView {
    self.bannerView.delegate = self
    self.bannerView.rootViewController = viewController
    return self.bannerView
	}
  public func setupNative(viewController: NativeAdvertisingViewController, adLoader: inout GADAdLoader){
    let multipleAdsOptions = GADMultipleAdsAdLoaderOptions()
    multipleAdsOptions.numberOfAds = 5
    adLoader = GADAdLoader(adUnitID: self.nativeID,
                           rootViewController: viewController,
                           adTypes: [ .unifiedNative],
                           options: [multipleAdsOptions])
    adLoader.delegate = viewController
    adLoader.load(GADRequest())
  }
  public func setupInterstitial(viewController: InterstitialViewController){
    self.interstitial = GADInterstitial(adUnitID: self.interstitialID)
    interstitial.delegate = viewController
    interstitial.load(GADRequest())
    if interstitial.isReady {
     
    } else {
      print("Ad wasn't ready")
    }
    
  }
  public func presentInterstitial(viewController: InterstitialViewController){
    self.interstitial.present(fromRootViewController: viewController)
  }
}


