
import UIKit
import OneSignal

class GDSetupOneSignal {
  
  //START OneSignal initialization code
  private let appID = "d98ec3b6-fee5-4861-8dba-c080505df9ad"
  
  public func setup(launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
   
    OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
    OneSignal.initWithLaunchOptions(launchOptions)
    OneSignal.setAppId(self.appID)
    
    OneSignal.promptForPushNotifications(userResponse: { accepted in
      print("User accepted notifications: \(accepted)")
    })
  }
  
}
