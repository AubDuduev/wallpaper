import StoreKit
import UIKit

class ErrorHandlerPurchaces {
  
  public func check(errorCode: Int) -> Bool {
    do {
      try self.error(errorCode: errorCode)
    } catch HandlerError.Success {
      return true
    } catch {
      let text = HandlerError.allCases[errorCode].rawValue
      AlertEK.customText(title: .error, textEnd: text)
      return false
    }
  return false
  }
  private func error(errorCode: Int) throws  {
    
    //Ошибка получения
    guard let error = SKError.Code(rawValue: errorCode) else { throw HandlerError.Nil }
    
    
    
    //Ошибки не обнаружены
    throw HandlerError.allCases[error.rawValue]
  }
  
  private enum HandlerError: String, Error, CaseIterable{
    
    case unknown = "Извените произошла неизвестная или неожиданная ошибка."
    case clientInvalid = "Извените клиенту не разрешено выполнить попытку действия."
    case paymentCancelled = "Извените пользователь отменил платежный запрос."
    case paymentInvalid   = "Извените один из параметров платежа не был распознан App Store."
    case paymentNotAllowed = "Извените пользователю не разрешено авторизовать платежи."
    case storeProductNotAvailable = "Извените запрошенный товар недоступен в магазине."
    case cloudServicePermissionDenied = "Извените пользователь не разрешил доступ к информации облачного сервиса."
    case cloudServiceNetworkConnectionFailed = "Извените устройству не удалось подключиться к сети."
    case cloudServiceRevoked = "Извените пользователь отозвал разрешение на использование этой облачной службы."
    case privacyAcknowledgementRequired = "Извените пользователь еще не подтвердил политику конфиденциальности Apple для Apple Music."
    case unauthorizedRequestData = "Извените приложение пытается использовать свойство, для которого у него нет необходимых прав."
    case invalidOfferIdentifier = "Извените идентификатор предложения не может быть найден или не активен."
    case invalidSignature = "Извените подпись в платежной скидке недействительна."
    case missingOfferParams = "Извените отсутствие параметров в скидке на оплату."
    case invalidOfferPrice = "Извените цена, указанная в App Store Connect, больше не действительна."
    case overlayCancelled = "Наложение отменено"
    case overlayInvalidConfiguration = "Неверная конфигурация наложения"
    case overlayTimeout = "Overlay Timeout"
    case ineligibleForOffer = "Не соответствует требованиям для предложения"
    case Nil
    case Success
  }
  
}


