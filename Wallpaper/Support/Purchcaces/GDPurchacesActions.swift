
import Foundation

class GDPurchacesActions: NSObject {
  
  static let shared = GDPurchacesActions()
  
  private let errorHandler = ErrorHandlerPurchaces()
  private let server       = Server()
  private let coreData     = CoreData()
  
  public var isPurchaces = false
  
  public func actions(purchased: ClousureEmpty? = nil, failed: ClousureEmpty? = nil, purchasing: ClousureEmpty? = nil, restored: ClousureEmpty? = nil){
  
    //Покупка произведена
    GDPurchaсes.shared.purchasesPurchased = { productID in
      self.savePurchaces(id: productID)
      self.getCurrentPurchaces()
      purchased?()
    }
    //Ошибка покупки
    GDPurchaсes.shared.purchasesFailed = { [weak self] errorCode in
      guard let self = self else { return }
      let _ = self.errorHandler.check(errorCode: errorCode)
      print(errorCode)
      failed?()
    }
    //Процесс покупки
    GDPurchaсes.shared.purchasesPurchasing = {
      AlertEK.dеfault(title: .purchaces, message: .purchasesProcess)
      purchasing?()
    }
    //Востановление покупки
    GDPurchaсes.shared.purchasesRestored = {
      AlertEK.dеfault(title: .purchaces, message: .purchasesFiled)
      restored?()
    }
  }
  public func restored(){
    //1 - Запрашиваем у Apple все покупки в приложении
    self.receipt { [weak self] (receipt) in
      guard let self = self else { return }
      guard let inApp = receipt?.receipt?.inApp else {
        AlertEK.dеfault(title: .error, message: .noRestoredPurcahaes)
        return
      }
      //2 - Ищем есть ли в этих покупках та которую выбрал пользователь
      let product1 = inApp.filter { $0.productID == GDPurchacesProductID.allCases[0].rawValue }
      let product2 = inApp.filter { $0.productID == GDPurchacesProductID.allCases[1].rawValue }
      let product3 = inApp.filter { $0.productID == GDPurchacesProductID.allCases[2].rawValue }
      guard !product1.isEmpty else { return }
      AlertEK.dеfault(title: .information, message: .purchasesRestored){
        self.savePurchaces(id: 0)
        self.getCurrentPurchaces()
      }
    }
  }
  public func pay(id: GDPurchacesProductID){
    GDPurchaсes.shared.pay(id: id)
  }
  public func getCurrentPurchaces(){
    self.coreData.fetch(object: CDPurchase.self) { (purchase) in
      if let purchase = purchase as? CDPurchase {
        let date = purchase.date ?? Date()
        let day = date.days(from: Date())
        if day < purchase.periood {
          self.isPurchaces = true
        } else {
          self.isPurchaces = false
        }
      } else {
        self.isPurchaces = false
      }
    }
  }
  private func receipt(completion: @escaping Clousure<DECReceipt?>){
    //Request
    self.server.request(requestType: POSTReceipt()) { (serverResult) in
      //Response
      switch serverResult {
        case .error(let error):
          guard let error = error else { return }
          print("Error server data: class: ServerPurchaces ->, function: receipt -> data: DECReceipt ->, description: ", error.localizedDescription)
        //Susses
        case .object(let object):
          let receipt  = object as? DECReceipt
          completion(receipt)
          print("Succesful data: class: ServerPurchaces ->, function: receipt ->, data: DECReceipt")
        
      }
    }
  }
  private func savePurchaces(id: Int){
    self.coreData.fetch(object: CDPurchase.self) { (purchase) in
      if let purchase = purchase as? CDPurchase {
        purchase.date    = Date()
        purchase.periood = 30
      } else {
        let purchase = self.coreData.create(object: CDPurchase.self)
        purchase.date    = Date()
        purchase.periood = 30
      }
      self.coreData.edit()
    }
  }
  override private init() { }
}
