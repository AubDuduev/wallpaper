import Foundation
import StoreKit

//add observer of transactions paymentQueue protocols
extension GDPurchaсes: SKPaymentTransactionObserver {
  
  func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
    for transaction in transactions {
      switch transaction.transactionState{
      case .deferred:
        print("loading purchases...")
      case .failed:
        self.failed(transactions: transaction)
      case .purchased:
        self.purchased(transactions: transaction)
      case .purchasing:
        self.purchasing(transactions: transaction)
      case .restored:
        self.restored(transactions: transaction)
      default: break
      }
    }
  }
}
//response from server for ID purchases get product
extension GDPurchaсes: SKProductsRequestDelegate {
  func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
    self.products = response.products
    self.products.forEach{ print($0.productIdentifier, " - productIdentifier Purchases") }
  }
}
//functions transactions state
extension GDPurchaсes {
  
  //error purchased
  private func failed(transactions: SKPaymentTransaction){
    print("failed purchases...")
    guard let transactionsError = transactions.error as NSError? else { return }
    self.paymentQueue.finishTransaction(transactions)
    self.purchasesFailed?(transactionsError.code)
  }
  //load purchased
  private func deferred(transactions: SKPaymentTransaction){
    
  }
  // success purchased
  private func purchased(transactions: SKPaymentTransaction){
    print("purchased complited...")
    //make a purchses and create add score
    for (index, id) in GDPurchacesProductID.allCases.enumerated() {
      if transactions.payment.productIdentifier == id.rawValue {
        self.purchasesPurchased?(index)
      }
    }
    //delete data from is purchases memory
    self.paymentQueue.finishTransaction(transactions)
  }
  //process purchased
  private func purchasing(transactions: SKPaymentTransaction){
    print("purchasing process...")
    self.purchasesPurchasing?()
  }
  //resored this purchased
  private func restored(transactions: SKPaymentTransaction){
    print("restored purchases...")
    self.purchasesRestored?()
    self.paymentQueue.restoreCompletedTransactions()
    self.paymentQueue.finishTransaction(transactions)
  }
}

