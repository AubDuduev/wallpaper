
import Foundation

enum GDPurchacesProductID: String, CaseIterable {
  
  case monthID      = "com.orangewallpapers.wallpapers.month"
  case threemonthID = "com.orangewallpapers.wallpapers.threemonth"
  case yearID       = "com.orangewallpapers.wallpapers.year"
}
