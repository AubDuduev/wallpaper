import Foundation
import StoreKit

class GDPurchaсes: NSObject {
  
  static let shared = GDPurchaсes()
  
  public var products              : [SKProduct]!
  public let paymentQueue          = SKPaymentQueue.default()
  public var purchasesFailed       : Clousure<Int>!
  public var purchasesPurchased    : Clousure<Int>!
  public var purchasesPurchasing   : ClousureEmpty!
  public var purchasesRestored     : ClousureEmpty!
  public var errorHandlerPurchaces = ErrorHandlerPurchaces()
  
  //check whether the app can make purchases
  public func testPurchasesWorking(){
    if SKPaymentQueue.canMakePayments() {
      //add observer of transactions (delegate)
      paymentQueue.add(self)
      print("this App can make purchases")
      self.getIdProduct()
    } else {
      print("this App don't can make purchases !!!")
    }
  }
  //ge IDt priduct
  private func getIdProduct(){
    var id = Set<String>()
    //get all identifire
    GDPurchacesProductID.allCases.forEach{ id.insert($0.rawValue) }
    //get reqiest product for identifire
    let productRequest = SKProductsRequest(productIdentifiers: id)
    productRequest.delegate = self
    productRequest.start()
  }
  // this in Payment and make a purchase
  public func pay(id: GDPurchacesProductID){
    guard let products = products else {
      AlertEK.dеfault(title: .error, message: .purchasesNoSuccess)
      return
    }
    let product = products.filter{ $0.productIdentifier == id.rawValue }
    guard product.first != nil else { return }
    let payment = SKPayment(product: product.first!)
    //make a purchase
    self.paymentQueue.add(payment)
  }
  override init(){}
}


