import UIKit

class Texts {
	static func text(_ text: Project) -> String {
		return text.rawValue
	}
	enum Title: String, CaseIterable {
		
		case empty        = ""
		case error        = "ОШИБКА"
		case noNetwork    = "НЕТ ИНТЕРНЕТА"
		case noMessage    = "НЕТ СООБЩЕНИЙ"
		case sendEmail    = "ОТПРАВЛЕН"
		case save         = "Сохранен"
		case registration = "Регистрация"
		case information  = "Информация"
		case install      = "Установить"
		case addPhoto     = "Изменить фото"
		case searchResult = "Результат поиска"
    case success      = "Удачно"
    case purchaces    = "Покупки"
		
		private func localizedString() -> String {
			return NSLocalizedString(self.rawValue, comment: "")
		}
		
		func getTextFor(title: Title?) -> String {
			return title!.localizedString()
		}
	}
	enum Message: String, CaseIterable {
		
		case empty               = ""
		case noNetwork           = "Отсутствует подключение к сети, подключите Ваше устройство и попробуйте снова"
		case noJSON              = "Извините произошла ошибка получения данных"
		case noMessage           = "У Вас нет не одного сообщения"
		case errorUnknown        = "Неизвестная ошибка"
		case noMailCompose       = "Вы не можете отправлять сообщения"
    case errorSaveWalppaer   = "Ошибка сохронения обоев"
    case saveSuccess         = "Обои сохранены"
    case noRestoredPurcahaes = "Вы не можете востановить покупку ,так как Вы ее не совершали!"
    case purchasesCompleted  = "Ваша покупка оформлена"
    case purchasesNoSuccess  = "Покупки недоступны"
    case purchasesProcess    = "Идет процесс покупки...."
    case purchasesFiled      = "Ошибка покупки"
    case purchasesRestored   = "Покупка востановлена"
    case alertPurchases      = "Если Вы уже совершали эту покупку, то вы можете востановить ее. "
		
		private func localizedString() -> String {
			return NSLocalizedString(self.rawValue, comment: "")
		}
		
		func getTextFor(message: Message?) -> String {
			return message!.localizedString()
		}
		
		func custom(message: Message, textEnd: String) -> String {
			return message.rawValue + textEnd
		}
	}
	enum MessageType {
		case text(Message)
		case custom(MessageCustom)
	}
	enum MessageCustom {
		case message(String)
		
		func custom(message: Message) -> String {
			switch self {
				case .message(let text):
					return message.rawValue + text
			}
		}
	}
	enum Worning {
		case non
	}
	
	enum Project: String {
		
		case empty
		
		private func localizedString() -> String {
			return NSLocalizedString(self.rawValue, comment: "")
		}
		func getTextFor(_ text: Project?) -> String {
			return text!.localizedString()
		}
	}
}
enum TextType {
	
	case Title
	case Message
	case Project
}
enum TextAlert: String, CaseIterable {
	
	case empty
	case noJSON
	case noNetwork
	case noSendedEmail
	case yesSendedEmail
	case inputEmail
	case emptyField
	case noMessage
	case sendEmail
	case tryEnded
	case saveLike
	case registrRequaer
	case changeTour
	
}
enum TextSimple {
	case some
}



