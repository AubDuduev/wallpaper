
import Foundation
import CoreData


extension CDPreview {
	
	@nonobjc public class func fetchRequest() -> NSFetchRequest<CDPreview> {
		return NSFetchRequest<CDPreview>(entityName: "CDPreview")
	}
	
	@NSManaged public var previewURL: String?
	@NSManaged public var url: String?
	
	convenience init() {
		self.init(context: PersistentService.context)
	}
}

extension CDPreview : Identifiable {
	
}
