
import UIKit

class ErrorHandlerHomeData {
   
	public func check(previewData: DECPreviewData?) -> Bool {
    do {
			try self.error(previewData: previewData)
    } catch HandlerError.Succes {
      return true
    } catch HandlerError.Nil {
      AlertEK.customText(title: .error, textEnd: HandlerError.Nil.rawValue)
      return false
    } catch HandlerError.Empty {
      AlertEK.customText(title: .error, textEnd: HandlerError.Empty.rawValue)
      return false
    } catch {
      AlertEK.dеfault(title: .error, message: .errorUnknown)
      return false
    }
  return false
  }
  private func error(previewData: DECPreviewData?) throws  {
    
    //Ошибка получения
    guard let _ = previewData else { throw HandlerError.Nil }
    
    
    //Ошибки не обнаружены
    throw HandlerError.Succes
  }
  
  private enum HandlerError: String, Error {
    
    case Nil    = "Ошибка получения данных для экрана Home"
    case Empty  = "Получен , но пустой"
    case Succes
  }
}


