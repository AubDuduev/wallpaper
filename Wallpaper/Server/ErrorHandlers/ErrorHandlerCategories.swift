
import UIKit

class ErrorHandlerCategories {
   
  public func check(wallpapers: [DECWallpaper]?) -> Bool {
    do {
			try self.error(wallpapers: wallpapers)
    } catch HandlerError.Succes {
      return true
    } catch HandlerError.Nil {
      AlertEK.customText(title: .error, textEnd: HandlerError.Nil.rawValue)
      return false
    } catch HandlerError.Empty {
      AlertEK.customText(title: .error, textEnd: HandlerError.Empty.rawValue)
      return false
    } catch {
      AlertEK.dеfault(title: .error, message: .errorUnknown)
      return false
    }
  return false
  }
	private func error(wallpapers: [DECWallpaper]?) throws  {
    
    //Ошибка получения
    guard let _ = wallpapers else { throw HandlerError.Nil }
    
    //Ошибки не обнаружены
    throw HandlerError.Succes
  }
  
  private enum HandlerError: String, Error {
    
    case Nil    = "Ошибка получения категорий"
    case Empty  = "Получен , но пустой"
    case Succes
  }
}


