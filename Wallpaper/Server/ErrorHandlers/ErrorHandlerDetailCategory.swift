
import UIKit

class ErrorHandlerDetailCategory {
   
	public func check(mainPreview: DECPreviewData?) -> Bool {
    do {
			try self.error(mainPreview: mainPreview)
    } catch HandlerError.Succes {
      return true
    } catch HandlerError.Nil {
      AlertEK.customText(title: .error, textEnd: HandlerError.Nil.rawValue)
      return false
    } catch HandlerError.Empty {
      AlertEK.customText(title: .error, textEnd: HandlerError.Empty.rawValue)
      return false
    } catch {
      AlertEK.dеfault(title: .error, message: .errorUnknown)
      return false
    }
  return false
  }
  private func error(mainPreview: DECPreviewData?) throws  {
    
    //Ошибка получения
    guard let _ = mainPreview else { throw HandlerError.Nil }
    
    
    //Ошибки не обнаружены
    throw HandlerError.Succes
  }
  
  private enum HandlerError: String, Error {
    
    case Nil    = "Ошибка получения под категории"
    case Empty  = "Получен , но пустой"
    case Succes
  }
}


