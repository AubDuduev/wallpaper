
import UIKit

class ErrorHandlerMainData {
   
	public func check(decMainData: DECMainData?) -> Bool {
    do {
			try self.error(decMainData: decMainData)
    } catch HandlerError.Succes {
      return true
    } catch HandlerError.Nil {
      AlertEK.customText(title: .error, textEnd: HandlerError.Nil.rawValue)
      return false
    } catch HandlerError.Empty {
      AlertEK.customText(title: .error, textEnd: HandlerError.Empty.rawValue)
      return false
    } catch {
      AlertEK.dеfault(title: .error, message: .errorUnknown)
      return false
    }
  return false
  }
  private func error(decMainData: DECMainData?) throws  {
    
    //Ошибка получения
    guard let _ = decMainData else { throw HandlerError.Nil }
    
    
    //Ошибки не обнаружены
    throw HandlerError.Succes
  }
  
  private enum HandlerError: String, Error {
    
    case Nil    = "Ошибка получения всех обоев"
    case Empty  = "Получен , но пустой"
    case Succes
  }
}


