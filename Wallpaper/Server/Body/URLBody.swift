
import UIKit

class URLBody {
  
  enum Types {
    
    case Receipt(String?)
  }
  
  public func create(type: Types?) -> Data? {
    guard let type = type else { return nil }
    switch type {
      case .Receipt(let receiptString):
        let receipt = ENCReceipt(receiptData: receiptString)
        let data = try? JSONEncoder().encode(receipt)
        return data
    }
  }
}
