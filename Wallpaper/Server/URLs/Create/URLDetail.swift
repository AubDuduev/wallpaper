
import Foundation

class URLDetail: URLCreatoble {
  
	private var urlPath   = URLPath()
	private var urls      = URLs()
	private var customURL = URLCustom()
	
	public func url(_ type: URLType.Types) -> ReturnURL? {
		
		switch type {
			case .detail(let path):
				let customURL = self.customURL.create(type: .init(scheme: .http, host: .Static(.main), changePath: .detail(path)))
				return (customURL.string, customURL.URL)
			default:
				 return nil
		}
	}
}
