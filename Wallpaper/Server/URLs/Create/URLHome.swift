
import Foundation

class URLHome: URLCreatoble {
  
  private var urlPath   = URLPath()
  private var urls      = URLs()
	private var customURL = URLCustom()
  
  public func url(_ type: URLType.Types) -> ReturnURL? {
    
    switch type {
			case .home:
				let customURL = self.customURL.create(type: .init(scheme: .http, host: .Static(.main), path: .home))
				return (customURL.string, customURL.URL)
      default:
         return nil
    }
  }
}
