
import Foundation

class URLReceipt: URLCreatoble {
  
  private let urlPath = URLPath()
  private let urls    = URLCustom()
  private let host    = URLHost()
  
  public func url(_ type: URLType.Types) -> ReturnURL? {
    
    switch type {
      case .receipt:
        var hostCreate: URLHost.Types! = .Static(.verifyDev)
        #if DEBUG
        hostCreate = .Static(.verifyDev)
        #else
        hostCreate = .Static(.verifyProd)
        #endif
        let custum = URLCustom.Custom(scheme: .https, host: hostCreate, path: .verifyReceipt)
        let stringURL = urls.create(type: custum).string ?? ""
        let url       = URL(string: stringURL)
        return (stringURL, url)
      default:
         return nil
    }
  }
}
