//
//  URLString.swift
//  DgBetTrip
//
//  Created by Senior Developer on 19.04.2020.
//  Copyright © 2020 Senior Developer. All rights reserved.
//
import Foundation

class URLHost {
  
  enum Host: String {

    case non
    case main       = "157.230.26.144"
    case verifyProd = "buy.itunes.apple.com"
    case verifyDev  = "sandbox.itunes.apple.com"
  }
  enum Types {
    case Static(Host)
    case String(String?)
    case non
  }
  public func create(_ type: URLHost.Types) -> String {
    switch type {
      case .Static(let host):
        return host.rawValue
      case .String(let stringHost):
        return stringHost ?? ""
      case .non:
        return ""
    }
  }
}
