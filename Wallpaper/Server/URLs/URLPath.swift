
import Foundation

class URLPath {
  
  private let currentUserID = ""//gvUserData?.userID ?? "no userd id"
  
  enum Path: String {

    case non           = ""
    case home          = "/home-ios.json"
		case mainNik       = "/main-nik.json"
		case first         = "/first.json"
		case live          = "/live-cat.json"
		case categories    = "/categories.json"
    case verifyReceipt = "/verifyReceipt"
  }
  enum ChangePath {
    
    case non
    case detail(String)
  }
  
  public func create(change: ChangePath) -> String {
    //create Change Path for url
    switch change {
      //MARK: - Non
      case .non:
        return ""
			case .detail(let path):
				return path
    }
  }
}
