
import Foundation

struct DECPreview {
  
	let previewURL: String!
	let url       : String!
	let alias     : AliasCategory! = .podCategory
  
  enum CodingKeys: String, CodingKey {
		case previewURL = "preview_url"
		case url        = "url"
  }
}
extension DECPreview: Decodable {
  
  init(from decoder: Decoder) throws {
		
    let values = try decoder.container(keyedBy: CodingKeys.self)
		
    self.previewURL = try? values.decode(String?.self, forKey: .previewURL)
		self.url        = try? values.decode(String?.self, forKey: .url)
  }
}

extension DECPreview {
	
	init(preview: CDPreview) {
		self.previewURL = preview.previewURL
		self.url        = preview.url
	}
}

extension DECPreview {
  
  init(flag: DECFlag) {
    self.previewURL = flag.image
    self.url        = flag.link
  }
}

extension DECPreview {
  
  init(wallpaper: DECWallpaper) {
    self.previewURL = wallpaper.image
    self.url        = wallpaper.link
  }
}
