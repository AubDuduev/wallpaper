
import Foundation

struct DECPreviewData {
  
  var data: DECMainPreview!
  
  enum CodingKeys: String, CodingKey {
		
    case data = "data"
  }
}
extension DECPreviewData: Decodable {
  
  init(from decoder: Decoder) throws {
		
    let values = try decoder.container(keyedBy: CodingKeys.self)
		
    self.data = try? values.decode(DECMainPreview?.self, forKey: .data)
  }
}
