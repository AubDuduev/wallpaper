
import Foundation

struct DECMainPreview {
  
	let ID      : Int!
	let name    : String!
	let url     : String!
	var previews: [DECPreview]!
  
  enum CodingKeys: String, CodingKey {
		
    case ID       = "id"
		case name     = "nameCategory"
		case url      = "urlPhoto"
		case previews = "array"
  }
}
extension DECMainPreview: Decodable {
  
  init(from decoder: Decoder) throws {
		
    let values = try decoder.container(keyedBy: CodingKeys.self)
		
		self.ID       = try? values.decode(Int?.self         , forKey: .ID)
    self.name     = try? values.decode(String?.self      , forKey: .name)
		self.url      = try? values.decode(String?.self      , forKey: .url)
		self.previews = try? values.decode([DECPreview]?.self, forKey: .previews)
  }
}
