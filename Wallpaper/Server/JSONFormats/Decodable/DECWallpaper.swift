
import Foundation

struct DECWallpaper {
  
	let link : String?
	let title: String?
	let image: String?
	let flags: [DECFlag]?
  
  enum CodingKeys: String, CodingKey {
    
		case link  = "link"
		case title = "title"
		case image = "image"
		case flags = "items"
  }
}
extension DECWallpaper: Decodable {
  
  init(from decoder: Decoder) throws {
		
    let values = try decoder.container(keyedBy: CodingKeys.self)
		
		self.link  = try? values.decode(String?.self   , forKey: .link)
		self.title = try? values.decode(String?.self   , forKey: .title)
		self.image = try? values.decode(String?.self   , forKey: .image)
		self.flags = try? values.decode([DECFlag]?.self, forKey: .flags)
  }
}
