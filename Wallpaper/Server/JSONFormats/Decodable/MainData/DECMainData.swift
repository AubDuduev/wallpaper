
import Foundation

struct DECMainData {
	
	let categories: [GDCategory]!
	
	private struct DynamicCodingKeys: CodingKey {
		
		var stringValue: String
		init?(stringValue: String) {
			self.stringValue = stringValue
		}
		
		var intValue: Int?
		init?(intValue: Int) {
			return nil
		}
	}
	enum StringCodingKeys: String, CodingKey {
		case categoryFlag = "flag_category"
	}
}
//Inition
extension DECMainData: Decodable {
	
	init(from decoder: Decoder) throws {
		
		//1 - Получаем все данные в контеинер
		let dynamicContainer = try decoder.container(keyedBy: DynamicCodingKeys.self)
		
		var createCategories = [GDCategory]()
		
		//2 - Продимся по всем ключчам в контеинере
		for key in dynamicContainer.allKeys {
			
			//3 - Создаем по ключу кетегория тип массив wallpapers и ключ
			if let wallpapers = try? dynamicContainer.decode([DECWallpaper]?.self, forKey: DynamicCodingKeys(stringValue: key.stringValue)!){
				let category = GDCategory(alias: key.stringValue, wallpapers: wallpapers, flags: nil)
				createCategories.append(category)
			}
			//4 - Создаем по ключу кетегория тип wallpapers и ключ
			else if let wallpaper = try? dynamicContainer.decode(DECWallpaper?.self, forKey: DynamicCodingKeys(stringValue: key.stringValue)!){
				let category = GDCategory(alias: key.stringValue, wallpapers: [wallpaper], flags: wallpaper.flags)
				createCategories.append(category)
			}
		}
		
		//6 - Добовляем все категории
		self.categories   = createCategories
	}
}

