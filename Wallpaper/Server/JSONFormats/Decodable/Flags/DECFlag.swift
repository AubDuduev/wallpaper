
import Foundation

struct DECFlag {
	
	let link : String?
	let image: String?
	
	enum CodingKeys: String, CodingKey {
		
		case link  = "link"
		case image = "image"
	}
}
extension DECFlag: Decodable {
	
	init(from decoder: Decoder) throws {
		
		let values = try decoder.container(keyedBy: CodingKeys.self)
		
		self.link  = try? values.decode(String?.self, forKey: .link)
		self.image = try? values.decode(String?.self, forKey: .image)
	}
}
