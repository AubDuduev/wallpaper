
import Foundation

struct DECMainWallpaper {
  
	let favorites: [DECWallpaper]?
	let hot      : [DECWallpaper]?
	let toplive  : [DECWallpaper]?
	let top      : [DECWallpaper]?
  
  enum CodingKeys: String, CodingKey {
		
		case favorites = "categories_favorites"
		case hot       = "categories_hot"
		case toplive   = "categories_toplive"
		case top       = "top_live"
  }
}
extension DECMainWallpaper: Decodable {
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
		
		self.favorites = try? values.decode([DECWallpaper]?.self, forKey:  .favorites)
		self.hot       = try? values.decode([DECWallpaper]?.self, forKey:  .hot      )
		self.toplive   = try? values.decode([DECWallpaper]?.self, forKey:  .toplive  )
		self.top       = try? values.decode([DECWallpaper]?.self, forKey:  .top      )
  }
}
