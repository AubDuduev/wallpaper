//
//  DECReceipt.swift
//  DG.BET
//
//  Created by Senior Developer on 10.06.2020.
//  Copyright © 2020 -=ALEKSANDR=-. All rights reserved.
//
import Foundation

struct DECReceipt {
  
  let receipt    : DECReceiptType!
  let status     : Int!
  let environment: String!
  
  enum CodingKeys: String, CodingKey {
    
    case receipt     = "receipt"
    case status      = "status"
    case environment = "environment"
  }
}
extension DECReceipt: Decodable {
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    self.receipt     = try? values.decode(DECReceiptType?.self, forKey: .receipt)
    self.status      = try? values.decode(Int?.self           , forKey: .status)
    self.environment = try? values.decode(String?.self        , forKey: .environment)
  }
}

//- some : {\n\"receipt\":
//              {\"receipt_type\":\"Production andbox\",
//                \"adam_id\":0,
//                "app_item_id\":0,
//                \"bundle_id\":\"DiegoGarsia.Chats\",
//                \"application_version\":\"1\",
//                \"download_id\":0,
//                \"version_external_identifier\":0,
//                \"receipt_creation_date\":\"2020-06-11 09:41:08 Etc/GMT\",
//                \"receipt_creation_date_ms\":\"1591868468000\",
//                \"receipt_creation_date_pst\":\"2020-06-11 02:41:08 America/Los_Angeles\",
//                \"request_date\":\"2020-06-11 10:04:24 Etc/GMT\",
//                \"request_date_ms\":\"1591869864501\",
//                \"request_date_pst\":\"2020-06-11 03:04:24 America/Los_Angeles\",
//                \"original_purchase_date\":\"2013-08-01 07:00:00 Etc/GMT\",
//                \"original_purchase_date_ms\":\"1375340400000\",
//                \"original_purchase_date_pst\":\"2013-08-01 00:00:00 America/Los_Angeles\",
//                \"original_application_version\":\"1.0\",
//                \n\"in_app\":
//                [\n{\"quantity\":\"1\",
//                                    \"product_id\":\"EventPayVictoriesSeriesMonth\",
//                                    \"transaction_id\":\"1000000677670741\",
//                                    \"original_transaction_id\":\"1000000677670741\",
//                                    \"purchase_date\":\"2020-06-10 18:07:19 Etc/GMT\",
//                                    \"purchase_date_ms\":\"1591812439000\",
//                                    \"purchase_date_pst\":\"2020-06-10 11:07:19 America/Los_Angeles\",
//                                    \"original_purchase_date\":\"2020-06-10 18:07:19 Etc/GMT\",
//                                    \"original_purchase_date_ms\":\"1591812439000\",
//                                    \"original_purchase_date_pst\":\"2020-06-10 11:07:19 America/Los_Angeles\",
//                                    \"is_trial_period\":\"false
//                                     \},
//                                     ]},
//                                    \"status\":0,
//                                    \"environment\":\"Sandbox\
//                                      }
