//
//  DECReceiptType.swift
//  DG.BET
//
//  Created by Senior Developer on 11.06.2020.
//  Copyright © 2020 -=ALEKSANDR=-. All rights reserved.
//
import Foundation

struct DECReceiptType {
  
  let receiptType               : String?
  let adamID                    : String?
  let appItemID                 : String?
  let bundleID                  : String?
  let applicationVersion        : String?
  let downloadId                : String?
  let versionExternalIdentifier : String?
  let receiptCreationDate       : String?
  let receiptCreationDate_ms    : String?
  let receiptCreationDate_pst   : String?
  let requestDate               : String?
  let requestDateMs             : String?
  let requestDatePst            : String?
  let originalPurchaseDate      : String?
  let originalPurchaseDateMs    : String?
  let originalPurchaseDatePst   : String?
  let originalApplicationVersion: String?
  let inApp                     : [DECProduct]?
  
  enum CodingKeys: String, CodingKey {
    
    case receiptType                = "receipt_type"
    case adamID                     = "adam_id"
    case appItemID                  = "app_item_id"
    case bundleID                   = "bundle_id"
    case applicationVersion         = "application_version"
    case downloadId                 = "download_id"
    case versionExternalIdentifier  = "version_external_identifier"
    case receiptCreationDate        = "receipt_creation_date"
    case receiptCreationDate_ms     = "receipt_creation_date_ms"
    case receiptCreationDate_pst    = "receipt_creation_date_pst"
    case requestDate                = "request_date"
    case requestDateMs              = "request_date_ms"
    case requestDatePst             = "request_date_pst"
    case originalPurchaseDate       = "original_purchase_date"
    case originalPurchaseDateMs     = "original_purchase_date_ms"
    case originalPurchaseDatePst    = "original_purchase_date_pst"
    case originalApplicationVersion = "original_application_version"
    case inApp                      = "in_app"
  }
}
extension DECReceiptType: Decodable {
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
    
    receiptType                = try? values.decode(String.self      , forKey: .receiptType               )
    adamID                     = try? values.decode(String.self      , forKey: .adamID                    )
    appItemID                  = try? values.decode(String.self      , forKey: .appItemID                 )
    bundleID                   = try? values.decode(String.self      , forKey: .bundleID                  )
    applicationVersion         = try? values.decode(String.self      , forKey: .applicationVersion        )
    downloadId                 = try? values.decode(String.self      , forKey: .downloadId                )
    versionExternalIdentifier  = try? values.decode(String.self      , forKey: .versionExternalIdentifier )
    receiptCreationDate        = try? values.decode(String.self      , forKey: .receiptCreationDate       )
    receiptCreationDate_ms     = try? values.decode(String.self      , forKey: .receiptCreationDate_ms    )
    receiptCreationDate_pst    = try? values.decode(String.self      , forKey: .receiptCreationDate_pst   )
    requestDate                = try? values.decode(String.self      , forKey: .requestDate               )
    requestDateMs              = try? values.decode(String.self      , forKey: .requestDateMs             )
    requestDatePst             = try? values.decode(String.self      , forKey: .requestDatePst            )
    originalPurchaseDate       = try? values.decode(String.self      , forKey: .originalPurchaseDate      )
    originalPurchaseDateMs     = try? values.decode(String.self      , forKey: .originalPurchaseDateMs    )
    originalPurchaseDatePst    = try? values.decode(String.self      , forKey: .originalPurchaseDatePst   )
    originalApplicationVersion = try? values.decode(String.self      , forKey: .originalApplicationVersion)
    inApp                      = try? values.decode([DECProduct].self, forKey: .inApp                     )
  }
}

