//
//  DECProduct.swift
//  DG.BET
//
//  Created by Senior Developer on 11.06.2020.
//  Copyright © 2020 -=ALEKSANDR=-. All rights reserved.
//
import Foundation

struct DECProduct {
  
  let quantity               : String?
  let productID              : String?
  let transactionID          : String?
  let originalTransactionID  : String?
  let purchaseDate           : String?
  let purchaseDateMs         : String?
  let purchaseDatePst        : String?
  let originalPurchaseDate   : String?
  let originalPurchaseDateMs : String?
  let originalPurchaseDatePst: String?
  let isTrialPeriod          : Bool?
  
  enum CodingKeys: String, CodingKey {
    
    case quantity                = "quantity"
    case productID               = "product_id"
    case transactionID           = "transaction_id"
    case originalTransactionID   = "original_transaction_id"
    case purchaseDate            = "purchase_date"
    case purchaseDateMs          = "purchase_date_ms"
    case purchaseDatePst         = "purchase_date_pst"
    case originalPurchaseDate    = "original_purchase_date"
    case originalPurchaseDateMs  = "original_purchase_date_ms"
    case originalPurchaseDatePst = "original_purchase_date_pst"
    case isTrialPeriod           = "is_trial_period"
  }
}
extension DECProduct: Decodable {
  
  init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
   
    self.quantity                = try? values.decode(String.self, forKey: .quantity               )
    self.productID               = try? values.decode(String.self, forKey: .productID              )
    self.transactionID           = try? values.decode(String.self, forKey: .transactionID          )
    self.originalTransactionID   = try? values.decode(String.self, forKey: .originalTransactionID  )
    self.purchaseDate            = try? values.decode(String.self, forKey: .purchaseDate           )
    self.purchaseDateMs          = try? values.decode(String.self, forKey: .purchaseDateMs         )
    self.purchaseDatePst         = try? values.decode(String.self, forKey: .purchaseDatePst        )
    self.originalPurchaseDate    = try? values.decode(String.self, forKey: .originalPurchaseDate   )
    self.originalPurchaseDateMs  = try? values.decode(String.self, forKey: .originalPurchaseDateMs )
    self.originalPurchaseDatePst = try? values.decode(String.self, forKey: .originalPurchaseDatePst)
    self.isTrialPeriod           = try? values.decode(Bool.self  , forKey: .isTrialPeriod          )
  }
}
