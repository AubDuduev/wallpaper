
import Foundation

struct ENCReceipt: Encodable {
  
  let receiptData     : String!
  let password        = "27ef696fc1764f4fb9be83f4b3a14eee"
  let oldTransactions = false
  
  enum CodingKeys: String, CodingKey {
    
    case password        = "password"
    case receiptData     = "receipt-data"
    case oldTransactions = "exclude-old-transactions"
  }
}
