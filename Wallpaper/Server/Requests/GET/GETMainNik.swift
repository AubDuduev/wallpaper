
import Foundation

class GETMainNik: Requestoble {
	
	private var urls       = URLs()
	private var parameters = URLParameters()
	private var headers    = URLHeaders()
	private var jsonDecode = JSONDecode()
  private var urlBody    = URLBody()
	
	public func type(data: Any?, completion: @escaping ClousureRequest) {
  
		let url = self.urls.create(type: .home)?.URL
    
		//Request
		self.jsonDecode.decode(jsonType: DECMainData.self, url: url, httpMethod: .get) { (decodeResult) in
			//Responce
			switch decodeResult {
				//Error
				case .error(let error):
					completion(.error(error))
				//Susses
				case .json(let object):
          if let homeData = object as? DECMainData {
					completion(.object(homeData))
        }
			}
		}
	}
}
