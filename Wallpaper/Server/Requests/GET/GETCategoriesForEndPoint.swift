
import Foundation

class GETCategoriesForEndPoint: Requestoble {
	
	private var urls       = URLs()
	private var parameters = URLParameters()
	private var headers    = URLHeaders()
	private var jsonDecode = JSONDecode()
	private var urlBody    = URLBody()
	
	public func type(data: Any?, completion: @escaping ClousureRequest) {
	
		let endPath = data as! String
		let url     = self.urls.create(type: .detail(endPath))?.URL
		
		//Request
		self.jsonDecode.decode(jsonType: [DECWallpaper]?.self, url: url, httpMethod: .get) { (decodeResult) in
			//Responce
			switch decodeResult {
				//Error
				case .error(let error):
					completion(.error(error))
				//Susses
				case .json(let object):
					if let wallpapers = object as? [DECWallpaper] {
					completion(.object(wallpapers))
				}
			}
		}
	}
}
