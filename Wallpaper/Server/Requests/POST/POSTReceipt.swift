
import Foundation

class POSTReceipt: Requestoble {
	
	var urls       = URLs()
	var parameters = URLParameters()
	var headers    = URLHeaders()
	var jsonDecode = JSONDecode()
  var urlBody    = URLBody()
	
	public func type(data: Any?, completion: @escaping ClousureRequest) {
  
    guard let receiptURL = Bundle.main.appStoreReceiptURL else { return }
    let receiptString = try? Data(contentsOf: receiptURL).base64EncodedString()
    let body          = self.urlBody.create(type: .Receipt(receiptString))
    let url           = self.urls.create(type: .receipt)?.URL
    let header        = self.headers.create(type: .appJson)
    
		//Request
    self.jsonDecode.decode(jsonType: DECReceipt.self, url: url, header: header, body: body, httpMethod: .post) { (decodeResult) in
			//Responce
			switch decodeResult {
				//Error
				case .error(let error):
					completion(.error(error))
				//Susses
				case .json(let object):
          if let receipt = object as? DECReceipt {
					completion(.object(receipt))
        }
			}
		}
	}
}
