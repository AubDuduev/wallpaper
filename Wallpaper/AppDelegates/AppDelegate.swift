//
//  AppDelegate.swift
//  Wallpaper
//
//  Created by Senior Developer on 29.12.2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	
	private let rootVC        = RootVC()
	private let addMob        = GDAddMob()
	private let setupFirebase = GDSetupFirebase()
  private let appmetrica    = GDSetupAppMetrica()
	private let appLovin      = GDAppLovin()
  private let adColony      = GDAdColony()
  private let lifeCycleApp  = LifeCycleApp()
  private let oneSignal     = GDSetupOneSignal()

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    //Setup GDPurchaсes
    GDPurchaсes.shared.testPurchasesWorking()
    GDPurchacesActions.shared.getCurrentPurchaces()
		//Setup AppLovin
		self.appLovin.setup()
    //Setup DAdColony
    self.adColony.setup()
		//Setup Firebase
		self.setupFirebase.setup()
    //Setup AppMetrica
    self.appmetrica.activate()
    //Setup OneSignal
    self.oneSignal.setup(launchOptions: launchOptions)
		//Setup addMob
		self.addMob.setup()
		//Set rootviewcontroller
		self.rootVC.set(window: self.window)
		return true
	}
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data){
    //setup notification Appmetrica
    self.appmetrica.setDeviceToken(deviceToken: deviceToken)
  }
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
    //setup YandexMetrica
    self.appmetrica.handlePushNotification(userInfo)
    //Print full message.
    print(userInfo, "Remote notification Background")
  }
  func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    //Setup YandexMetrica
    self.appmetrica.handlePushNotification(userInfo)
    // Print full message.
    print(userInfo, "Remote notification Forenfgraund and Bacrgarun")
    completionHandler(.newData)
  }
  func applicationDidEnterBackground(_ application: UIApplication) {
    self.lifeCycleApp.applicationDidEnterBackground(application)
  }
  func applicationDidBecomeActive(_ application: UIApplication) {
    self.lifeCycleApp.applicationDidBecomeActive(application)
  }
}

