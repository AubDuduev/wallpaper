import UIKit

class AnimationNotification: VMManager {
  
  //MARK: - Public variable
  public var VM: NotificationViewModal!
  
  
}
//MARK: - Initial
extension AnimationNotification {
  
  //MARK: - Inition
  convenience init(viewModal: NotificationViewModal) {
    self.init()
    self.VM = viewModal
  }
}

