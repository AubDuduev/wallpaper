
import UIKit

class RouterNotification: VMManager {
  
  //MARK: - Public variable
  public var VM: NotificationViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .NavBarVC:
        self.pushNavBarVC()
    }
  }
  
  enum Push {
    case NavBarVC
  }
}
//MARK: - Private functions
extension RouterNotification {
  
  private func pushNavBarVC(){
    DispatchQueue.main.async { [weak self] in
      guard let self = self else { return }
      let navBarVC = self.VM.VC.getVCForID(storyboardID     : .NavBar,
                                           vcID             : .NavBarVC,
                                           transitionStyle  : .crossDissolve,
                                           presentationStyle: .fullScreen) as! NavBarViewController
      UIApplication.shared.windows.first?.rootViewController = navBarVC
      UIApplication.shared.windows.first?.makeKey()
      self.VM.VC.present(navBarVC, animated: true)
    }
  }
}
//MARK: - Initial
extension RouterNotification {
  
  //MARK: - Inition
  convenience init(viewModal: NotificationViewModal) {
    self.init()
    self.VM = viewModal
  }
}



