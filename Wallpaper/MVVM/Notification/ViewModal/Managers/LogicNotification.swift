import Foundation

class LogicNotification: VMManager {
  
  //MARK: - Public variable
  public var VM: NotificationViewModal!
  
  public func getIt(){
    self.VM.notification.setup()
    self.VM.notification.requestUser { [weak self] in
      guard let self = self else { return }
      DispatchQueue.main.async { [weak self] in
        guard let self = self else { return }
        self.VM.VC.dismiss(animated: true)
      }
    }
  }
}
//MARK: - Initial
extension LogicNotification {
  
  //MARK: - Inition
  convenience init(viewModal: NotificationViewModal) {
    self.init()
    self.VM = viewModal
  }
}
