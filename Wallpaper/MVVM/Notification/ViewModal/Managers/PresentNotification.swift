
import UIKit

class PresentNotification: VMManager {
  
  //MARK: - Public variable
  public var VM: NotificationViewModal!
  
  
}
//MARK: - Initial
extension PresentNotification {
  
  //MARK: - Inition
  convenience init(viewModal: NotificationViewModal) {
    self.init()
    self.VM = viewModal
  }
}

