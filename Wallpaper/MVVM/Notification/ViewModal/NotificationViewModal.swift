
import Foundation

class NotificationViewModal: VMManagers {
	
	public var notificationModal: NotificationModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers     : NotificationManagers!
  public var VC           : NotificationViewController!
  public let notification = GDSetupNotification()
  
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension NotificationViewModal {
  
  convenience init(viewController: NotificationViewController) {
    self.init()
    self.VC       = viewController
    self.managers = NotificationManagers(viewModal: self)
  }
}
