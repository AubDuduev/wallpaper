//
//  ActionsNotificationViewController.swift
//  Wallpaper
//
//  Created by Senior Developer on 09.01.2021.
//
import UIKit

extension NotificationViewController {
  
  @IBAction func getItButton(button: UIButton){
    self.viewModal.managers.logic.getIt()
  }
}
