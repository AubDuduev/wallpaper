import UIKit

class NotificationViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: NotificationViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
   
  }
  private func initViewModal(){
    self.viewModal = NotificationViewModal(viewController: self)
  }
}
