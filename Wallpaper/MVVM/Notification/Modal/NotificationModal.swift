
import UIKit

enum NotificationModal {
	
	case loading
	case getData
	case errorHandler
	case presentData(Data)
	
	struct Data {
		
	
	}
}

