import UIKit

class LiveViewController: UIViewController {
	
	//MARK: - ViewModel
	public var viewModal: LiveViewModal!
	
	//MARK: - Public variable
	
	
	//MARK: - Outlets
	@IBOutlet weak var liveTable: LiveTable!
	
	//MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initViewModal()
		self.viewModal.viewDidLoad()
	}
	private func initViewModal(){
		self.viewModal = LiveViewModal(viewController: self)
	}
}
