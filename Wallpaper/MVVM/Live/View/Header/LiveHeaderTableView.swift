
import UIKit

class LiveHeaderTableView: UITableViewHeaderFooterView, LoadNidoble {
  
  private var viewModal: LiveViewModal!
	
	public var wallpaper: DECWallpaper!
	
	@IBOutlet weak var categoryNameLabel: UILabel!
  
  public func configure(viewModal: LiveViewModal?, wallpaper: DECWallpaper?){
		self.viewModal = viewModal
		self.wallpaper = wallpaper
		self.viewModal.managers.present.title(header: self)
  }
  @IBAction func pushDetailButton(button: UIButton){
    self.viewModal.managers.logic.pushDetailVC(wallpaper: self.wallpaper)
  }
}
