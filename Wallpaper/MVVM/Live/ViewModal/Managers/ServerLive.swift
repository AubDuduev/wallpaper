import UIKit

class ServerLive: VMManager {
  
  //MARK: - Public variable
  public var VM: LiveViewModal!
  
	public func getLive(completion: @escaping Clousure<[DECWallpaper]?>){
		//Request
		self.VM.server.request(requestType: GETLive()) { [weak self]  (serverResult) in
			guard let self = self else { return }
			//Response
			switch serverResult {
				case .error(let error):
					guard let error = error else { return }
					print("Error server data: class: ServerLive ->, function: getLive -> data: [DECWallpaper] ->, description: ", error.localizedDescription)
					AlertEK.dеfault(title: .error, message: .noJSON){
						self.getLive(completion: completion)
					}
				//Susses
				case .object(let object):
					let wallpapers = object as? [DECWallpaper]
					completion(wallpapers)
					print("Succesful data: class: ServerLive ->, function: getLive ->, data: [DECWallpaper]")
					
			}
		}
	}
}
//MARK: - Initial
extension ServerLive {
  
  //MARK: - Inition
  convenience init(viewModal: LiveViewModal) {
    self.init()
    self.VM = viewModal
  }
}


