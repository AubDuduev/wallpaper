import Foundation
import UIKit

class LogicLive: VMManager {
  
  //MARK: - Public variable
  public var VM: LiveViewModal!
  
  public func prepareForReuse(image: UIImageView){
    image.isSkeletonable = true
    image.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
  }
  public func pushDetailVC(wallpaper: DECWallpaper){
    guard let detailGetData = DetailGetData(wallpaper  : wallpaper,
                                            dataType   : .server,
                                            detailTitle: .live) else { return }
    self.VM.managers.router.push(.Detail(detailGetData))
  }
}
//MARK: - Initial
extension LogicLive {
  
  //MARK: - Inition
  convenience init(viewModal: LiveViewModal) {
    self.init()
    self.VM = viewModal
  }
}
