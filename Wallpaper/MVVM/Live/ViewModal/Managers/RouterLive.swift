
import UIKit

class RouterLive: VMManager {
	
	//MARK: - Public variable
	public var VM: LiveViewModal!
	
	public func push(_ type: Push){
		
		switch type {
			case .Detail(let detailGetData):
				self.pushDetailVC(detailGetData: detailGetData)
		}
	}
	
	enum Push {
		case Detail(DetailGetData)
	}
}
//MARK: - Private functions
extension RouterLive {
	
	private func pushDetailVC(detailGetData: DetailGetData){
		let detailVC = self.VM.VC.getVCForID(storyboardID     : .Detail,
																				 vcID             : .DetailVC,
																				 transitionStyle  : .crossDissolve,
																				 presentationStyle: .fullScreen) as! DetailViewController
		detailVC.detailGetData = detailGetData
		self.VM.VC.present(detailVC, animated: true)
	}
}
//MARK: - Initial
extension RouterLive {
	
	//MARK: - Inition
	convenience init(viewModal: LiveViewModal) {
		self.init()
		self.VM = viewModal
	}
}



