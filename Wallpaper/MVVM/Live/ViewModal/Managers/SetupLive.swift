
import UIKit
import NVActivityIndicatorView

class SetupLive: VMManager {
  
  //MARK: - Public variable
  public var VM: LiveViewModal!
  
	//MARK: - Public LiveViewController
	public func startLoading(){
		let rect = CGRect(x: 0, y: 0, width: 250, height: 250)
		self.VM.loading = NVActivityIndicatorView(frame  : rect,
																							type   : .ballBeat,
																							color  : .black,
																							padding: 10)
		self.VM.loading.center = self.VM.VC.view.center
		self.VM.VC.view.addSubview(self.VM.loading)
		self.VM.loading.startAnimating()
	}
	public func liveTable(){
		self.VM.VC.liveTable.viewModal = self.VM
	}
	
	//MARK: - Public LiveTableCell
	public func commonView(cell: LiveTableCell){
		cell.commonView.cornerRadius(5, true)
		cell.wallpaperImageView.cornerRadius(5, true)
	}
}
//MARK: - Initial
extension SetupLive {
  
  //MARK: - Inition
  convenience init(viewModal: LiveViewModal) {
    self.init()
    self.VM = viewModal
  }
}


