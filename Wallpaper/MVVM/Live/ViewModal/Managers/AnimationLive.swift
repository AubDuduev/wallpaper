import UIKit

class AnimationLive: VMManager {
  
  //MARK: - Public variable
  public var VM: LiveViewModal!
  
  
}
//MARK: - Initial
extension AnimationLive {
  
  //MARK: - Inition
  convenience init(viewModal: LiveViewModal) {
    self.init()
    self.VM = viewModal
  }
}

