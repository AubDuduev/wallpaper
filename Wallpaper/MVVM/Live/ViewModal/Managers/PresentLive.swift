
import UIKit

class PresentLive: VMManager {
  
  //MARK: - Public variable
  public var VM: LiveViewModal!
	
	public func table(liveModalData: LiveModal.Data){
		self.VM.VC.liveTable.wallpapers = liveModalData.wallpapers
		self.VM.VC.liveTable.tableView?.reloadData()
	}
  
  //MARK: - Present LiveTableCell
  public func image(cell: LiveTableCell){
    guard let image = cell.wallpaper?.image else { return }
    guard let url   = self.VM.server.urls.get(type: .store(image)).URL else { return }
    cell.wallpaperImageView.isSkeletonable = true
    cell.wallpaperImageView.skeletonCornerRadius = 5
    cell.wallpaperImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
      cell.wallpaperImageView.sd_setImage(with: url) { (image, error, _, url) in
        
        if let error = error {
          
          print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
        } else {
          //cell.wallpaperImageView.image = image!.sd_flippedImage(withHorizontal: true, vertical: true)
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            cell.wallpaperImageView.hideSkeleton()
          }
        }
      }
    }
  }
	//MARK: - Present LiveHeaderTableView
	public func title(header: LiveHeaderTableView){
		header.categoryNameLabel.text = header.wallpaper.title
	}
}
//MARK: - Initial
extension PresentLive {
  
  //MARK: - Inition
  convenience init(viewModal: LiveViewModal) {
    self.init()
    self.VM = viewModal
  }
}

