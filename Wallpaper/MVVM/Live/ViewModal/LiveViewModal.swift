
import NVActivityIndicatorView
import Foundation

class LiveViewModal: VMManagers {
	
	public var liveModal: LiveModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
	public var loading      : NVActivityIndicatorView!
  public var managers     : LiveManagers!
  public var VC           : LiveViewController!
	public let server       = Server()
	public let errorHandler = ErrorHandlerLive()
  
	public func viewDidLoad() {
		self.managers.setup.liveTable()
		self.commonLogic()
	}
	public func commonLogic(){
		
		switch liveModal {
			//1 - Загрузка
			case .loading:
				self.managers.setup.startLoading()
				self.liveModal = .getData
				
			//2 - Получение данных
			case .getData:
				//Делаем запрос на сервер для получения данных
				self.managers.server.getLive { [weak self] (wallpapers) in
					self?.liveModal = .errorHandler(wallpapers)
				}
				
			//3 - Обработка ошибок запроса
			case .errorHandler(let wallpapers):
				//Обрабатываем на ошибки полученные данные
				guard self.errorHandler.check(wallpapers: wallpapers) else { return }
				let liveModalData = LiveModal.Data(wallpapers: wallpapers!)
				self.liveModal = .presentData(liveModalData)
				
			//4 - Отоброжение данных на kонтроллере
			case .presentData(let liveModalData):
				self.managers.present.table(liveModalData: liveModalData)
				self.loading.stopAnimating()
		}
	}
}
//MARK: - Initial
extension LiveViewModal {
  
  convenience init(viewController: LiveViewController) {
    self.init()
    self.VC       = viewController
    self.managers = LiveManagers(viewModal: self)
  }
}
