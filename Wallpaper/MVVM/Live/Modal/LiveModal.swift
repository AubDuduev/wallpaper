
import UIKit

enum LiveModal {
	
	case loading
	case getData
	case errorHandler([DECWallpaper]?)
	case presentData(Data)
	
	struct Data {
		
		let wallpapers: [DECWallpaper]!
		
		init(wallpapers: [DECWallpaper]){
			self.wallpapers = wallpapers
		}
	}
}

