
import Foundation

class LiveManagers: VMManagers {
  
  let setup    : SetupLive!
  let server   : ServerLive!
  let present  : PresentLive!
  let logic    : LogicLive!
  let animation: AnimationLive!
  let router   : RouterLive!
  
  init(viewModal: LiveViewModal) {
    
    self.setup     = SetupLive(viewModal: viewModal)
    self.server    = ServerLive(viewModal: viewModal)
    self.present   = PresentLive(viewModal: viewModal)
    self.logic     = LogicLive(viewModal: viewModal)
    self.animation = AnimationLive(viewModal: viewModal)
    self.router    = RouterLive(viewModal: viewModal)
  }
}

