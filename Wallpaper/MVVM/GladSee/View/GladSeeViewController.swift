import UIKit

class GladSeeViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: GladSeeViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
   
  }
  private func initViewModal(){
    self.viewModal = GladSeeViewModal(viewController: self)
  }
}
