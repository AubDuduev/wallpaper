import UIKit

class AnimationGladSee: VMManager {
  
  //MARK: - Public variable
  public var VM: GladSeeViewModal!
  
  
}
//MARK: - Initial
extension AnimationGladSee {
  
  //MARK: - Inition
  convenience init(viewModal: GladSeeViewModal) {
    self.init()
    self.VM = viewModal
  }
}

