import Foundation

class LogicGladSee: VMManager {
  
  //MARK: - Public variable
  public var VM: GladSeeViewModal!
  
  
}
//MARK: - Initial
extension LogicGladSee {
  
  //MARK: - Inition
  convenience init(viewModal: GladSeeViewModal) {
    self.init()
    self.VM = viewModal
  }
}
