
import Foundation

class GladSeeViewModal: VMManagers {
	
	public var gladSeeModal: GladSeeModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers: GladSeeManagers!
  public var VC      : GladSeeViewController!
  
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
  
}
//MARK: - Initial
extension GladSeeViewModal {
  
  convenience init(viewController: GladSeeViewController) {
    self.init()
    self.VC       = viewController
    self.managers = GladSeeManagers(viewModal: self)
  }
}
