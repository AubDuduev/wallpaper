
import UIKit

enum GladSeeModal {
	
	case loading
	case getData
	case errorHandler
	case presentData(Data)

}

