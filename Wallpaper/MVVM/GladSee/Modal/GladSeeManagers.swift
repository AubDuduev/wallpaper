
import Foundation

class GladSeeManagers: VMManagers {
  
  let setup    : SetupGladSee!
  let server   : ServerGladSee!
  let present  : PresentGladSee!
  let logic    : LogicGladSee!
  let animation: AnimationGladSee!
  let router   : RouterGladSee!
  
  init(viewModal: GladSeeViewModal) {
    
    self.setup     = SetupGladSee(viewModal: viewModal)
    self.server    = ServerGladSee(viewModal: viewModal)
    self.present   = PresentGladSee(viewModal: viewModal)
    self.logic     = LogicGladSee(viewModal: viewModal)
    self.animation = AnimationGladSee(viewModal: viewModal)
    self.router    = RouterGladSee(viewModal: viewModal)
  }
}

