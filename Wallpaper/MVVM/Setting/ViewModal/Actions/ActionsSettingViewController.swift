
import UIKit

extension SettingViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true)
  }
	@IBAction func shareButton(button: UIButton){
		self.viewModal.managers.logic.share()
	}
	@IBAction func useConditionsButton(button: UIButton){
		self.viewModal.managers.logic.useConditionsButton()
	}
	@IBAction func instagramButton(button: UIButton){
		self.viewModal.managers.logic.instagramButton()
	}
	@IBAction func privacyButton(button: UIButton){
		self.viewModal.managers.logic.privacyButton()
	}
	@IBAction func supportButton(button: UIButton){
		self.viewModal.managers.logic.support()
	}
	@IBAction func estimateButton(button: UIButton){
		self.viewModal.managers.logic.estimate()
	}
  @IBAction func pushPurchacesButton(button: UIButton){
    self.viewModal.managers.router.push(.PurchacesVC)
  }
  @IBAction func purchacesRestoreButton(button: UIButton){
    GDPurchacesActions.shared.restored()
  }
}
