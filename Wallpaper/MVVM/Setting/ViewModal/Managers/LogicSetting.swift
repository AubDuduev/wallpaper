import Foundation
import UIKit
import MessageUI

class LogicSetting: VMManager {
  
  //MARK: - Public variable
  public var VM: SettingViewModal!
  
	public func share(){
		let controller = UIActivityViewController(activityItems: ["Поделись с друзями"], applicationActivities: nil)
		self.VM.VC.present(controller, animated: true, completion: nil)
	}
	public func useConditionsButton(){
		let webContentData = WebContentData(urlString: .termUse, title: .useTerms)
		self.VM.managers.router.push(.WebContentVC(webContentData))
	}
	public func instagramButton(){
		let webContentData = WebContentData(urlString: .instagramm, title: .Instagramm)
		self.VM.managers.router.push(.WebContentVC(webContentData))
	}
	public func privacyButton(){
		let webContentData = WebContentData(urlString: .privacy, title: .privacy)
		self.VM.managers.router.push(.WebContentVC(webContentData))
	}
	public func support(){
		if MFMailComposeViewController.canSendMail() {
			let composer = MFMailComposeViewController()
			composer.mailComposeDelegate = self.VM.VC
			composer.setToRecipients([URLString.Url.support.rawValue])
			composer.setSubject("Приложение ORANGE")
			composer.setMessageBody("Хотел бы обратится к вам за помощью", isHTML: false)
			self.VM.VC.present(composer, animated: true)
		} else {
			AlertEK.dеfault(title: .information, message: .noMailCompose)
		}
	}
	public func estimate(){
		if let detailGetData = DetailGetData(wallpaper  : nil,
                                         dataType   : .local,
                                         detailTitle: .other) {
			self.VM.managers.router.push(.Detail(detailGetData))
		}
	}
}
//MARK: - Initial
extension LogicSetting {
  
  //MARK: - Inition
  convenience init(viewModal: SettingViewModal) {
    self.init()
    self.VM = viewModal
  }
}
