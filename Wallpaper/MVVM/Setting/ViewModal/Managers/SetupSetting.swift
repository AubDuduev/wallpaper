
import UIKit
import AnimatedGradientView

class SetupSetting: VMManager {
  
  //MARK: - Public variable
  public var VM: SettingViewModal!
  
	public func iconsButtonView(){
		let animatedGradient = AnimatedGradientView(frame: self.VM.VC.iconsButtonView.bounds)
		animatedGradient.direction = .up
		animatedGradient.colors = [[#colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1803921569, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8564737541), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.95)]]
		self.VM.VC.iconsButtonView.insertSubview(animatedGradient, belowSubview: self.VM.VC.iconsButtonStackView)
		self.VM.VC.iconsButtonView.cornerRadius(25, true)
  }
	public func advertisingView(){
		self.VM.VC.advertisingView.cornerRadius(5, true)
	}
	
}
//MARK: - Initial
extension SetupSetting {
  
  //MARK: - Inition
  convenience init(viewModal: SettingViewModal) {
    self.init()
    self.VM = viewModal
  }
}


