
import UIKit

class RouterSetting: VMManager {
	
	//MARK: - Public variable
	public var VM: SettingViewModal!
	
	public func push(_ type: Push){
		
		switch type {
			case .WebContentVC( let webContentData):
				self.pushWebContentVC(webContentData: webContentData)
			case .Detail(let detailGetData):
				self.pushDetailVC(detailGetData: detailGetData)
      case .PurchacesVC:
        self.pushPurchacesVC()
		}
	}
	
	enum Push {
		case WebContentVC(WebContentData)
		case Detail(DetailGetData)
    case PurchacesVC
	}
}
//MARK: - Private functions
extension RouterSetting {
	
	private func pushWebContentVC(webContentData: WebContentData){
		let webContentVC = self.VM.VC.getVCForID(storyboardID     : .WebContent,
																						 vcID             : .WebContentVC,
																						 transitionStyle  : .crossDissolve,
																						 presentationStyle: .fullScreen) as! WebContentViewController
		webContentVC.webContentData = webContentData
		self.VM.VC.present(webContentVC, animated: true)
	}
	private func pushDetailVC(detailGetData: DetailGetData){
		let detailVC = self.VM.VC.getVCForID(storyboardID     : .Detail,
																				 vcID             : .DetailVC,
																				 transitionStyle  : .crossDissolve,
																				 presentationStyle: .fullScreen) as! DetailViewController
		detailVC.detailGetData = detailGetData
		self.VM.VC.present(detailVC, animated: true)
	}
  private func pushPurchacesVC(){
    let purchacesVC = self.VM.VC.getVCForID(storyboardID     : .Purchaces,
                                            vcID             : .PurchacesVC,
                                            transitionStyle  : .crossDissolve,
                                            presentationStyle: .fullScreen) as! PurchacesViewController
    self.VM.VC.present(purchacesVC, animated: true)
  }
}
//MARK: - Initial
extension RouterSetting {
	
	//MARK: - Inition
	convenience init(viewModal: SettingViewModal) {
		self.init()
		self.VM = viewModal
	}
}



