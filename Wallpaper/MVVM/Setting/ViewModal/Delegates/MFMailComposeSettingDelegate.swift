
import UIKit
import MessageUI

extension SettingViewController: MFMailComposeViewControllerDelegate {
  
	func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
		
		controller.dismiss(animated: true)
	}
}
