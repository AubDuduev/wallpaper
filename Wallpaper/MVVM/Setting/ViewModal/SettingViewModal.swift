
import Foundation

class SettingViewModal: VMManagers {
  
  //MARK: - Public variable
  public var managers: SettingManagers!
  public var VC      : SettingViewController!
  
	public func viewDidLoad() {
		self.managers.setup.iconsButtonView()
	}
	public func viewDidAppear() {
    self.managers.setup.advertisingView()
	}
}
//MARK: - Initial
extension SettingViewModal {
  
  convenience init(viewController: SettingViewController) {
    self.init()
    self.VC       = viewController
    self.managers = SettingManagers(viewModal: self)
  }
}
