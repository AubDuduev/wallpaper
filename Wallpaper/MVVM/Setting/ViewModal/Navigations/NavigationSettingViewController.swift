
import UIKit

extension SettingViewController {
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    switch segue.ID() {
      case .SettingVC_NativeAddVC:
        let nativeAdvertisingVC = (segue.destination as? NativeAdvertisingViewController)
        nativeAdvertisingVC?.presentationType = .Setting
      default:
      break
    }
  }
}
