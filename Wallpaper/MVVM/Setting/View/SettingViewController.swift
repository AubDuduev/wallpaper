import UIKit

class SettingViewController: UIViewController {
	
	//MARK: - ViewModel
	public var viewModal: SettingViewModal!
	
	//MARK: - Public variable
	
	
	//MARK: - Outlets
	@IBOutlet weak var iconsButtonView      : UIView!
	@IBOutlet weak var iconsButtonStackView : UIStackView!
	@IBOutlet weak var advertisingView      : UIView!

	//MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initViewModal()
		self.viewModal.viewDidLoad()
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		self.viewModal.viewDidAppear()
	}
	private func initViewModal(){
		self.viewModal = SettingViewModal(viewController: self)
	}
}
