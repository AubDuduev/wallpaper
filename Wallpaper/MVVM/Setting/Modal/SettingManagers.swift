
import Foundation

class SettingManagers: VMManagers {
  
  let setup    : SetupSetting!
  let server   : ServerSetting!
  let present  : PresentSetting!
  let logic    : LogicSetting!
  let animation: AnimationSetting!
  let router   : RouterSetting!
  
  init(viewModal: SettingViewModal) {
    
    self.setup     = SetupSetting(viewModal: viewModal)
    self.server    = ServerSetting(viewModal: viewModal)
    self.present   = PresentSetting(viewModal: viewModal)
    self.logic     = LogicSetting(viewModal: viewModal)
    self.animation = AnimationSetting(viewModal: viewModal)
    self.router    = RouterSetting(viewModal: viewModal)
  }
}

