import UIKit

class AnimationSaveWalpaper: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveWalpaperViewModal!
  
  
}
//MARK: - Initial
extension AnimationSaveWalpaper {
  
  //MARK: - Inition
  convenience init(viewModal: SaveWalpaperViewModal) {
    self.init()
    self.VM = viewModal
  }
}

