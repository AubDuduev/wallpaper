
import UIKit
import AnimatedGradientView

class SetupSaveWalpaper: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveWalpaperViewModal!
  
  public func arrayGradienViews(){
    let animatedGradient1 = AnimatedGradientView(frame: self.VM.VC.arrayGradienViews.first!.bounds)
    animatedGradient1.direction = .left
    animatedGradient1.colors = [[#colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1803921569, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8564737541), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.95)]]
    let animatedGradient2 = AnimatedGradientView(frame: self.VM.VC.arrayGradienViews.first!.bounds)
    animatedGradient2.direction = .left
    animatedGradient2.colors = [[#colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1803921569, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8564737541), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.95)]]
    let animatedGradient3 = AnimatedGradientView(frame: self.VM.VC.arrayGradienViews.first!.bounds)
    animatedGradient3.direction = .left
    animatedGradient3.colors = [[#colorLiteral(red: 0.1568627451, green: 0.1568627451, blue: 0.1803921569, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8564737541), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.95)]]
    self.VM.VC.arrayGradienViews[0].insertSubview(animatedGradient1, at: 0)
    self.VM.VC.arrayGradienViews[1].insertSubview(animatedGradient2, at: 0)
    self.VM.VC.arrayGradienViews[2].insertSubview(animatedGradient3, at: 0)
    let corner = CGFloat(75 / 2)
    self.VM.VC.arrayGradienViews.forEach({ $0.cornerRadius(corner, true)})
  }
}
//MARK: - Initial
extension SetupSaveWalpaper {
  
  //MARK: - Inition
  convenience init(viewModal: SaveWalpaperViewModal) {
    self.init()
    self.VM = viewModal
  }
}


