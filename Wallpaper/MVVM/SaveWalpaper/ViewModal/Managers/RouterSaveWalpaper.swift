
import UIKit

class RouterSaveWalpaper: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveWalpaperViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .SaveEndVC:
        self.pushSaveEndVC()
    }
  }
  
  enum Push {
    case SaveEndVC
  }
}
//MARK: - Private functions
extension RouterSaveWalpaper {
  
  private func pushSaveEndVC(){
    let saveEndVC = self.VM.VC.getVCForID(storyboardID     : .SaveEnd,
                                          vcID             : .SaveEndVC,
                                          transitionStyle  : .crossDissolve,
                                          presentationStyle: .fullScreen) as! SaveEndViewController
    self.VM.VC.present(saveEndVC, animated: true)
  }
}
//MARK: - Initial
extension RouterSaveWalpaper {
  
  //MARK: - Inition
  convenience init(viewModal: SaveWalpaperViewModal) {
    self.init()
    self.VM = viewModal
  }
}



