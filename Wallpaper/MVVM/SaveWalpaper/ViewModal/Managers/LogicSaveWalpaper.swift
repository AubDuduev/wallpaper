import Foundation

class LogicSaveWalpaper: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveWalpaperViewModal!
  
  public func pushSaveEndVC(){
    self.VM.managers.router.push(.SaveEndVC)
  }
}
//MARK: - Initial
extension LogicSaveWalpaper {
  
  //MARK: - Inition
  convenience init(viewModal: SaveWalpaperViewModal) {
    self.init()
    self.VM = viewModal
  }
}
