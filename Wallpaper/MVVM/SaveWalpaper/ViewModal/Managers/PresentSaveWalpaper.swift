
import UIKit

class PresentSaveWalpaper: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveWalpaperViewModal!
  
  
}
//MARK: - Initial
extension PresentSaveWalpaper {
  
  //MARK: - Inition
  convenience init(viewModal: SaveWalpaperViewModal) {
    self.init()
    self.VM = viewModal
  }
}

