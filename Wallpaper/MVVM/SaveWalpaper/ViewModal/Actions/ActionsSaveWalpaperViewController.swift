//
//  ActionsSaveWalpaperViewController.swift
//  Wallpaper
//
//  Created by Senior Developer on 08.01.2021.
//
import UIKit

extension SaveWalpaperViewController {
  
  @IBAction func pushSaveEndVCButton(button: UIButton){
    self.viewModal.managers.logic.pushSaveEndVC()
  }
}
