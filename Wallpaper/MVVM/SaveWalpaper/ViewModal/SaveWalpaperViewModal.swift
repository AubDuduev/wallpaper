
import Foundation

class SaveWalpaperViewModal: VMManagers {
	
	public var saveWalpaperModal: SaveWalpaperModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers: SaveWalpaperManagers!
  public var VC      : SaveWalpaperViewController!
  
  public func viewDidLoad() {
    self.managers.setup.arrayGradienViews()
  }
  
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension SaveWalpaperViewModal {
  
  convenience init(viewController: SaveWalpaperViewController) {
    self.init()
    self.VC       = viewController
    self.managers = SaveWalpaperManagers(viewModal: self)
  }
}
