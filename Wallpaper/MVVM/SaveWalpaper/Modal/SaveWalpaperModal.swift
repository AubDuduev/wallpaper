
import UIKit

enum SaveWalpaperModal {
	
	case loading
	case getData
	case errorHandler
	case presentData(Data)
	
}

