
import Foundation

class SaveWalpaperManagers: VMManagers {
  
  let setup    : SetupSaveWalpaper!
  let server   : ServerSaveWalpaper!
  let present  : PresentSaveWalpaper!
  let logic    : LogicSaveWalpaper!
  let animation: AnimationSaveWalpaper!
  let router   : RouterSaveWalpaper!
  
  init(viewModal: SaveWalpaperViewModal) {
    
    self.setup     = SetupSaveWalpaper(viewModal: viewModal)
    self.server    = ServerSaveWalpaper(viewModal: viewModal)
    self.present   = PresentSaveWalpaper(viewModal: viewModal)
    self.logic     = LogicSaveWalpaper(viewModal: viewModal)
    self.animation = AnimationSaveWalpaper(viewModal: viewModal)
    self.router    = RouterSaveWalpaper(viewModal: viewModal)
  }
}

