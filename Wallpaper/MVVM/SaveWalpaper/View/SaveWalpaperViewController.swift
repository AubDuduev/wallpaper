import UIKit

class SaveWalpaperViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: SaveWalpaperViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
  @IBOutlet var arrayGradienViews: [UIView]!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  private func initViewModal(){
    self.viewModal = SaveWalpaperViewModal(viewController: self)
  }
}
