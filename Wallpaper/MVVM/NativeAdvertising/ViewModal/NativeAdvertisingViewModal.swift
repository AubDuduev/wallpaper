
import Foundation
import GoogleMobileAds
import SnapKit

class NativeAdvertisingViewModal: VMManagers {
  
  public var nativeAdvertisingModal: NativeAdvertisingModal = .loading {
    didSet {
      self.commonLogic()
    }
  }
  
  //MARK: - Public variable
  public var managers    : NativeAdvertisingManagers!
  public var VC          : NativeAdvertisingViewController!
  public var adLoader    = GADAdLoader()
  public let gdAddMob    = GDAddMob()
  public var clousureDissmiss: ClousureEmpty!
  
  public func viewDidLoad() {
    self.managers.setup.addedView()
    self.managers.setup.adLoaderInit()
    self.managers.logic.dissmiss()
    self.managers.logic.advertising()

  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.constraintNativeView()
  }
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension NativeAdvertisingViewModal {
  
  convenience init(viewController: NativeAdvertisingViewController) {
    self.init()
    self.VC       = viewController
    self.managers = NativeAdvertisingManagers(viewModal: self)
  }
}
