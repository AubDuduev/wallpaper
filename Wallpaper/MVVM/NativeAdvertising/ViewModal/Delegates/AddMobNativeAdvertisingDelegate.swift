
import UIKit
import GoogleMobileAds

extension NativeAdvertisingViewController: GADVideoControllerDelegate {

	func videoControllerDidEndVideoPlayback(_ videoController: GADVideoController) {
	 
	}
}

extension NativeAdvertisingViewController: GADAdLoaderDelegate {

	func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
		print("\(adLoader) failed with error: \(error.localizedDescription)")
	}
}

extension NativeAdvertisingViewController: GADUnifiedNativeAdLoaderDelegate {

	func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
		// Set ourselves as the native ad delegate to be notified of native ad events.
		nativeAd.delegate = self
		// Note: this should always be done after populating the ad views.
    self.viewModal.managers.logic.nativeAddsDelegate(nativeAd: nativeAd)
    print("adLoader")
	}
}

// MARK: - GADUnifiedNativeAdDelegate implementation
extension NativeAdvertisingViewController: GADUnifiedNativeAdDelegate {

	func nativeAdDidRecordClick(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdDidRecordImpression(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdWillPresentScreen(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdWillDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdDidDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdWillLeaveApplication(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}
}
