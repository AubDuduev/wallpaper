import Foundation
import GoogleMobileAds

class LogicNativeAdvertising: VMManager {
  
  //MARK: - Public variable
  public var VM: NativeAdvertisingViewModal!
  
  public func dissmiss(){
    self.VM.VC.addsHomeCenterView.dissmiss = { [weak self] in
      guard let self = self else { return }
      self.VM.VC.dissmissAdds?()
    }
  }
  
  public func advertising(){
    self.VM.VC.nativeAdView.advertising = { url in
      OpenURL.shared.openString(urlString: url)
    }
    
    self.VM.VC.nativeAdCellView.advertising = { url in
      OpenURL.shared.openString(urlString: url)
    }
    
    self.VM.VC.nativeAdNoImage.advertising = { url in
      OpenURL.shared.openString(urlString: url)
    }
    
    self.VM.VC.settingAdView.advertising = { url in
      OpenURL.shared.openString(urlString: url)
    }
    
    self.VM.VC.addsHomeBottomView.advertising = { url in
      OpenURL.shared.openString(urlString: url)
    }
    
    self.VM.VC.addsHomeCenterView.advertising = { url in
      OpenURL.shared.openString(urlString: url)
    }
    
    
  }
  public func nativeAddsDelegate(nativeAd: GADUnifiedNativeAd){
    
    switch self.VM.VC.presentationType {
      case .Controller:
        self.VM.VC.nativeAdView.setup(nativeAd: nativeAd)
      case .CollectionCell:
        self.VM.VC.nativeAdCellView.setup(nativeAd: nativeAd)
      case .NoImage:
        self.VM.VC.nativeAdNoImage.setup(nativeAd: nativeAd)
      case .Setting:
        self.VM.VC.settingAdView.setup(nativeAd: nativeAd)
      case .HomeBottomView:
        self.VM.VC.addsHomeBottomView.setup(nativeAd: nativeAd)
      case .HomeCenterView:
        self.VM.VC.addsHomeCenterView.setup(nativeAd: nativeAd)
    }
  }
}
//MARK: - Initial
extension LogicNativeAdvertising {
  
  //MARK: - Inition
  convenience init(viewModal: NativeAdvertisingViewModal) {
    self.init()
    self.VM = viewModal
  }
}
