
import UIKit

class PresentNativeAdvertising: VMManager {
  
  //MARK: - Public variable
  public var VM: NativeAdvertisingViewModal!
  
  
}
//MARK: - Initial
extension PresentNativeAdvertising {
  
  //MARK: - Inition
  convenience init(viewModal: NativeAdvertisingViewModal) {
    self.init()
    self.VM = viewModal
  }
}

