
import UIKit
import GoogleMobileAds
import SnapKit

class SetupNativeAdvertising: VMManager {
  
  //MARK: - Public variable
  public var VM: NativeAdvertisingViewModal!
  
  public func adLoaderInit(){
    self.VM.gdAddMob.setupNative(viewController: self.VM.VC, adLoader: &self.VM.adLoader)
  }
  
  public func constraintNativeView(){
    switch self.VM.VC.presentationType {
      case .Controller:
        self.VM.VC.nativeAdView.snp.makeConstraints { (nativeAdView) in
          nativeAdView.edges.equalTo(self.VM.VC.view)
        }
      case .CollectionCell:
        self.VM.VC.nativeAdCellView.snp.makeConstraints { (nativeAdCell) in
          nativeAdCell.edges.equalTo(self.VM.VC.view)
        }
      case .HomeBottomView:
        self.VM.VC.addsHomeBottomView.snp.makeConstraints { (addsHomeBottomView) in
          addsHomeBottomView.edges.equalTo(self.VM.VC.view)
        }
      case .NoImage:
        self.VM.VC.nativeAdNoImage.snp.makeConstraints { (nativeAdNoImage) in
          nativeAdNoImage.edges.equalTo(self.VM.VC.view)
        }
      case .Setting:
        self.VM.VC.settingAdView.snp.makeConstraints { (settingAdView) in
          settingAdView.edges.equalTo(self.VM.VC.view)
        }
      case .HomeCenterView:
        self.VM.VC.addsHomeCenterView.snp.makeConstraints { (addsHomeCenterView) in
          addsHomeCenterView.edges.equalTo(self.VM.VC.view)
        }
    }
  }
  public func addedView(){
    switch self.VM.VC.presentationType {
      case .Controller:
        self.VM.VC.view.addSubview(self.VM.VC.nativeAdView)
      case .CollectionCell:
        self.VM.VC.view.addSubview(self.VM.VC.nativeAdCellView)
      case .HomeBottomView:
        self.VM.VC.view.addSubview(self.VM.VC.addsHomeBottomView)
      case .NoImage:
        self.VM.VC.view.addSubview(self.VM.VC.nativeAdNoImage)
      case .Setting:
        self.VM.VC.view.addSubview(self.VM.VC.settingAdView)
      case .HomeCenterView:
        self.VM.VC.view.addSubview(self.VM.VC.addsHomeCenterView)
    }
  }
}
//MARK: - Initial
extension SetupNativeAdvertising {
  
  //MARK: - Inition
  convenience init(viewModal: NativeAdvertisingViewModal) {
    self.init()
    self.VM = viewModal
  }
}


