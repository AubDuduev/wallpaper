import UIKit

class AnimationNativeAdvertising: VMManager {
  
  //MARK: - Public variable
  public var VM: NativeAdvertisingViewModal!
  
  
}
//MARK: - Initial
extension AnimationNativeAdvertising {
  
  //MARK: - Inition
  convenience init(viewModal: NativeAdvertisingViewModal) {
    self.init()
    self.VM = viewModal
  }
}

