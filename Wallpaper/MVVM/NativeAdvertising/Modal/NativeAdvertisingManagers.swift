
import Foundation

class NativeAdvertisingManagers: VMManagers {
  
  let setup    : SetupNativeAdvertising!
  let server   : ServerNativeAdvertising!
  let present  : PresentNativeAdvertising!
  let logic    : LogicNativeAdvertising!
  let animation: AnimationNativeAdvertising!
  let router   : RouterNativeAdvertising!
  
  init(viewModal: NativeAdvertisingViewModal) {
    
    self.setup     = SetupNativeAdvertising(viewModal: viewModal)
    self.server    = ServerNativeAdvertising(viewModal: viewModal)
    self.present   = PresentNativeAdvertising(viewModal: viewModal)
    self.logic     = LogicNativeAdvertising(viewModal: viewModal)
    self.animation = AnimationNativeAdvertising(viewModal: viewModal)
    self.router    = RouterNativeAdvertising(viewModal: viewModal)
  }
}

