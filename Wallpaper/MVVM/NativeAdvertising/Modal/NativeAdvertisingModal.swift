
import UIKit

enum NativeAdvertisingModal {
	
	case loading
	case getData
	case presentData(Data)
	
}

