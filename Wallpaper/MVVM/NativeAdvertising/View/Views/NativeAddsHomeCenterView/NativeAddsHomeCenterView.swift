
import GoogleMobileAds
import UIKit

class NativeAddsHomeCenterView: GADUnifiedNativeAdView {

  @IBOutlet weak var customMediaView    : GADMediaView!
  @IBOutlet weak var customHeadlineLabel: UILabel!
  @IBOutlet weak var customBodyLabel    : UILabel!
  
  public var dissmiss   : ClousureEmpty!
  public var advertising: Clousure<String?>!
  
  public func setup(nativeAd: GADUnifiedNativeAd){
    
    self.nativeAd = nativeAd
    
    self.setContent(nativeAd: nativeAd)
    
    self.cornerRadius(1, true)
  }
  public func setContent(nativeAd: GADUnifiedNativeAd){
    //
    self.customMediaView.mediaContent = nativeAd.mediaContent
    self.customMediaView.contentMode  = .scaleAspectFill
    //
    self.customHeadlineLabel.text = nativeAd.headline
    //
    self.customBodyLabel.text = nativeAd.body
  }
  @IBAction func dissmissButton(button: UIButton){
    self.dissmiss?()
  }
}
