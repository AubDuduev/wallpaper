
import GoogleMobileAds
import UIKit

class NativeAdvertisingView: GADUnifiedNativeAdView {
  
  @IBOutlet weak var customMediaView    : GADMediaView!
  @IBOutlet weak var customHeadlineLabel: UILabel!
  @IBOutlet weak var customBodyLabel    : UILabel!
  @IBOutlet weak var utteranceCircleView: UIView!
  @IBOutlet weak var customImageView    : UIImageView!
  
  public var dissmiss   : ClousureEmpty!
  public var advertising: Clousure<String?>!
  
  public func setup(nativeAd: GADUnifiedNativeAd){
    self.utteranceCircle()
    
    self.nativeAd = nativeAd
    
    self.setContent(nativeAd: nativeAd)
    
    self.customMediaView.cornerRadius(5, true)
  }
  public func setContent(nativeAd: GADUnifiedNativeAd){
    //
    self.customMediaView.mediaContent = nativeAd.mediaContent
    self.customMediaView.contentMode  = .scaleAspectFill
    //
    self.customHeadlineLabel.text = nativeAd.headline
    //
    self.customBodyLabel.text = nativeAd.body
    //
    self.customImageView.image = nativeAd.icon?.image
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.utteranceCircle()
  }
  public func utteranceCircle(){
    self.utteranceCircleView.cornerRadius(25, true)
  }
  @IBAction func dissmissButton(button: UIButton){
    self.dissmiss?()
  }
}

