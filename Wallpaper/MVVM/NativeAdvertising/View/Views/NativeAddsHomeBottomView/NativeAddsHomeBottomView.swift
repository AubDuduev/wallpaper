
import GoogleMobileAds
import UIKit

class NativeAddsHomeBottomView: GADUnifiedNativeAdView {

  @IBOutlet weak var customHeadlineLabel: UILabel!
  @IBOutlet weak var customBodyLabel    : UILabel!
  @IBOutlet weak var utteranceCircleView: UIView!
  @IBOutlet weak var customImageView    : UIImageView!
  
  public var dissmiss   : ClousureEmpty!
  public var advertising: Clousure<String?>!
  
  public func setup(nativeAd: GADUnifiedNativeAd){
    
    self.nativeAd = nativeAd
    
    self.setContent(nativeAd: nativeAd)
    
    self.utteranceCircleView.cornerRadius(5, true)
  }
  public func setContent(nativeAd: GADUnifiedNativeAd){
    //
    self.customHeadlineLabel.text = nativeAd.headline
    //
    self.customBodyLabel.text = nativeAd.body
    //
    self.customImageView.image = nativeAd.icon?.image
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.utteranceCircleView.cornerRadius(5, true)
  }
  @IBAction func dissmissButton(button: UIButton){
    self.dissmiss?()
  }
}
