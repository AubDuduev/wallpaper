
import UIKit
import GoogleMobileAds

class NativeAddsForNoImageView: GADUnifiedNativeAdView {

  @IBOutlet weak var customMediaView    : GADMediaView!
  @IBOutlet weak var customHeadlineLabel: UILabel!
  @IBOutlet weak var customBodyLabel    : UILabel!
  @IBOutlet weak var advertisingButton  : UIButton!
  
  public var dissmiss   : ClousureEmpty!
  public var advertising: Clousure<String?>!
  
  public func setup(nativeAd: GADUnifiedNativeAd){
    
    self.nativeAd = nativeAd
    
    self.setContent(nativeAd: nativeAd)
    
    self.customMediaView.cornerRadius(5, true)
  }
  public func setContent(nativeAd: GADUnifiedNativeAd){
    //Media Content
    self.customMediaView.mediaContent = nativeAd.mediaContent
    self.customMediaView.contentMode  = .scaleAspectFill
    //Headline
    self.customHeadlineLabel.text = nativeAd.headline
    //Body
    self.customBodyLabel.text = nativeAd.body
  }

  @IBAction func dissmissButton(button: UIButton){
    self.dissmiss?()
  }
}
