import GoogleMobileAds
import UIKit

class NativeAdvertisingViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: NativeAdvertisingViewModal!
  
  //MARK: - Public variable
	public let nativeAdView       = NativeAdvertisingView().loadNib()
  public let nativeAdCellView   = NativeAddForCollectionView().loadNib()
  public let nativeAdNoImage    = NativeAddsForNoImageView().loadNib()
  public let settingAdView      = NativeSettingAdvertisingView().loadNib()
  public let addsHomeCenterView = NativeAddsHomeCenterView().loadNib()
  public let addsHomeBottomView = NativeAddsHomeBottomView().loadNib()
  
  public var presentationType : NativeAddVCPresentation = .Controller
  
  public var dissmissAdds: ClousureEmpty!
  
  override func loadView() {
    super.loadView()
  }
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
		self.viewModal.viewDidLoad()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  private func initViewModal(){
    self.viewModal = NativeAdvertisingViewModal(viewController: self)
  }
}

