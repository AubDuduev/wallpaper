
import Foundation

class RateUsViewModal: VMManagers {
	
	public var rateUsModal: RateUsModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers: RateUsManagers!
  public var VC      : RateUsViewController!
  
  public func viewDidLayoutSubviews() {
    self.managers.setup.setConstraints()
  }
  
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension RateUsViewModal {
  
  convenience init(viewController: RateUsViewController) {
    self.init()
    self.VC       = viewController
    self.managers = RateUsManagers(viewModal: self)
  }
}
