//
//  ActionsRateUsViewController.swift
//  Wallpaper
//
//  Created by Senior Developer on 08.01.2021.
//
import UIKit

extension RateUsViewController {
  
  @IBAction func pushNavBarVC(button: UIButton){
    self.viewModal.managers.logic.pushNavBarVC()
  }
  @IBAction func rateApp(button: UIButton){
    self.viewModal.managers.logic.rateApp()
  }
  @IBAction func backButton(button: UIButton){
    self.viewModal.managers.logic.pushNavBarVC()
  }
}
