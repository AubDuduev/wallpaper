
import UIKit
import Foundation

class RateUsSetConstraint {
  
  
  private let constraints   : RateUsConstraints!
  private var constraintoble: Constraintoble!
  
  public func setConstraint() -> Constraintoble {
    switch GDDevice.shared.type() {
      
      //1 - iPhone 5
      case .iPhone5:
        self.constraintoble = iPhone5RateUsConstraints(constraints: self.constraints)
        return constraintoble
      //2 - iPad 7
      case  .iPad7:
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //3 - iPad Air
      case  .iPadAir:
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //4 - iPadPro 9Inch
      case  .iPadPro9Inch:
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //5 - iPadPro 11Inch
      case .iPadPro11Inch:
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //6 - iPadPro 12Inch
      case  .iPadPro12Inch:
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //7 - iPad Mini
      case  .iPadMini:
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //8 - iPad 2
      case  .iPad2:
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //9 - iPhone 7
      case .iPhone7:
        self.constraintoble = iPhone7RateUsConstraints(constraints: self.constraints)
        return constraintoble
      //10 - iPhone X
      case .iPhoneX:
        self.constraintoble = iPhone11RateUsConstraints(constraints: self.constraints)
        return constraintoble
      //11 - iPhone 11 MAX
      case .iPhone11ProMax:
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //12 - iPhone Plus
      case .iPhone6Plus:
        self.constraintoble = iPhonePlusRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //13 - iPhone 12ProMax
      case .iPhone12ProMax:
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
      //9 - Error
      default:
        print(GDDevice.shared.type().realDevice)
        self.constraintoble = iPhoneMaxRateUsConstraints(constraints: self.constraints)
        return constraintoble
    }
  }
  
  init(constraints: RateUsConstraints) {
    self.constraints = constraints
  }
}




