
import UIKit

class SetupRateUs: VMManager {
  
  //MARK: - Public variable
  public var VM: RateUsViewModal!
  
  public func setConstraints(){
    let constraints = RateUsConstraints(bottomButtonConstant: self.VM.VC.bottomButtonConstant)
    let constraint = RateUsSetConstraint(constraints: constraints)
    let device = constraint.setConstraint()
    device.setup()
  }
}
//MARK: - Initial
extension SetupRateUs {
  
  //MARK: - Inition
  convenience init(viewModal: RateUsViewModal) {
    self.init()
    self.VM = viewModal
  }
}


