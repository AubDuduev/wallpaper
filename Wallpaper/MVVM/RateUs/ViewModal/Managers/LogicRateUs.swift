import Foundation
import StoreKit

class LogicRateUs: VMManager {
  
  //MARK: - Public variable
  public var VM: RateUsViewModal!
  
  public func pushNavBarVC(){
    self.VM.managers.router.push(.NavBarVC)
  }
  public func rateApp(){
    SKStoreReviewController.requestReview()
  }
}
//MARK: - Initial
extension LogicRateUs {
  
  //MARK: - Inition
  convenience init(viewModal: RateUsViewModal) {
    self.init()
    self.VM = viewModal
  }
}
