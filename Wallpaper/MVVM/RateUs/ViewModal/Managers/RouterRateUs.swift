
import UIKit

class RouterRateUs: VMManager {
  
  //MARK: - Public variable
  public var VM: RateUsViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .TabBarVC:
        self.pushTabBarVC()
      case .NavBarVC:
        self.pushNavBarVC()
    }
  }
  
  enum Push {
    case TabBarVC
    case NavBarVC
  }
}
//MARK: - Private functions
extension RouterRateUs {
  
  private func pushTabBarVC(){
    let tabBarVC = self.VM.VC.getVCForID(storyboardID     : .TabBar,
                                         vcID             : .TabBarVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! TabBarViewController
    self.VM.VC.present(tabBarVC, animated: true)
  }
  private func pushNavBarVC(){
    let navBarVC = self.VM.VC.getVCForID(storyboardID     : .NavBar,
                                         vcID             : .NavBarVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! NavBarViewController
    self.VM.VC.present(navBarVC, animated: true)
  }
}
//MARK: - Initial
extension RouterRateUs {
  
  //MARK: - Inition
  convenience init(viewModal: RateUsViewModal) {
    self.init()
    self.VM = viewModal
  }
}



