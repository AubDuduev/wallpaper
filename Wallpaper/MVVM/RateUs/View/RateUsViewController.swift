import UIKit

class RateUsViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: RateUsViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
  @IBOutlet weak var bottomButtonConstant: NSLayoutConstraint!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  private func initViewModal(){
    self.viewModal = RateUsViewModal(viewController: self)
  }
}
