
import Foundation

class RateUsManagers: VMManagers {
  
  let setup    : SetupRateUs!
  let server   : ServerRateUs!
  let present  : PresentRateUs!
  let logic    : LogicRateUs!
  let animation: AnimationRateUs!
  let router   : RouterRateUs!
  
  init(viewModal: RateUsViewModal) {
    
    self.setup     = SetupRateUs(viewModal: viewModal)
    self.server    = ServerRateUs(viewModal: viewModal)
    self.present   = PresentRateUs(viewModal: viewModal)
    self.logic     = LogicRateUs(viewModal: viewModal)
    self.animation = AnimationRateUs(viewModal: viewModal)
    self.router    = RouterRateUs(viewModal: viewModal)
  }
}

