
import Foundation

class CategoriesManagers: VMManagers {
  
  let setup    : SetupCategories!
  let server   : ServerCategories!
  let present  : PresentCategories!
  let logic    : LogicCategories!
  let animation: AnimationCategories!
  let router   : RouterCategories!
  
  init(viewModal: CategoriesViewModal) {
    
    self.setup     = SetupCategories(viewModal: viewModal)
    self.server    = ServerCategories(viewModal: viewModal)
    self.present   = PresentCategories(viewModal: viewModal)
    self.logic     = LogicCategories(viewModal: viewModal)
    self.animation = AnimationCategories(viewModal: viewModal)
    self.router    = RouterCategories(viewModal: viewModal)
  }
}

