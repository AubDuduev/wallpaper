
import UIKit

enum CategoriesStatic {
	
	case loading
	case getData
	case errorHandler([DECWallpaper]?)
	case presentData(CategoriesStatic.Data)
	
	struct Data {
		
		let wallpapers: [DECWallpaper]!
		
		init(wallpapers: [DECWallpaper]){
			self.wallpapers = wallpapers
		}
	}
}

enum CategoriesPresent {
	
	case loading(title: String?)
	case getData(endPoint: String)
	case errorHandler([DECWallpaper]?)
	case presentData(CategoriesPresent.Data)
	
	struct Data {
		
		let wallpapers: [DECWallpaper]!
		
		init(wallpapers: [DECWallpaper]){
			self.wallpapers = wallpapers
		}
	}
}

enum TypeCategoriesData {
	
	case Present(CategoriesData)
	case Static
}


struct CategoriesData {
	
	let wallpaper: DECWallpaper!
	
	init?(wallpaper: DECWallpaper?) {
		if let wallpaper = wallpaper {
			self.wallpaper = wallpaper
		} else {
			return nil
		}
	}
}
