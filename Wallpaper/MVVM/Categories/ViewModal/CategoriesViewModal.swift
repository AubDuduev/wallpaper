
import Foundation
import NVActivityIndicatorView

class CategoriesViewModal: VMManagers {
	
	public var categoriesStatic: CategoriesStatic = .loading {
		didSet{
			self.commonCategoriesStatic()
		}
	}
	public var categoriesPresent: CategoriesPresent! {
		didSet{
			self.commonCategoriesPresent()
		}
	}
	//MARK: - Public variable
	public var loading      : NVActivityIndicatorView!
	public var managers     : CategoriesManagers!
	public var VC           : CategoriesViewController!
	public let server       = Server()
	public let errorHandler = ErrorHandlerCategories()
	
	public func viewDidLoad() {
		self.managers.setup.categoriesTable()
		self.managers.logic.changePresentData()
		self.managers.setup.bottomCommonConstant()
    self.managers.setup.navBar()
	}
	
	public func commonCategoriesStatic(){
		
		switch categoriesStatic {
			//1 - Загрузка
			case .loading:
				self.managers.setup.startLoading()
				self.categoriesStatic = .getData
				
			//2 - Получение данных
			case .getData:
				//Делаем запрос на сервер для получения данных
				self.managers.server.getCategories { [weak self] (wallpapers) in
					self?.categoriesStatic = .errorHandler(wallpapers)
				}
				
			//3 - Обрабка ошибок запроса
			case .errorHandler(let wallpapers):
				//Обрабатываем на ошибки полученные данные
				guard self.errorHandler.check(wallpapers: wallpapers) else { return }
				let categoriesStaticData = CategoriesStatic.Data(wallpapers: wallpapers!)
				self.categoriesStatic = .presentData(categoriesStaticData)
				
			//4 - Отоброжение данных на kонтроллере
			case .presentData(let categoriesStaticData):
				self.managers.present.table(wallpapers: categoriesStaticData.wallpapers)
				self.loading.stopAnimating()
		}
	}
	public func commonCategoriesPresent(){
		
		switch categoriesPresent {
			//1 - Загрузка
			case .loading(let title):
				self.managers.setup.startLoading()
				self.managers.present.titleNavBar(title: title)
				
			//2 - Получение данных
			case .getData(endPoint: let endPoint):
				//Делаем запрос на сервер для получения данных
				self.managers.server.getCategoriesForEndPoin(endPoint: endPoint){ [weak self] (wallpapers) in
					self?.categoriesPresent = .errorHandler(wallpapers)
				}
				
			//3 - Обрабка ошибок запроса
			case .errorHandler(let wallpapers):
				//Обрабатываем на ошибки полученные данные
				guard self.errorHandler.check(wallpapers: wallpapers) else { return }
				let categoriesPresentData = CategoriesPresent.Data(wallpapers: wallpapers!)
				self.categoriesPresent = .presentData(categoriesPresentData)
				
			//4 - Отоброжение данных на kонтроллере
			case .presentData(let categoriesPresentData):
				self.managers.present.table(wallpapers: categoriesPresentData.wallpapers)
				self.loading.stopAnimating()
				
			default: break
		}
	}
}
//MARK: - Initial
extension CategoriesViewModal {
  
  convenience init(viewController: CategoriesViewController) {
    self.init()
    self.VC       = viewController
    self.managers = CategoriesManagers(viewModal: self)
  }
}
