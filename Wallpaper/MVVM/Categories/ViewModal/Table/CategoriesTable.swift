
import UIKit

class CategoriesTable: NSObject {
	
	public var viewModal : CategoriesViewModal!
	public var tableView : UITableView!
	public var wallpapers: [DECWallpaper]!
}
//MARK: - Delegate
extension CategoriesTable: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
	}
}
//MARK: - DataSources
extension CategoriesTable: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		self.tableView = tableView
    if (section % 2) != 0 && !GDPurchacesActions.shared.isPurchaces {
      return 2
    } else {
      return 1
    }
	}
	func numberOfSections(in tableView: UITableView) -> Int {
		self.tableView = tableView
		return self.wallpapers?.count ?? 0
	}
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch true {
      case (indexPath.row) == 1 && !GDPurchacesActions.shared.isPurchaces :
        let cell = NativeAddsTableCell().tableCell()
        cell.configure(viewController: self.viewModal.VC)
        return cell
      default:
        let cell = CategoriesTableCell().tableCell()
        cell.configure(viewModal: self.viewModal, wallpaper: wallpapers?[indexPath.section])
        return cell
    }
	}
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch true {
      case (indexPath.row) == 1 && !GDPurchacesActions.shared.isPurchaces :
        return 320
      default:
        return 187
    }
	}
	//MARK: - Header
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 57
	}
	func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		let header = CategoriesHeaderTableView().loadNib()
		header.configure(viewModal: self.viewModal, wallpaper: wallpapers?[section])
		return header
	}
}
