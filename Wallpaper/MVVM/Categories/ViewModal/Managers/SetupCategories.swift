
import UIKit
import NVActivityIndicatorView

class SetupCategories: VMManager {
  
  //MARK: - Public variable
  public var VM: CategoriesViewModal!
  
	//MARK: - Public CategoriesViewController
	public func startLoading(){
		let rect = CGRect(x: 0, y: 0, width: 250, height: 250)
		self.VM.loading = NVActivityIndicatorView(frame  : rect,
																							type   : .ballBeat,
																							color  : .black,
																							padding: 10)
		self.VM.loading.center = self.VM.VC.view.center
		self.VM.VC.view.addSubview(self.VM.loading)
		self.VM.loading.startAnimating()
	}
	public func categoriesTable(){
		self.VM.VC.categoriesTable.viewModal = self.VM
	}
	public func bottomCommonConstant(){
		switch self.VM.VC.typeCategoriesData {
			case .Present:
				self.VM.VC.bottomCommonConstant.constant = 0
			case .Static:
				self.VM.VC.bottomCommonConstant.constant = 55
		}
	}
  public func navBar(){
    switch self.VM.VC.typeCategoriesData {
      case .Present:
        self.VM.VC.titleNavBarView.isHidden = false
      case .Static:
        self.VM.VC.titleNavBarView.isHidden = true
    }
  }
	//MARK: - Public CategoriesTableCell
	public func commonView(cell: CategoriesTableCell){
		cell.commonView.cornerRadius(5, true)
		cell.wallpaperImageView.cornerRadius(5, true)
	}
}
//MARK: - Initial
extension SetupCategories {
  
  //MARK: - Inition
  convenience init(viewModal: CategoriesViewModal) {
    self.init()
    self.VM = viewModal
  }
}


