
import UIKit

class PresentCategories: VMManager {
  
  //MARK: - Public variable
  public var VM: CategoriesViewModal!
  
	//MARK: - CategoriesViewController
	public func table(wallpapers: [DECWallpaper]){
		self.VM.VC.categoriesTable.wallpapers = wallpapers
		self.VM.VC.categoriesTable.tableView?.reloadData()
	}

	public func titleNavBar(title: String?){
		self.VM.VC.titleNavBarLabel.text = title
	}
	
  //MARK: - Present CategoriesTableCell
  public func image(cell: CategoriesTableCell){
    guard let image = cell.wallpaper?.image else { return }
    guard let url   = self.VM.server.urls.get(type: .store(image)).URL else { return }
    cell.wallpaperImageView.isSkeletonable = true
    cell.wallpaperImageView.skeletonCornerRadius = 5
    cell.wallpaperImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
      cell.wallpaperImageView.sd_setImage(with: url) { (image, error, _, url) in
        
        if let error = error {
          
          print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
        } else {
          //cell.wallpaperImageView.image = image!.sd_flippedImage(withHorizontal: true, vertical: true)
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            cell.wallpaperImageView.hideSkeleton()
          }
        }
      }
    }
  }
	//MARK: - Present CategoriesHeaderTableView
	public func title(header: CategoriesHeaderTableView){
		header.categoryNameLabel.text = header.wallpaper.title
	}
}
//MARK: - Initial
extension PresentCategories {
  
  //MARK: - Inition
  convenience init(viewModal: CategoriesViewModal) {
    self.init()
    self.VM = viewModal
  }
}

