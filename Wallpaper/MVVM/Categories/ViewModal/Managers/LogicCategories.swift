import Foundation
import UIKit

class LogicCategories: VMManager {
  
  //MARK: - Public variable
  public var VM: CategoriesViewModal!
  
	public func changePresentData(){
		switch self.VM.VC.typeCategoriesData {
			
			case .Present(let categoriesData):
				self.VM.categoriesPresent = .loading(title: categoriesData.wallpaper.title)
				if let endPoint = categoriesData.wallpaper?.link {
					self.VM.categoriesPresent = .getData(endPoint: endPoint)
				}
			case .Static:
				self.VM.commonCategoriesStatic()
		}
	}
  public func prepareForReuse(image: UIImageView){
    image.isSkeletonable = true
    image.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
  }
  public func pushDetailVC(wallpaper: DECWallpaper){
    let detailTitle = DetailTitle.title(title: self.VM.VC.titleNavBarLabel.text)
    guard let detailGetData = DetailGetData(wallpaper  : wallpaper,
                                            dataType   : .server,
                                            detailTitle: detailTitle) else { return }
    self.VM.managers.router.push(.Detail(detailGetData))
  }
}
//MARK: - Initial
extension LogicCategories {
  
  //MARK: - Inition
  convenience init(viewModal: CategoriesViewModal) {
    self.init()
    self.VM = viewModal
  }
}
