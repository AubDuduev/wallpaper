
import UIKit

class RouterCategories: VMManager {
	
	//MARK: - Public variable
	public var VM: CategoriesViewModal!
	
	public func push(_ type: Push){
		
		switch type {
			case .Detail(let detailGetData):
				self.pushDetailVC(detailGetData: detailGetData)
		}
	}
	
	enum Push {
		case Detail(DetailGetData)
	}
}

//MARK: - Private functions
extension RouterCategories {
	
	private func pushDetailVC(detailGetData: DetailGetData){
		let detailVC = self.VM.VC.getVCForID(storyboardID     : .Detail,
																				 vcID             : .DetailVC,
																				 transitionStyle  : .crossDissolve,
																				 presentationStyle: .fullScreen) as! DetailViewController
		detailVC.detailGetData = detailGetData
		self.VM.VC.present(detailVC, animated: true)
	}
	
}
//MARK: - Initial
extension RouterCategories {
	
	//MARK: - Inition
	convenience init(viewModal: CategoriesViewModal) {
		self.init()
		self.VM = viewModal
	}
}



