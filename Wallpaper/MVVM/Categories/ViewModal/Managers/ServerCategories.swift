import UIKit

class ServerCategories: VMManager {
  
  //MARK: - Public variable
  public var VM: CategoriesViewModal!
  
	public func getCategories(completion: @escaping Clousure<[DECWallpaper]?>){
		//Request
		self.VM.server.request(requestType: GETCategories()) { [weak self]  (serverResult) in
			guard let self = self else { return }
			//Response
			switch serverResult {
				case .error(let error):
					guard let error = error else { return }
					print("Error server data: class: ServerCategories ->, function: getCategories -> data: [DECWallpaper] ->, description: ", error.localizedDescription)
					AlertEK.dеfault(title: .error, message: .noJSON){
						self.getCategories(completion: completion)
					}
				//Susses
				case .object(let object):
					let wallpapers = object as? [DECWallpaper]
					completion(wallpapers)
					print("Succesful data: class: ServerCategories ->, function: getCategories ->, data: [DECWallpaper]")
					
			}
		}
	}
	public func getCategoriesForEndPoin(endPoint: String, completion: @escaping Clousure<[DECWallpaper]?>){
		//Request
		self.VM.server.request(requestType: GETCategoriesForEndPoint(), data: endPoint) { [weak self]  (serverResult) in
			guard let self = self else { return }
			//Response
			switch serverResult {
				case .error(let error):
					guard let error = error else { return }
					print("Error server data: class: ServerCategories ->, function: getCategoriesForEndPoin -> data: [DECWallpaper] ->, description: ", error.localizedDescription)
					AlertEK.dеfault(title: .error, message: .noJSON){
						self.getCategoriesForEndPoin(endPoint: endPoint, completion: completion)
					}
				//Susses
				case .object(let object):
					let wallpapers = object as? [DECWallpaper]
					completion(wallpapers)
					print("Succesful data: class: ServerCategories ->, function: getCategoriesForEndPoin ->, data: [DECWallpaper]")
					
			}
		}
	}
}
//MARK: - Initial
extension ServerCategories {
  
  //MARK: - Inition
  convenience init(viewModal: CategoriesViewModal) {
    self.init()
    self.VM = viewModal
  }
}


