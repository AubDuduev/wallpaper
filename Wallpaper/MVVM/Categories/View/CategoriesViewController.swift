import UIKit

class CategoriesViewController: UIViewController {
	
	//MARK: - ViewModel
	public var viewModal: CategoriesViewModal!
	
	//MARK: - Public variable
	public var typeCategoriesData: TypeCategoriesData = .Static
	
	//MARK: - Outlets
	@IBOutlet weak var titleNavBarLabel: UILabel!
  @IBOutlet weak var titleNavBarView : UIView!
	@IBOutlet weak var categoriesTable : CategoriesTable!
	
	@IBOutlet weak var bottomCommonConstant: NSLayoutConstraint!
	
	//MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initViewModal()
		self.viewModal.viewDidLoad()
	}
	private func initViewModal(){
		self.viewModal = CategoriesViewModal(viewController: self)
	}
}
