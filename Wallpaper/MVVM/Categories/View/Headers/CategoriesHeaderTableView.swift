
import UIKit

class CategoriesHeaderTableView: UITableViewHeaderFooterView, LoadNidoble {
	
	private var viewModal: CategoriesViewModal!
	
	public var wallpaper: DECWallpaper!
	
	@IBOutlet weak var categoryNameLabel: UILabel!
	
	public func configure(viewModal: CategoriesViewModal?, wallpaper: DECWallpaper?){
		self.viewModal = viewModal
		self.wallpaper = wallpaper
		self.viewModal.managers.present.title(header: self)
	}
  @IBAction func pushDetailButton(button: UIButton){
    self.viewModal.managers.logic.pushDetailVC(wallpaper: self.wallpaper)
  }
}
