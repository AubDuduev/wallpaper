
import UIKit

class CategoriesTableCell: UITableViewCell, LoadNidoble {
	
	private var viewModal: CategoriesViewModal!
	
	public var wallpaper   : DECWallpaper!
	public var tableSection: Int!
	public var row         : Int!
	
	@IBOutlet weak var wallpaperImageView: UIImageView!
	@IBOutlet weak var commonView        : UIView!
	
	public func configure(viewModal: CategoriesViewModal?, wallpaper: DECWallpaper?){
		self.viewModal    = viewModal
		self.wallpaper    = wallpaper
		self.viewModal?.managers.present.image(cell: self)
	}
  override func prepareForReuse() {
    super.prepareForReuse()
    self.viewModal.managers.logic.prepareForReuse(image: self.wallpaperImageView)
  }
	override func layoutSubviews() {
		super.layoutSubviews()
		self.viewModal?.managers.setup.commonView(cell: self)
	}
	@IBAction func pushDetailButton(button: UIButton){
    self.viewModal.managers.logic.pushDetailVC(wallpaper: self.wallpaper)
	}
}
