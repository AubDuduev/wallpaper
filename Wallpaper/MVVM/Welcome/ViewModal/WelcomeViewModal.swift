
import Foundation

class WelcomeViewModal: VMManagers {
	
	public var welcomeDataModal: WelcomeDataModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers : WelcomeManagers!
  public var VC       : WelcomeViewController!
  public var timerAdds: Timer!
  
  public func viewDidLoad() {
    self.managers.logic.startTimerAdds()
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.setConstraints()
    self.managers.setup.exitButton()
  }
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension WelcomeViewModal {
  
  convenience init(viewController: WelcomeViewController) {
    self.init()
    self.VC       = viewController
    self.managers = WelcomeManagers(viewModal: self)
  }
}
