
import UIKit

extension WelcomeViewController {
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    switch segue.ID() {
      case .WelcomeVC_NativeAddVC:
        let nativeAdvertisingVC = (segue.destination as? NativeAdvertisingViewController)
        nativeAdvertisingVC?.presentationType = .NoImage
      default:
      break
    }
  }
}
