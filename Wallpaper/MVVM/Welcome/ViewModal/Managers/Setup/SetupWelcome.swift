
import UIKit

class SetupWelcome: VMManager {
  
  //MARK: - Public variable
  public var VM: WelcomeViewModal!
  
  public func setConstraints(){
    let constraints = WelcomeConstraints(arrayConstraints: self.VM.VC.arrayConstraints,
                                         titleLabels     : self.VM.VC.titleLabels,
                                         textStackView   : self.VM.VC.textStackView)
    let constraint = WelcomeSetConstraint(constraints: constraints)
    let device = constraint.setConstraint()
    device.setup()
  }
  public func exitButton(){
    self.VM.VC.exitButton.alpha = 0.3
  }
}
//MARK: - Initial
extension SetupWelcome {
  
  //MARK: - Inition
  convenience init(viewModal: WelcomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}


