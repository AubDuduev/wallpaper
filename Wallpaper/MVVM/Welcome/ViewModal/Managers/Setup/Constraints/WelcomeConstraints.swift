import UIKit

class WelcomeConstraints {
  
  //MARK: - Public variable
  public var bottomViewConstraint: NSLayoutConstraint!
  public var centerViewConstraint: NSLayoutConstraint!
  public var textHeightConstraint: NSLayoutConstraint!
  public var titleLabels         : [UILabel]!
  public var textStackView       : UIStackView!
  
  
  init(arrayConstraints: [NSLayoutConstraint], titleLabels: [UILabel], textStackView: UIStackView) {
    self.bottomViewConstraint = arrayConstraints[0]
    self.centerViewConstraint = arrayConstraints[1]
    self.textHeightConstraint = arrayConstraints[2]
    self.titleLabels          = titleLabels
    self.textStackView        = textStackView
  }
}




