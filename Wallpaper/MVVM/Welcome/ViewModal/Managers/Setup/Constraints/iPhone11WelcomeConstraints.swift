import UIKit

class iPhone11WelcomeConstraints: Constraintoble {
  
  //MARK: - Public variable
  public var constraints: WelcomeConstraints!
  
  func setup() {
    self.constraints.centerViewConstraint.constant = 70
    self.constraints.bottomViewConstraint.constant = 40
  }
  
  init(constraints: WelcomeConstraints) {
    self.constraints = constraints
  }
}


