import UIKit

class iPhone5WelcomeConstraints: Constraintoble {
  
  //MARK: - Public variable
  public var constraints: WelcomeConstraints!
  
  func setup() {
    self.constraints.centerViewConstraint.constant = 0
    self.constraints.bottomViewConstraint.constant = 0
  }
  
  init(constraints: WelcomeConstraints) {
    self.constraints = constraints
  }
}

