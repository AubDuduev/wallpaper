
import UIKit

class iPod12WelcomeConstraints: Constraintoble {
  
  //MARK: - Public variable
  public var constraints: WelcomeConstraints!
  
  func setup() {
    self.constraints.centerViewConstraint.constant = 100
    self.constraints.bottomViewConstraint.constant = 100
    self.constraints.textHeightConstraint.constant = 550
    self.constraints.titleLabels[0].font = .set(.aquireRegular, 145)
    self.constraints.titleLabels[1].font = .set(.aquireRegular, 105)
    self.constraints.titleLabels[2].font = .set(.aquireRegular, 170)
    self.constraints.titleLabels[3].font = .set(.aquireRegular, 125)
    self.constraints.textStackView.spacing = 10
  }
  
  init(constraints: WelcomeConstraints) {
    self.constraints = constraints
  }
}
