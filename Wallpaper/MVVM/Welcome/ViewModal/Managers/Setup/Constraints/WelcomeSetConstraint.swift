
import UIKit
import Foundation

class WelcomeSetConstraint {
  
  
  private let constraints   : WelcomeConstraints!
  private var constraintoble: Constraintoble!
  
  public func setConstraint() -> Constraintoble {
    print(GDDevice.shared.type().realDevice, "Real device")
    print(GDDevice.shared.type(), "TYPE device")
    switch GDDevice.shared.type() {
  
      //1 - iPad 7
      case  .iPad7:
        self.constraintoble = iPod12WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //2 - iPad Air
      case  .iPadAir:
        self.constraintoble = iPod12WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //3 - iPadPro 9Inch
      case  .iPadPro9Inch:
        self.constraintoble = iPod12WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //4 - iPadPro 11Inch
      case .iPadPro11Inch:
        self.constraintoble = iPod12WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //5 - iPadPro 12Inch
      case  .iPadPro12Inch:
        self.constraintoble = iPod12WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //6 - iPad Mini
      case  .iPadMini:
        self.constraintoble = iPod12WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //7 - iPad 2
      case  .iPad2:
        self.constraintoble = iPod12WelcomeConstraints(constraints: self.constraints)
        return constraintoble
        
      //8 - iPhone5
      case .iPhone5:
        self.constraintoble = iPhone5WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //9 - iPhone 7
      case .iPhone7:
        self.constraintoble = iPhone7WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //10 - iPhone X
      case .iPhoneX:
        self.constraintoble = iPhone11WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //11 - iPhone 11 MAX
      case .iPhone11ProMax:
        self.constraintoble = iPhoneMaxWelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //12 - iPhone Plus
      case .iPhone6Plus:
        self.constraintoble = iPhonePlusWelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //13 - iPhone 12ProMax
      case .iPhone12ProMax:
        self.constraintoble = iPhone11WelcomeConstraints(constraints: self.constraints)
        return constraintoble
      //9 - Error
      default:
        self.constraintoble = iPhone11WelcomeConstraints(constraints: self.constraints)
        return constraintoble
    }
  }
  
  init(constraints: WelcomeConstraints) {
    self.constraints = constraints
  }
}




