import UIKit

class iPhoneMaxWelcomeConstraints: Constraintoble {
  
  //MARK: - Public variable
  public var constraints: WelcomeConstraints!
  
  func setup() {
    self.constraints.centerViewConstraint.constant = 50
    self.constraints.bottomViewConstraint.constant = 30
    self.constraints.textHeightConstraint.constant = 250
    self.constraints.titleLabels[0].font = .set(.aquireRegular, 65)
    self.constraints.titleLabels[1].font = .set(.aquireRegular, 30)
    self.constraints.titleLabels[2].font = .set(.aquireRegular, 80)
    self.constraints.titleLabels[3].font = .set(.aquireRegular, 50)
    self.constraints.textStackView.spacing = 2
  }
  
  init(constraints: WelcomeConstraints) {
    self.constraints = constraints
  }
}


