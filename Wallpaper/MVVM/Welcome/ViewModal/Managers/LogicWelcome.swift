import Foundation

class LogicWelcome: VMManager {
  
  //MARK: - Public variable
  public var VM: WelcomeViewModal!

  
  public func dissmiss(){
    switch self.VM.VC.loadingVCPresentation {
      case .Home:
        self.VM.managers.router.push(.NavBarVC)
      case .Interstitial:
        self.VM.managers.router.push(.InterstitialVC)
        self.VM.VC.loadingVCPresentation = .Home
    }
  }
  public func commonView(){
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
      guard let self = self else { return }
      self.VM.VC.commonView.alpha = 1
    }
  }
  public func startTimerAdds(){
    var time = 3
    self.VM.timerAdds = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] (_) in
      guard let self = self else { return }
      self.VM.VC.exitButton.imageView?.image = nil
      self.VM.VC.exitButton.setTitle("\(time)", for: .normal)
      time -= 1
      guard time == 0 else { return }
      self.VM.VC.exitButton.setTitle("", for: .normal)
      self.VM.VC.exitButton.setImage(#imageLiteral(resourceName: "close"), for: .normal)
      self.VM.timerAdds.invalidate()
    })
    self.VM.timerAdds.fire()
  }
}
//MARK: - Initial
extension LogicWelcome {
  
  //MARK: - Inition
  convenience init(viewModal: WelcomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}
