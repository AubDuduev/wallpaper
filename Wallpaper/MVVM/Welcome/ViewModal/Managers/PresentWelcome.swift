
import UIKit

class PresentWelcome: VMManager {
  
  //MARK: - Public variable
  public var VM: WelcomeViewModal!
  
  
}
//MARK: - Initial
extension PresentWelcome {
  
  //MARK: - Inition
  convenience init(viewModal: WelcomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}

