import UIKit

class AnimationWelcome: VMManager {
  
  //MARK: - Public variable
  public var VM: WelcomeViewModal!
  
  
}
//MARK: - Initial
extension AnimationWelcome {
  
  //MARK: - Inition
  convenience init(viewModal: WelcomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}

