import UIKit

class ServerWelcome: VMManager {
  
  //MARK: - Public variable
  public var VM: WelcomeViewModal!
  
  
}
//MARK: - Initial
extension ServerWelcome {
  
  //MARK: - Inition
  convenience init(viewModal: WelcomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}


