
import UIKit

class RouterWelcome: VMManager {
  
  //MARK: - Public variable
  public var VM: WelcomeViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .InterstitialVC:
        self.pushInterstitialVC()
      case .NavBarVC:
        self.pushNavBarVC()
    }
  }
  
  enum Push {
    case InterstitialVC
    case NavBarVC
  }
}
//MARK: - Private functions
extension RouterWelcome {
  
  private func pushInterstitialVC(){
    let interstitialVC = self.VM.VC.getVCForID(storyboardID     : .Interstitial,
                                               vcID             : .InterstitialVC,
                                               transitionStyle  : .crossDissolve,
                                               presentationStyle: .fullScreen) as! InterstitialViewController
    interstitialVC.modalPresentationStyle  = .overCurrentContext
    interstitialVC.modalTransitionStyle    = .flipHorizontal
    interstitialVC.view.backgroundColor    = .clear
    interstitialVC.typeInterstitialPresent = .Welcome
    interstitialVC.interstitialWillPresentScreen = { [weak self] in
      guard let self = self else { return }
      self.VM.managers.logic.commonView()
    }
    self.VM.VC.present(interstitialVC, animated: true)
  }
  
  private func pushNavBarVC(){
    let navBarVC = self.VM.VC.getVCForID(storyboardID     : .NavBar,
                                         vcID             : .NavBarVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! NavBarViewController
    UIApplication.shared.windows.first?.rootViewController = navBarVC
    UIApplication.shared.windows.first?.makeKey()
    self.VM.VC.present(navBarVC, animated: true)
  }
}
//MARK: - Initial
extension RouterWelcome {
  
  //MARK: - Inition
  convenience init(viewModal: WelcomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}



