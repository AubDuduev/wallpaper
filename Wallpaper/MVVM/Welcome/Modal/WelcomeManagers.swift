
import Foundation

class WelcomeManagers: VMManagers {
  
  let setup    : SetupWelcome!
  let server   : ServerWelcome!
  let present  : PresentWelcome!
  let logic    : LogicWelcome!
  let animation: AnimationWelcome!
  let router   : RouterWelcome!
  
  init(viewModal: WelcomeViewModal) {
    
    self.setup     = SetupWelcome(viewModal: viewModal)
    self.server    = ServerWelcome(viewModal: viewModal)
    self.present   = PresentWelcome(viewModal: viewModal)
    self.logic     = LogicWelcome(viewModal: viewModal)
    self.animation = AnimationWelcome(viewModal: viewModal)
    self.router    = RouterWelcome(viewModal: viewModal)
  }
}

