import UIKit

class WelcomeViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: WelcomeViewModal!
  
  //MARK: - Public variable
  public var callbackWelcome      : ClousureEmpty!
  public var loadingVCPresentation: LoadingVCPresentation = .Interstitial
  
  //MARK: - Outlets
  @IBOutlet weak var commonView   : UIView!
  @IBOutlet weak var exitButton   : UIButton!
  @IBOutlet weak var textStackView: UIStackView!
  
  //MARK: - Outlets array
  @IBOutlet var titleLabels: [UILabel]!
  
  //MARK: - Outlets Constraint
  @IBOutlet var arrayConstraints: [NSLayoutConstraint]!
  
  //MARK: - LifeCycle
  override func loadView() {
    super.loadView()
    
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  private func initViewModal(){
    self.viewModal = WelcomeViewModal(viewController: self)
  }
}
