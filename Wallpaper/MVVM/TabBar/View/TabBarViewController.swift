
import UIKit
import AnimatedGradientView

class TabBarViewController: UITabBarController {
  
  //MARK: - ViewModel
  public var viewModal        : TabBarViewModal!
  public let animatedGradient = AnimatedGradientView()
  
  //MARK: - Public variable
  public let tabBarView = TabBarView().loadNib()
  
  //MARK: - Outlets
  
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  private func initViewModal(){
    self.viewModal = TabBarViewModal(tabBarViewController: self)
  }
}
