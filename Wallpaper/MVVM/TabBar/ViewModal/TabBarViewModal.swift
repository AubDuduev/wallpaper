
import Foundation

class TabBarViewModal: VMManagers {
  
  //MARK: - Public variable
  public var managers: TabBarManagers!
  public var tabbarVC: TabBarViewController!
  
	public func viewDidLoad() {
		self.managers.setup.gradientTabBarView()
		self.managers.setup.setDelegates()
		self.managers.setup.setupTabbarVC()
		self.managers.setup.addedAllView()
		self.managers.setup.tabBarView()
	}
	public func viewDidAppear() {
		//GDAddMob.shared.setupBannerView(vc: self.tabbarVC)
	}
	public func viewDidLayoutSubviews() {
		self.managers.setup.setTabBarView()
	}
}
//MARK: - Initial
extension TabBarViewModal {
  
  convenience init(tabBarViewController: TabBarViewController!) {
    self.init()
    self.tabbarVC = tabBarViewController
		self.managers = TabBarManagers(viewModal: self)
  }
}
