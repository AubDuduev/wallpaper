
import UIKit

class PresentTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: TabBarViewModal!
  
  
}
//MARK: - Initial
extension PresentTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: TabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}

