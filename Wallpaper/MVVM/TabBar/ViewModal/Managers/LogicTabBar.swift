import UIKit

class LogicTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: TabBarViewModal!
  
  //MARK: - Create View Controllers For TabBar
  public func createVcForTabBar() -> [UIViewController]{
		let home       = UIStoryboard.createVCID(sbID: .Home      , vcID: .HomeVC)
		let main       = UIStoryboard.createVCID(sbID: .Main      , vcID: .MainVC)
		let live       = UIStoryboard.createVCID(sbID: .Live      , vcID: .LiveVC)
		let categories = UIStoryboard.createVCID(sbID: .Categories, vcID: .CategoriesVC)
		let setting    = UIStoryboard.createVCID(sbID: .Setting   , vcID: .SettingVC)
    let controllers = [home, main, live, categories, setting]
    return controllers
  }
  public func tapButton() {
    self.VM.tabbarVC.tabBarView.actions = { [weak self] index in
			guard let self = self else { return }
      self.VM.tabbarVC.selectedIndex = index
			self.changesWhenSwitchingTabBar()
    }
		self.changesWhenSwitchingTabBar()
  }
  
  public func changesWhenSwitchingTabBar(){
		self.tabBarColor()
		self.navBarColor()
		self.navBarTitle()
  }
	
	private func tabBarColor(){
		if self.VM.tabbarVC.selectedIndex == 0 {
			self.VM.tabbarVC.tabBarView.backgroundColor = .clear
			self.VM.tabbarVC.animatedGradient.alpha = 1
		} else {
			self.VM.tabbarVC.tabBarView.backgroundColor = .black
			self.VM.tabbarVC.animatedGradient.alpha = 0
		}
	}
	private func navBarTitle(){
		if let navBar = self.VM.tabbarVC.navigationController as? NavBarViewController {
			navBar.navBarView.setTitleStatic(index: self.VM.tabbarVC.selectedIndex)
		}
	}
	private func navBarColor(){
		if let navBar = self.VM.tabbarVC.navigationController as? NavBarViewController {
			navBar.navBarView.setTitleStatic(index: self.VM.tabbarVC.selectedIndex)
			if self.VM.tabbarVC.selectedIndex == 0 {
				navBar.navBarView.backgroundColor = .clear
				navBar.safeAreaTopView.backgroundColor = .clear
			} else {
				navBar.navBarView.backgroundColor = .black
				navBar.safeAreaTopView.backgroundColor = .black
			}
		}
	}
}
//MARK: - Initial
extension LogicTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: TabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}
