
import UIKit
import AnimatedGradientView
import SnapKit

class SetupTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: TabBarViewModal!
  
}
//MARK: - Private functions
extension SetupTabBar {
  
  public func setupTabbarVC(){
    self.VM.tabbarVC.setViewControllers(self.VM.managers.logic.createVcForTabBar(), animated: true)
  }
  public func setDelegates(){
    //self.tabbarVC.delegate = self.tabbarVC
  }
  public func setTabBarView(){
		self.VM.tabbarVC.tabBarView.backgroundColor = .clear
		self.VM.tabbarVC.tabBar.isHidden = true
		self.VM.tabbarVC.tabBar.backgroundColor = .clear
    self.VM.tabbarVC.tabBarView.translatesAutoresizingMaskIntoConstraints = false
    let tabBarHeight = 75 + self.VM.tabbarVC.view.safeAreaInsets.bottom
    self.VM.tabbarVC.tabBarView.heightAnchor.constraint(equalToConstant: tabBarHeight).isActive = true
    self.VM.tabbarVC.tabBarView.leadingAnchor.constraint(equalTo: self.VM.tabbarVC.view.leadingAnchor).isActive = true
    self.VM.tabbarVC.tabBarView.trailingAnchor.constraint(equalTo: self.VM.tabbarVC.view.trailingAnchor).isActive = true
    self.VM.tabbarVC.tabBarView.bottomAnchor.constraint(equalTo: self.VM.tabbarVC.view.bottomAnchor).isActive = true
  }
  public func tabBarView(){
    self.VM.tabbarVC.tabBarView.setup()
    self.VM.tabbarVC.tabBarView.actionButton(button: UIButton())
    self.VM.managers.logic.tapButton()
  }
  public func addedAllView(){
    self.VM.tabbarVC.view.addSubview(self.VM.tabbarVC.tabBarView)
  }
	public func gradientTabBarView(){
		self.VM.tabbarVC.animatedGradient.direction = .up
		self.VM.tabbarVC.animatedGradient.colors = [[#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.9), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6), #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)]]
		self.VM.tabbarVC.view.addSubview(self.VM.tabbarVC.animatedGradient)
		self.VM.tabbarVC.animatedGradient.snp.makeConstraints { (marker) in
			marker.left.equalTo(self.VM.tabbarVC.view)
			marker.right.equalTo(self.VM.tabbarVC.view)
			marker.bottom.equalTo(self.VM.tabbarVC.view)
			marker.height.equalTo(160)
		}
	}
}
//MARK: - Initial
extension SetupTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: TabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}


