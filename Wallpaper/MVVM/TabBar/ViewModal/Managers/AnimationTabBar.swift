import UIKit

class AnimationTabBar: VMManager {
  
  //MARK: - Public variable
  public var VM: TabBarViewModal!
  
  
}
//MARK: - Initial
extension AnimationTabBar {
  
  //MARK: - Inition
  convenience init(viewModal: TabBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}

