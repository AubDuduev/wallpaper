
import Foundation

class TabBarManagers: VMManagers {
  
  let setup    : SetupTabBar!
  let server   : ServerTabBar!
  let present  : PresentTabBar!
  let logic    : LogicTabBar!
  let animation: AnimationTabBar!
  let router   : RouterTabBar!
  
  init(viewModal: TabBarViewModal) {
    
    self.setup     = SetupTabBar(viewModal: viewModal)
    self.server    = ServerTabBar(viewModal: viewModal)
    self.present   = PresentTabBar(viewModal: viewModal)
    self.logic     = LogicTabBar(viewModal: viewModal)
    self.animation = AnimationTabBar(viewModal: viewModal)
    self.router    = RouterTabBar(viewModal: viewModal)
  }
}

