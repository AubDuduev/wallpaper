
import NVActivityIndicatorView
import Foundation

class MainViewModal: VMManagers {
	
	public var mainModal: MainModal = .loading {
		didSet{
			self.mainLogic(mainModal: self.mainModal)
		}
	}
	public var podCategoryCellModal: PodCategoryCellModal! {
		didSet {
			self.podCategoryCellLogic()
		}
	}
  
  //MARK: - Public variable
	public var loading                    : NVActivityIndicatorView!
  public var managers                   : MainManagers!
  public var VC                         : MainViewController!
  public let server                     = Server()
	public let errorHandlerMainData       = ErrorHandlerMainData()
	public let errorHandlerDetailCategory = ErrorHandlerDetailCategory()
  public var indexPathAdvertising       = [Int]()
  public var indexPathActivity          = [Int]()
  
	public func viewDidLoad() {
		self.managers.setup.mainTable()
		self.mainLogic(mainModal: .loading)
	}
  
	public func mainLogic(mainModal: MainModal){
		
		switch mainModal {
			//1 - Загрузка
			case .loading:
				self.managers.setup.startLoading()
				self.mainModal = .getData
				
			//2 - Получение данных
			case .getData:
				//Делаем запрос на сервер для получения данных
				self.managers.server.getMainData { [weak self] (decMainData) in
					self?.mainModal = .errorHandler(decMainData)
				}
				
			//3 - Обрабка ошибок запроса
			case .errorHandler(let decMainData):
				//Обрабатываем на ошибки полученные данные
				guard self.errorHandlerMainData.check(decMainData: decMainData) else { return }
				let mainData = MainModal.Data(decMainData: decMainData!)
				self.mainModal = .presentData(mainData)
				
			//4 - Отоброжение данных на kонтроллере
			case .presentData(let mainData):
				self.managers.present.table(mainData: mainData)
				self.loading.stopAnimating()
		}
	}
	public func podCategoryCellLogic(){
		
		switch self.podCategoryCellModal {
			//1 - Загрузка
			case .loading(let endPath):
				self.podCategoryCellModal = .getData(endPath)
				
			//2 - Получение данных
			case .getData(let endPath):
				//Делаем запрос на сервер для получения данных
				self.managers.server.getDetailCategory(endPath: endPath){ [weak self] (mainPreview) in
					self?.podCategoryCellModal = .errorHandler(mainPreview)
				}
				
			//3 - Обрабка ошибок запроса
			case .errorHandler(let mainPreview):
				//Обрабатываем на ошибки полученные данные
				guard self.errorHandlerDetailCategory.check(mainPreview: mainPreview) else { return }
				let presentData = PodCategoryCellModal.Data(mainPreview: mainPreview!)
				self.podCategoryCellModal = .presentData(presentData)
				
			//4 - Отоброжение данных на kонтроллере
			case .presentData(let mainPreview):
				self.managers.present.table(mainPreview: mainPreview)
				self.loading.stopAnimating()
				
			default:
				break
		}
	}
}
//MARK: - Initial
extension MainViewModal {
  
  convenience init(viewController: MainViewController) {
    self.init()
    self.VC       = viewController
    self.managers = MainManagers(viewModal: self)
  }
}
