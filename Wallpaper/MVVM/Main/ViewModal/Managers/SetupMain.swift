
import NVActivityIndicatorView
import UIKit
import SnapKit

class SetupMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModal!
  
	//MARK: - Public Main
	public func startLoading(){
		let rect = CGRect(x: 0, y: 0, width: 250, height: 250)
		self.VM.loading = NVActivityIndicatorView(frame  : rect,
																							type   : .ballBeat,
																							color  : .black,
																							padding: 10)
		self.VM.loading.center = self.VM.VC.view.center
		self.VM.VC.view.addSubview(self.VM.loading)
		self.VM.loading.startAnimating()
	}
	public func mainTable(){
		self.VM.VC.mainTable.viewModal = self.VM
	}
	
	//MARK: - Public WallpaperCollectionCell
	public func wallpaperCollection(cell: MainTableCell){
		let collectionViewLaytout = WallpaperCollectionViewLaytout()
		collectionViewLaytout.sectionInset = .init(top: 0, left: 0, bottom: 0, right: 0)
		collectionViewLaytout.sectionInsetReference   = .fromContentInset
		collectionViewLaytout.scrollDirection         = .horizontal
		collectionViewLaytout.minimumLineSpacing      = 0
		collectionViewLaytout.minimumInteritemSpacing = 0
		cell.wallpaperCollectionView.collectionViewLayout = collectionViewLaytout
		cell.wallpaperCollection.viewModal            = self.VM
		cell.wallpaperCollection.tableSection         = cell.section
		cell.wallpaperCollectionView.delegate         = cell.wallpaperCollection
		cell.wallpaperCollectionView.dataSource       = cell.wallpaperCollection
	}
	
	//MARK: - Public WallpaperCollectionCell
	public func wallpaperImageView(cell: WallpaperCollectionCell){
		cell.wallpaperImageView.transform = CGAffineTransform(rotationAngle: .radians(gr: 180))
	}
	
	//MARK: - Public FlagsCollectionCell
	public func flagImageView(cell: FlagsCollectionCell){
		let corner = (cell.frame.height - 10) / 2
		cell.flagImageView.cornerRadius(corner, true)
    cell.flagImageView.skeletonCornerRadius = Float(corner)
	}
	
	//MARK: - Public AllCtegoryHeaderTableView
	public func wallpaperCollection(cell: AllCtegoryHeaderTableView){
		let collectionViewLaytout = WallpaperCollectionViewLaytout()
		collectionViewLaytout.sectionInset = .init(top: 0, left: 0, bottom: 0, right: 0)
		collectionViewLaytout.sectionInsetReference   = .fromContentInset
		collectionViewLaytout.scrollDirection         = .horizontal
		collectionViewLaytout.minimumLineSpacing      = 0
		collectionViewLaytout.minimumInteritemSpacing = 0
		cell.collectionView.collectionViewLayout = collectionViewLaytout
		cell.wallpaperCollection.viewModal       = self.VM
		cell.collectionView.delegate             = cell.wallpaperCollection
		cell.collectionView.dataSource           = cell.wallpaperCollection
	}
	//MARK: - Public PodCategoryTableCell
	public func podCategoryCollection(cell: PodCategoryTableCell){
		let collectionViewLaytout = PodCategoryCollectionViewLaytout()
		collectionViewLaytout.sectionInset = .init(top: 10, left: 10, bottom: 10, right: 10)
		collectionViewLaytout.sectionInsetReference   = .fromContentInset
		collectionViewLaytout.scrollDirection         = .vertical
		collectionViewLaytout.minimumLineSpacing      = 5
		collectionViewLaytout.minimumInteritemSpacing = 5
		cell.collectionView.collectionViewLayout = collectionViewLaytout
		cell.podCategoryCollection.viewModal   = self.VM
		cell.collectionView.delegate           = cell.podCategoryCollection
		cell.collectionView.dataSource         = cell.podCategoryCollection
    cell.podCategoryCollection.tableView   = self.VM.VC.mainTable.tableView
	}
	//MARK: - Public PodCategoryCollectionCell
	public func image(cell: PodCategoryCollectionCell){
		cell.previewImageView.cornerRadius(20, true)
    cell.cornerRadius(20, true)
	}
}
//MARK: - Initial
extension SetupMain {
  
  //MARK: - Inition
  convenience init(viewModal: MainViewModal) {
    self.init()
    self.VM = viewModal
  }
}


