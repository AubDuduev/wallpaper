
import UIKit

class RouterMain: VMManager {
	
	//MARK: - Public variable
	public var VM: MainViewModal!
	
	public func push(_ type: Push){
		
		switch type {
      
			case .Detail(let detailGetData):
				self.pushDetailVC(detailGetData: detailGetData)
        
			case .Categories(let categoriesData):
				self.pushCategoriesVC(categoriesData: categoriesData)
        
      case .HomeLiveVC(let homePresentData):
        self.pushHomeLiveVC(homePresentData: homePresentData)
        
      case .HomeStaticVC(let homePresentData):
        self.pushHomeStaticVC(homePresentData: homePresentData)
		}
	}
	
	enum Push {
		case Detail(DetailGetData)
		case Categories(CategoriesData)
    case HomeLiveVC(HomePresentData)
    case HomeStaticVC(HomePresentData)
	}
}
//MARK: - Private functions
extension RouterMain {
	
	private func pushDetailVC(detailGetData: DetailGetData){
		let detailVC = self.VM.VC.getVCForID(storyboardID     : .Detail,
																				 vcID             : .DetailVC,
																				 transitionStyle  : .crossDissolve,
																				 presentationStyle: .fullScreen) as! DetailViewController
		detailVC.detailGetData = detailGetData
		self.VM.VC.present(detailVC, animated: true)
	}
	private func pushCategoriesVC(categoriesData: CategoriesData){
		let categoriesVC = self.VM.VC.getVCForID(storyboardID     : .Categories,
																						 vcID             : .CategoriesVC,
																						 transitionStyle  : .crossDissolve,
																						 presentationStyle: .fullScreen) as! CategoriesViewController
		categoriesVC.typeCategoriesData = .Present(categoriesData)
		self.VM.VC.present(categoriesVC, animated: true)
	}
  private func pushHomeLiveVC(homePresentData: HomePresentData){
    let homeVC = self.VM.VC.getVCForID(storyboardID     : .Home,
                                       vcID             : .HomeVC,
                                       transitionStyle  : .crossDissolve,
                                       presentationStyle: .fullScreen) as! HomeViewController
    homeVC.typePresentHomeData = .localLive(homePresentData)
    self.VM.VC.present(homeVC, animated: true)
  }
  private func pushHomeStaticVC(homePresentData: HomePresentData){
    let homeVC = self.VM.VC.getVCForID(storyboardID     : .Home,
                                       vcID             : .HomeVC,
                                       transitionStyle  : .crossDissolve,
                                       presentationStyle: .fullScreen) as! HomeViewController
    homeVC.typePresentHomeData = .local(homePresentData)
    print(homePresentData)
    self.VM.VC.present(homeVC, animated: true)
  }
}
//MARK: - Initial
extension RouterMain {
	
	//MARK: - Inition
	convenience init(viewModal: MainViewModal) {
		self.init()
		self.VM = viewModal
	}
}



