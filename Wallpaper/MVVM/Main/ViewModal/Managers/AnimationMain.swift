import UIKit

class AnimationMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModal!
  
  
}
//MARK: - Initial
extension AnimationMain {
  
  //MARK: - Inition
  convenience init(viewModal: MainViewModal) {
    self.init()
    self.VM = viewModal
  }
}

