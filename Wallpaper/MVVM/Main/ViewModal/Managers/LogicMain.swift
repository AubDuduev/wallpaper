import Foundation
import UIKit

class LogicMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModal!
  
	//MARK: - MainFooterTableCell
	public func countWallpaperControl(footer: MainFooterTableView){
		footer.countWallpaperControl.numberOfPages = footer.category.wallpapers.count
	}
	//MARK: - AllCtegoryHeaderTableView
	public func logicPresentFirstPodCategoryAfterLoad(cell: AllCtegoryHeaderTableView){
		guard let endPath = cell.category.wallpapers.first?.link else { return }
		self.VM.podCategoryCellModal = .loading(endPath)
	}
	public func createHeaderState(cell: AllCtegoryHeaderTableView){
		var headerStates = [HeaderChoiceState]()
		for (index, _ ) in cell.category.wallpapers.enumerated() {
			if index == 0 {
				//Cтавим у первую ячейку открытой
				let headerState = HeaderChoiceState(row: index, state: .open)
				headerStates.append(headerState)
			} else {
				//Cтавим все кроме первой ячейки закрытыми
				let headerState = HeaderChoiceState(row: index, state: .clouse)
				headerStates.append(headerState)
			}
		}
		self.VM.VC.mainTable.headerStates = headerStates
	}
	
	//MARK: - TitleCategoryCollectionCell
	public func changeStateHeader(cell: TitleCategoryCollectionCell){
		self.VM.VC.mainTable.headerStates.forEach({ $0.state = .clouse })
		self.VM.VC.mainTable.headerStates[cell.indexPath.row].state = .open
		let IndexPaths = cell.collectionView.visibleCells.map({ cell.collectionView.indexPath(for: $0)!})
		cell.collectionView.reloadItems(at: IndexPaths)
	}
	public func testStateCell(cell: TitleCategoryCollectionCell){
    guard !self.VM.VC.mainTable.headerStates.isEmpty else { return }
		switch self.VM.VC.mainTable.headerStates[cell.indexPath.row].state {
			case .open:
				cell.choiceImageView.tintColor = .set(.tabBarClick)
			case .clouse:
				cell.choiceImageView.tintColor = .black
			default:
				break
		}
	}
	public func getDataPodCategory(cell: TitleCategoryCollectionCell){
		guard let endPath = cell.walpaper?.link else { return }
		self.VM.podCategoryCellModal = .loading(endPath)
	}

	//MARK: - WallpaperCollectionCell
	public func setCurrentPage(tableSection: Int, row: Int){
		if let footer = self.VM.VC.mainTable.tableView.footerView(forSection: tableSection) as? MainFooterTableView {
			footer.countWallpaperControl.currentPage = row
		}
    if let header = self.VM.VC.mainTable.tableView.headerView(forSection: tableSection) as? MainHeaderTableView {
      header.indexPath = IndexPath(row: row, section: tableSection)
    }
	}
	public func pushDetailVCOrCategoriesVC(category: GDCategory, indexPath: IndexPath){
    guard category.wallpapers != nil else { return }
    let wallpaper = category.wallpapers?[indexPath.row]
    let detailTitle = DetailTitle.category(category: category)
		switch category.typeCategory {
			//При нажатии на ячейку с обоями переходим на детальный список категорий
			case .category:
        guard let categoriesData = CategoriesData(wallpaper: wallpaper) else { return }
				self.VM.managers.router.push(.Categories(categoriesData))
        
			//При нажатии на ячейку с обоями переходим на детальный обоев
			case .categories:
				guard let detailGetData = DetailGetData(wallpaper  : wallpaper,
                                                dataType   : .server,
                                                detailTitle: detailTitle) else { return }
				self.VM.managers.router.push(.Detail(detailGetData))
        
      //При нажатии на ячейку с обоями переходим на детальный обоев
      case .other:
        guard let detailGetData = DetailGetData(wallpaper  : wallpaper,
                                                dataType   : .server,
                                                detailTitle: detailTitle) else { return }
        self.VM.managers.router.push(.Detail(detailGetData))
			default: break
		}
	}
  public func pushHomeLiveVC(category: GDCategory, indexPath: IndexPath){
    switch category.alias {
      case .categoryflag:
        let previews = category.flags?.compactMap{ DECPreview(flag: $0)}
        let homePresentData = HomePresentData(previews : previews,
                                              indexPath: indexPath)
        self.VM.managers.router.push(.HomeLiveVC(homePresentData))
      default:
        break
    }
  }
  public func pushHomeStaticVC(previews: [DECPreview], indexPath: IndexPath){
    let homePresentData = HomePresentData(previews : previews,
                                          indexPath: indexPath)
    self.VM.managers.router.push(.HomeStaticVC(homePresentData))
  }
  public func indexPathAdvertising(wallpapers: [DECWallpaper]?){
    guard let wallpapers = wallpapers else { return }
    let countAdvertising = (wallpapers.count / 5)
    var indexPathRow = 6
    for _ in 0...countAdvertising {
      self.VM.indexPathAdvertising.append(indexPathRow)
      indexPathRow += 7
    }
  }
  public func indexPathActivity(wallpapers: [DECWallpaper]?){
    guard let wallpapers = wallpapers else { return }
    let countAdvertising = (wallpapers.count / 6)
    var indexPathRow = 13
    for _ in 0...countAdvertising {
      self.VM.indexPathActivity.append(indexPathRow)
      indexPathRow += 14
    }
  }
  public func prepareForReuse(image: UIImageView){
    image.isSkeletonable = true
    image.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
  }
}
//MARK: - Initial
extension LogicMain {
  
  //MARK: - Inition
  convenience init(viewModal: MainViewModal) {
    self.init()
    self.VM = viewModal
  }
}
