
import UIKit

class PresentMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModal!
  
	//MARK: - Present MainViewController
	public func table(mainData: MainModal.Data){
		self.VM.VC.mainTable.categories = mainData.categories.sorted{ $0.sorted < $1.sorted }
		self.VM.VC.mainTable.tableView.reloadData()
	}
	
	//MARK: - Present MainTableCell
	public func wallpaperCollection(cell: MainTableCell){
		cell.wallpaperCollection.category = cell.category
		cell.wallpaperCollectionView.reloadData()
	}
	//MARK: - Present MainHeaderTableCell
	public func title(header: MainHeaderTableView){
		header.categoryNameLabel.text = header.category.title
	}
  //MARK: - Present ScreenshotCollectionCell
  public func image(cell: WallpaperCollectionCell){
    guard let image = cell.wallpaper?.image else { return }
    guard let url   = self.VM.server.urls.get(type: .store(image)).URL else { return }
    cell.wallpaperImageView.isSkeletonable = true
    cell.wallpaperImageView.skeletonCornerRadius = 0
    cell.wallpaperImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
      cell.wallpaperImageView.sd_setImage(with: url) { (image, error, _, url) in
        
        if let error = error {
          print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
        } else {
          cell.wallpaperImageView.image = image!.sd_flippedImage(withHorizontal: true, vertical: true)
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            cell.wallpaperImageView.hideSkeleton()
          }
        }
      }
    }
  }
  //MARK: - Public FlagsCollectionCell
  public func image(cell: FlagsCollectionCell){
    guard let image = cell.flag?.image else { return }
    guard let url   = self.VM.server.urls.get(type: .store(image)).URL else { return }
    cell.flagImageView.isSkeletonable = true
    let corner = (cell.frame.height - 10) / 2
    cell.flagImageView.skeletonCornerRadius = Float(corner)
    cell.flagImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
     
      cell.flagImageView.sd_setImage(with: url) { (image, error, _, url) in
        if let error = error {
          print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
        } else {
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            cell.flagImageView.hideSkeleton()
            let corner = (cell.frame.height - 10) / 2
            cell.flagImageView.cornerRadius(corner, true)
          }
        }
      }
    }
  }
	//MARK: - Present AllCtegoryHeaderTableView
	public func title(cell: TitleCategoryCollectionCell){
		cell.categoryTitleLabel.text = cell.walpaper.title
	}
	public func wallpaperCollection(cell: AllCtegoryHeaderTableView){
		cell.wallpaperCollection.category = cell.category
		cell.collectionView.reloadData()
	}
	
	//MARK: - Present PodCategoryCollectionCell
	public func table(mainPreview: PodCategoryCellModal.Data){
		self.VM.VC.mainTable.mainPreview = mainPreview.data
		
		//Получаем ячейку таблицы с типом PodCategoryTableCell
		if let cell = self.VM.VC.mainTable.tableView.visibleCells.filter({ $0 is PodCategoryTableCell}).first {
			if let indexPath = self.VM.VC.mainTable.tableView.indexPath(for: cell) {
				//Обновляем ячейку с типом PodCategoryTableCell
				self.VM.VC.mainTable.tableView.reloadRows(at: [indexPath], with: .automatic)
			}
		}
	}
	
	public func podCategoryCollection(cell: PodCategoryTableCell){
		cell.podCategoryCollection.previews = cell.mainPreview?.previews
		cell.collectionView.reloadData()
	}
	
	public func image(cell: PodCategoryCollectionCell){
		guard let image = cell.preview?.previewURL else { return }
		guard let url   = self.VM.server.urls.get(type: .store(image)).URL else { return }
		cell.previewImageView.isSkeletonable = true
    cell.previewImageView.skeletonCornerRadius = 0
    cell.previewImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
      
      cell.previewImageView.sd_setImage(with: url) { (image, error, _, url) in
        
        if let error = error {
          print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
        } else {
          //cell.previewImageView.image = image!.sd_flippedImage(withHorizontal: true, vertical: true)
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            cell.previewImageView.hideSkeleton()
          }
        }
      }
    }
	}
}
//MARK: - Initial
extension PresentMain {
  
  //MARK: - Inition
  convenience init(viewModal: MainViewModal) {
    self.init()
    self.VM = viewModal
  }
}

