import UIKit

class ServerMain: VMManager {
  
  //MARK: - Public variable
  public var VM: MainViewModal!
  
	public func getMainData(completion: @escaping Clousure<DECMainData?>){
		//Request
		self.VM.server.request(requestType: GETMainNik()) { [weak self] (serverResult) in
			guard let self = self else { return }
			//Response
			switch serverResult {
				case .error(let error):
					guard let error = error else { return }
					print("Error server data: class: ServerMain ->, function: getMainData -> data: DECMainData ->, description: ", error.localizedDescription)
					AlertEK.dеfault(title: .error, message: .noJSON){
						self.getMainData(completion: completion)
					}
				//Susses
				case .object(let object):
					let mainData = object as! DECMainData
					completion(mainData)
					print("Succesful data: class: ServerMain ->, function: getMainData ->, data: DECMainData")
					
			}
		}
	}
	public func getDetailCategory(endPath: String?, completion: @escaping Clousure<DECPreviewData?>){
		//Request
		self.VM.server.request(requestType: GETCategoryDetail(), data: endPath) { [weak self]  (serverResult) in
			guard let self = self else { return }
			//Response
			switch serverResult {
				case .error(let error):
					guard let error = error else { return }
					print("Error server data: class: ServerMain ->, function: getDetailCategory -> data: DECCategoryDetail ->, description: ", error.localizedDescription)
					AlertEK.dеfault(title: .error, message: .noJSON){
						self.getDetailCategory(endPath: endPath, completion: completion)
					}
				//Susses
				case .object(let object):
					let categoryDetail = object as? DECPreviewData
					completion(categoryDetail)
					print("Succesful data: class: ServerMain ->, function: getDetailCategory ->, data: DECCategoryDetail")
					
			}
		}
	}
}
//MARK: - Initial
extension ServerMain {
  
  //MARK: - Inition
  convenience init(viewModal: MainViewModal) {
    self.init()
    self.VM = viewModal
  }
}


