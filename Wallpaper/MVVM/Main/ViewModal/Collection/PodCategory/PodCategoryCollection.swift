
import UIKit

class PodCategoryCollection : NSObject {
  
  //MARK: - Variable
  public var collectionView: UICollectionView!
  public var viewModal     : MainViewModal!
	public var previews      : [DECPreview]!
  public var tableView     : UITableView!
}

//MARK: - Delegate CollectionView
extension PodCategoryCollection: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    self.viewModal.managers.logic.pushHomeStaticVC(previews: previews, indexPath: indexPath)
  }
}

//MARK: - DataSource CollectionView
extension PodCategoryCollection: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    self.collectionView = collectionView
  return self.previews?.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
    switch true {
      case self.viewModal.indexPathAdvertising.contains(indexPath.row) && !GDPurchacesActions.shared.isPurchaces:
        let cell = NativeAddCollectionCell().collectionCell(collectionView, indexPath: indexPath)
        cell.configure(viewController: self.viewModal.VC)
        return cell
      default:
        let cell = PodCategoryCollectionCell().collectionCell(collectionView, indexPath: indexPath)
        cell.configure(viewModal: viewModal, preview: previews?[indexPath.row])
        return cell
    }
  }
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    if scrollView.contentOffset.y > 0 {
      tableView.scrollTo(.bottom)
    }
  }
}
//MARK: - DelegateFlowLayout  CollectionView
extension PodCategoryCollection: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		
    switch true {
      case self.viewModal.indexPathAdvertising.contains(indexPath.row) && !GDPurchacesActions.shared.isPurchaces:
        let width : CGFloat = collectionView.bounds.width - 10
        let height: CGFloat = 320
        return .init(width: width, height: height)
      default:
        let width : CGFloat = collectionView.bounds.width  / 3 - 10
        let height: CGFloat = width + (width / 2)
        return .init(width: width, height: height)
    }
  }
  
}


