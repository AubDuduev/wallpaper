
import UIKit

class WallpaperCollection: NSObject {
  
  //MARK: - Variable
  public var collectionView: UICollectionView!
  public var viewModal     : MainViewModal!
	public var category      : GDCategory!
	public var tableSection  : Int!
}

//MARK: - Delegate CollectionView
extension WallpaperCollection: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    self.viewModal.managers.logic.pushHomeLiveVC(category: self.category, indexPath: indexPath)
  }
	func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
		guard self.tableSection != nil else { return }
		self.viewModal.managers.logic.setCurrentPage(tableSection: self.tableSection, row: indexPath.row)
	}
}

//MARK: - DataSource CollectionView
extension WallpaperCollection: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    self.collectionView = collectionView
		
		switch self.category.alias {
			case .categoryflag:
				return self.category.flags?.count ?? 0
			default:
				return self.category?.wallpapers?.count ?? 0
		}
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		switch self.category.alias {
			
			case .categoryflag:
				let cell = FlagsCollectionCell().collectionCell(collectionView, indexPath: indexPath)
				cell.configure(viewModal: self.viewModal, flag: self.category.flags?[indexPath.row])
				return cell
				
			case .categoriesAll:
				let cell = TitleCategoryCollectionCell().collectionCell(collectionView, indexPath: indexPath)
				cell.configure(viewModal     : self.viewModal,
											 walpaper      : self.category.wallpapers?[indexPath.row],
											 indexPath     : indexPath,
											 collectionView: collectionView)
				return cell
				
			default:
				let cell = WallpaperCollectionCell().collectionCell(collectionView, indexPath: indexPath)
				cell.configure(viewModal: viewModal,
											 category : self.category,
                       indexPath: indexPath)
				return cell
		}
  }
}
//MARK: - DelegateFlowLayout  CollectionView
extension WallpaperCollection: UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		switch self.category.alias {
			
			case .categoryflag:
				let width : CGFloat = collectionView.bounds.height
				let height: CGFloat = collectionView.bounds.height
				return .init(width: width, height: height)
				
			case .categoriesAll:
				let text = self.category.wallpapers?[indexPath.row].title
				let width : CGFloat = text?.heightText(font: .set(.aquireRegular, 25)).width ?? 0
				let height: CGFloat = collectionView.bounds.height
				return .init(width: width, height: height)
				
			default:
				let width : CGFloat = collectionView.bounds.width
				let height: CGFloat = collectionView.bounds.height
				return .init(width: width, height: height)
		}
	}
}


