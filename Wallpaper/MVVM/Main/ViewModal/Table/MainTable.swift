
import UIKit

class MainTable: NSObject {
  
  public var viewModal    : MainViewModal!
  public var tableView    : UITableView!
  public var categories   : [GDCategory]!
  public var mainPreview  : DECMainPreview!
  public var footers      = [MainFooterTableView]()
  public var headerStates = [HeaderChoiceState]()
  
}
//MARK: - Delegate
extension MainTable: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
  }
}
//MARK: - DataSources
extension MainTable: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    self.tableView = tableView
    return 1
  }
  func numberOfSections(in tableView: UITableView) -> Int {
    self.tableView = tableView
    return self.categories?.count ?? 0
  }
  //MARK: - Cell
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let category = self.categories?[indexPath.section]
    //Native adds cell
    if 1 == indexPath.row {
      let cell = NativeAddsTableCell().loadNib()
      cell.configure(viewController: self.viewModal.VC)
      return cell
    }
    //Other cells
    switch category?.alias {
      case .categoriesAll:
        let cell = PodCategoryTableCell().loadNib()
        cell.configure(viewModal: self.viewModal, mainPreview: self.mainPreview)
        return cell

      default:
        let cell = MainTableCell().tableCell()
        cell.configure(viewModal: self.viewModal,
                       category : self.categories?[indexPath.section],
                       section  : indexPath.section)
        return cell
    }
  }
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    //Native adds cell
    if 1 == indexPath.row {
      return 320
    }
    //Other cells
    let category = self.categories?[indexPath.section]
    switch category?.alias {
      case .categoryflag:
        return 100
      case .categoriesAll:
        return 600
      default:
        return 200
    }
  }
  //MARK: - Header
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    let category = self.categories?[section]
    switch category?.alias {
      
      case .categoriesAll:
        let header = AllCtegoryHeaderTableView().loadNib()
        header.configure(viewModal: self.viewModal, category: category)
        return header
        
      default:
        let header = MainHeaderTableView().loadNib()
        header.configure(viewModal: self.viewModal,
                         category : self.categories?[section],
                         section  : section)
        return header
    }
    
  }
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == 0 {
      return 0
    } else {
      return 53
    }
  }
  //MARK: - Footer
  func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let footer = MainFooterTableView().loadNib()
    footer.configure(viewModal: self.viewModal, category: self.categories?[section])
    self.footers.append(footer)
    return footer
  }
  func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    
    let category = self.categories?[section]
    switch category?.typeCategory {
      case .category:
        return 0
      case .categories where category?.alias == .categoriesAll:
        return 0
      case .other:
        return 0
      default:
        return 30
    }
  }
}
