
import Foundation

enum PodCategoryCellModal {
	
	case loading(String?)
	case getData(String?)
	case errorHandler(DECPreviewData?)
	case presentData(PodCategoryCellModal.Data)
	
	struct Data {
		
		let data: DECMainPreview!
		
		init(mainPreview: DECPreviewData){
			self.data = mainPreview.data
		}
	}
}

