
import Foundation

enum MainModal {
	
	case loading
	case getData
	case errorHandler(DECMainData?)
	case presentData(Data)
	
	struct Data {
		
		let decMainData : DECMainData!
		let categories  : [GDCategory]!
		
		init(decMainData: DECMainData) {
			
			self.decMainData  = decMainData
			self.categories   = decMainData.categories
		}
	}
}



