
import Foundation

class MainManagers: VMManagers {
  
  let setup    : SetupMain!
  let server   : ServerMain!
  let present  : PresentMain!
  let logic    : LogicMain!
  let animation: AnimationMain!
  let router   : RouterMain!
  
  init(viewModal: MainViewModal) {
    
    self.setup     = SetupMain(viewModal: viewModal)
    self.server    = ServerMain(viewModal: viewModal)
    self.present   = PresentMain(viewModal: viewModal)
    self.logic     = LogicMain(viewModal: viewModal)
    self.animation = AnimationMain(viewModal: viewModal)
    self.router    = RouterMain(viewModal: viewModal)
  }
}

