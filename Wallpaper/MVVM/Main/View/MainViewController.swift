import UIKit

class MainViewController: UIViewController {
	
	//MARK: - ViewModel
	public var viewModal: MainViewModal!
	
	//MARK: - Public variable
	
	
	//MARK: - Outlets
	@IBOutlet weak var mainTable: MainTable!
	
	//MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initViewModal()
		self.viewModal.viewDidLoad()
	}
	private func initViewModal(){
		self.viewModal = MainViewModal(viewController: self)
	}
}
