
import UIKit
import NVActivityIndicatorView

class ActivityCollectionCell: UICollectionViewCell, LoadNidoble {
  
 
  private var viewModal           : MainViewModal!
  private var animationLoadingView: NVActivityIndicatorView!
  
  @IBOutlet weak var activityView: UIView!
  
  public func configure(viewModal: MainViewModal?){
    
    self.animationLoading()
  }
  
  private func animationLoading(){
    let rect = CGRect(x: 0, y: 0, width: 30, height: 30)
    self.animationLoadingView = NVActivityIndicatorView(frame  : rect,
                                                        type   : .ballSpinFadeLoader,
                                                        color  : .white,
                                                        padding: 0)
    animationLoadingView.startAnimating()
    self.activityView.addSubview(animationLoadingView)
    animationLoadingView.snp.makeConstraints { (animationLoadingView) in
      animationLoadingView.edges.equalTo(self.activityView)
    }
  }
  override func prepareForReuse() {
    super.prepareForReuse()
    self.animationLoadingView?.removeFromSuperview()
  }
}
