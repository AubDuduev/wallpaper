
import UIKit

class PodCategoryCollectionCell: UICollectionViewCell, LoadNidoble {
  
  private var viewModal: MainViewModal!
	
	public var preview: DECPreview?
	
	@IBOutlet weak var previewImageView: UIImageView!
  
	public func configure(viewModal: MainViewModal?, preview: DECPreview?){
		self.viewModal = viewModal
		self.preview   = preview
		self.viewModal.managers.present.image(cell: self)
  }
  override func prepareForReuse() {
    super.prepareForReuse()
    self.viewModal.managers.logic.prepareForReuse(image: self.previewImageView)
  }
	override func layoutSubviews() {
		super.layoutSubviews()
		self.viewModal.managers.setup.image(cell: self)
	}
}
