
import UIKit

class TitleCategoryCollectionCell: UICollectionViewCell, LoadNidoble {
	
	private var viewModal: MainViewModal!
	
	public var walpaper      : DECWallpaper!
	public var collectionView: UICollectionView!
	public var indexPath     : IndexPath!
	
	@IBOutlet weak var categoryTitleLabel: UILabel!
	@IBOutlet weak var choiceImageView   : UIImageView!
	
	public func configure(viewModal: MainViewModal?, walpaper: DECWallpaper?, indexPath: IndexPath, collectionView: UICollectionView){
		self.viewModal      = viewModal
		self.walpaper       = walpaper
		self.collectionView = collectionView
		self.indexPath      = indexPath
		self.viewModal.managers.present.title(cell: self)
		self.viewModal.managers.logic.testStateCell(cell: self)
	}
  override func prepareForReuse() {
    super.prepareForReuse()
    //self.viewModal.managers.logic.prepareForReuse(image: self.choiceImageView)
  }
	@IBAction func getDetailCategoryButton(button: UIButton){
		self.viewModal.managers.logic.getDataPodCategory(cell: self)
		self.viewModal.managers.logic.changeStateHeader(cell: self)
		self.viewModal.managers.logic.testStateCell(cell: self)
	
	}
}
