
import UIKit

class FlagsCollectionCell: UICollectionViewCell, LoadNidoble {
  
  private var viewModal: MainViewModal!
	
	public var flag: DECFlag!
	
	@IBOutlet weak var flagImageView: UIImageView!
  
	public func configure(viewModal: MainViewModal?, flag: DECFlag?){
		self.viewModal = viewModal
		self.flag      = flag
		self.viewModal.managers.present.image(cell: self)
		self.viewModal.managers.setup.flagImageView(cell: self)
  }
  
	override func layoutSubviews() {
		super.layoutSubviews()
		self.viewModal.managers.setup.flagImageView(cell: self)
	}
  override func prepareForReuse() {
    super.prepareForReuse()
    self.viewModal.managers.logic.prepareForReuse(image: self.flagImageView)
  }
}
