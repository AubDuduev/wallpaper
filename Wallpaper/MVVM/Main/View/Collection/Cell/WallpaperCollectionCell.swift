
import UIKit

class WallpaperCollectionCell: UICollectionViewCell, LoadNidoble {
  
  private var viewModal: MainViewModal!
	
	public var wallpaper: DECWallpaper!
	public var category : GDCategory!
	public var indexPath: IndexPath!
	
	@IBOutlet weak var wallpaperImageView: UIImageView!
	
	public func configure(viewModal: MainViewModal?, category: GDCategory?, indexPath: IndexPath){
		self.viewModal = viewModal
		self.indexPath = indexPath
    self.wallpaper = category?.wallpapers[indexPath.row]
		self.category  = category
		self.viewModal.managers.present.image(cell: self)
  }
	override func layoutSubviews() {
		super.layoutSubviews()
		self.viewModal.managers.setup.wallpaperImageView(cell: self)
	}
  override func prepareForReuse() {
    super.prepareForReuse()
    self.viewModal.managers.logic.prepareForReuse(image: self.wallpaperImageView)
  }
	@IBAction func pushDetailButton(button: UIButton){
    self.viewModal.managers.logic.pushDetailVCOrCategoriesVC(category : self.category,
                                                             indexPath: self.indexPath)
	}
}
