
import UIKit

class AllCtegoryHeaderTableView: UITableViewHeaderFooterView, LoadNidoble {
  
  private var viewModal: MainViewModal!
	
	public var category              : GDCategory!
	public var wallpaperCollection   = WallpaperCollection()
	@IBOutlet weak var collectionView: UICollectionView!
  
	public func configure(viewModal: MainViewModal?, category: GDCategory?){
		self.viewModal = viewModal
		self.category  = category
		self.viewModal.managers.setup.wallpaperCollection(cell: self)
		self.viewModal.managers.present.wallpaperCollection(cell: self)
		self.viewModal.managers.logic.logicPresentFirstPodCategoryAfterLoad(cell: self)
		self.viewModal.managers.logic.createHeaderState(cell: self)
    self.viewModal.managers.logic.indexPathAdvertising(wallpapers: category?.wallpapers)
    self.viewModal.managers.logic.indexPathActivity(wallpapers: category?.wallpapers)
  }
  
}
