
import UIKit

class MainHeaderTableView: UITableViewHeaderFooterView, LoadNidoble {
  
 
  private var viewModal: MainViewModal!
	
	public var category  : GDCategory!
  public var indexPath : IndexPath!
	
	@IBOutlet weak var categoryNameLabel: UILabel!
  
  public func configure(viewModal: MainViewModal?, category: GDCategory?, section: Int){
		self.category  = category
		self.viewModal = viewModal
    self.indexPath = IndexPath(row: 0, section: section)
		self.viewModal.managers.present.title(header: self)
  }
  @IBAction func pushDetailButton(button: UIButton){
    self.viewModal.managers.logic.pushDetailVCOrCategoriesVC(category : self.category,
                                                             indexPath: self.indexPath)
  }
}
