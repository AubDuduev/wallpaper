
import UIKit

class MainFooterTableView: UITableViewHeaderFooterView, LoadNidoble {
  
  private var viewModal: MainViewModal!
	
	public var category: GDCategory!
	
	@IBOutlet weak var countWallpaperControl: UIPageControl!
  
  public func configure(viewModal: MainViewModal?, category: GDCategory?){
		self.viewModal = viewModal
		self.category  = category
		self.viewModal.managers.logic.countWallpaperControl(footer: self)
  }
  
}
