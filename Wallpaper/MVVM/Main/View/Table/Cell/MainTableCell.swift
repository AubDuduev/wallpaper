
import UIKit

class MainTableCell: UITableViewCell, LoadNidoble {
  
  private var viewModal: MainViewModal!
	
	public var category: GDCategory!
	public var section : Int!
	
	public var wallpaperCollection            = WallpaperCollection()
	@IBOutlet weak var wallpaperCollectionView: UICollectionView!
  
	public func configure(viewModal: MainViewModal?, category: GDCategory?, section: Int){
		self.viewModal = viewModal
		self.category  = category
		self.section   = section
		self.viewModal?.managers.setup.wallpaperCollection(cell: self)
		self.viewModal?.managers.present.wallpaperCollection(cell: self)
  }
}
