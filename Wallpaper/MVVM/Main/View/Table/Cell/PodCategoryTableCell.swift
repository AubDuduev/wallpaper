
import UIKit

class PodCategoryTableCell: UITableViewCell, LoadNidoble {
  
  private var viewModal: MainViewModal!
	
	public var mainPreview           : DECMainPreview!
	public var podCategoryCollection = PodCategoryCollection()
	@IBOutlet weak var collectionView: UICollectionView!
  
	public func configure(viewModal: MainViewModal, mainPreview: DECMainPreview?){
		self.viewModal   = viewModal
		self.mainPreview = mainPreview
		self.viewModal?.managers.setup.podCategoryCollection(cell: self)
		self.viewModal?.managers.present.podCategoryCollection(cell: self)
  }
  
}
