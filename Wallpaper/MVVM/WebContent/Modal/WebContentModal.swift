
import UIKit

enum WebContentModal {
	
	case loading
	case errorHandler(WebContentData)
	case presentData(WebContentData)
	
}

struct WebContentData {
	
	let urlString: URLString.Url?
	let title    : String!
	
	init(urlString: URLString.Url?, title: WebContentTitle){
		self.urlString = urlString
		self.title     = title.rawValue
	}
}

enum WebContentTitle: String {
	case Instagramm
	case privacy    = "Privacy Policy"
	case useTerms   = "Use conditions"
}
