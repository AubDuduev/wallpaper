
import Foundation

class WebContentManagers: VMManagers {
  
  let setup    : SetupWebContent!
  let server   : ServerWebContent!
  let present  : PresentWebContent!
  let logic    : LogicWebContent!
  let animation: AnimationWebContent!
  let router   : RouterWebContent!
  
  init(viewModal: WebContentViewModal) {
    
    self.setup     = SetupWebContent(viewModal: viewModal)
    self.server    = ServerWebContent(viewModal: viewModal)
    self.present   = PresentWebContent(viewModal: viewModal)
    self.logic     = LogicWebContent(viewModal: viewModal)
    self.animation = AnimationWebContent(viewModal: viewModal)
    self.router    = RouterWebContent(viewModal: viewModal)
  }
}

