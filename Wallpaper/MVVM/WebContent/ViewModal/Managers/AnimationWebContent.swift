import UIKit

class AnimationWebContent: VMManager {
  
  //MARK: - Public variable
  public var VM: WebContentViewModal!
  
  
}
//MARK: - Initial
extension AnimationWebContent {
  
  //MARK: - Inition
  convenience init(viewModal: WebContentViewModal) {
    self.init()
    self.VM = viewModal
  }
}

