import Foundation

class LogicWebContent: VMManager {
  
  //MARK: - Public variable
  public var VM: WebContentViewModal!
  
  
}
//MARK: - Initial
extension LogicWebContent {
  
  //MARK: - Inition
  convenience init(viewModal: WebContentViewModal) {
    self.init()
    self.VM = viewModal
  }
}
