
import UIKit
import NVActivityIndicatorView

class SetupWebContent: VMManager {
  
  //MARK: - Public variable
  public var VM: WebContentViewModal!
  
	public func startLoading(){
		let rect = CGRect(x: 0, y: 0, width: 250, height: 250)
		self.VM.loading = NVActivityIndicatorView(frame  : rect,
																							type   : .ballBeat,
																							color  : .black,
																							padding: 10)
		self.VM.loading.center = self.VM.VC.view.center
		self.VM.VC.view.addSubview(self.VM.loading)
		self.VM.loading.startAnimating()
	}
}
//MARK: - Initial
extension SetupWebContent {
  
  //MARK: - Inition
  convenience init(viewModal: WebContentViewModal) {
    self.init()
    self.VM = viewModal
  }
}


