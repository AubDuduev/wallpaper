
import UIKit

class PresentWebContent: VMManager {
  
  //MARK: - Public variable
  public var VM: WebContentViewModal!
  
	//MARK: - WebContentViewController
	public func webContentView(urls: URLString.Url){
		self.VM.VC.webContentView.set(urls: urls)
	}
	public func titleNavBar(title: String){
		self.VM.VC.titleNavBarLabel.text = title
	}
}
//MARK: - Initial
extension PresentWebContent {
  
  //MARK: - Inition
  convenience init(viewModal: WebContentViewModal) {
    self.init()
    self.VM = viewModal
  }
}

