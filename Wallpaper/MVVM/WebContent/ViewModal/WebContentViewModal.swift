
import Foundation
import NVActivityIndicatorView

class WebContentViewModal: VMManagers {
	
	public var webContentModal: WebContentModal = .loading {
		didSet{
			self.presentData()
		}
	}
  
  //MARK: - Public variable
  public var managers: WebContentManagers!
  public var VC      : WebContentViewController!
	public var loading : NVActivityIndicatorView!
	
	public func viewDidLoad() {
		self.webContentModal = .loading
	}
	
	private func presentData(){
		
		switch webContentModal {
			//1 - Загрузка
			case .loading:
				self.managers.setup.startLoading()
				self.webContentModal = .errorHandler(self.VC.webContentData)
				
			//3 - Обрабка ошибок запроса
			case .errorHandler(let webContentData):
				//Обрабатываем на ошибки полученные данные
				guard webContentData.urlString != nil else { return }
				self.webContentModal = .presentData(webContentData)
				
			//4 - Отоброжение данных на kонтроллере
			case .presentData(let webContentData):
				guard let urls = webContentData.urlString else { return }
				self.managers.present.webContentView(urls: urls)
				self.managers.present.titleNavBar(title: webContentData.title)
				self.loading.stopAnimating()
		}
	}
}
//MARK: - Initial
extension WebContentViewModal {
  
  convenience init(viewController: WebContentViewController) {
    self.init()
    self.VC       = viewController
    self.managers = WebContentManagers(viewModal: self)
  }
}
