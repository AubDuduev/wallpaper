import UIKit
import WebKit

class WebContentViewController: UIViewController {
	
	//MARK: - ViewModel
	public var viewModal: WebContentViewModal!
	
	//MARK: - Public variable
	public var webContentData: WebContentData!
	
	//MARK: - Outlets
	@IBOutlet weak var webContentView  : WKWebView!
	@IBOutlet weak var titleNavBarLabel: UILabel!
	
	//MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initViewModal()
		self.viewModal.viewDidLoad()
	}
	private func initViewModal(){
		self.viewModal = WebContentViewModal(viewController: self)
	}
}
