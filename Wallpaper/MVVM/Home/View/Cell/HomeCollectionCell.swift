
import UIKit
import SnapKit
import SkeletonView

class HomeCollectionCell: UICollectionViewCell, LoadNidoble {
  
 
  private var viewModal: HomeViewModal!
	
	public var preview: DECPreview!
	
	@IBOutlet weak var wallpaperImageView: UIImageView!
	@IBOutlet weak var buttonsView       : UIView!
	@IBOutlet weak var likeButton        : UIButton!
  @IBOutlet weak var likeLabel         : UILabel!
  
	public func configure(viewModal: HomeViewModal?, preview: DECPreview?){
		self.preview   = preview
		self.viewModal = viewModal
		self.viewModal.managers.present.image(cell: self)
    self.viewModal.managers.present.countLike(likelabel: self.likeLabel)
    self.viewModal.managers.setup.buttonsView(buttonsView: self.buttonsView)
		self.viewModal.managers.logic.testSave(cellPreview: self.preview, likeButton: self.likeButton)
    self.viewModal.managers.setup.preview(buttonsView: self.buttonsView)
    self.viewModal.managers.animation.bottomAddsView()
  }

	@IBAction func saveButton(button: UIButton){
    self.viewModal.managers.logic.save(cellPreview: self.preview)
    self.viewModal.managers.logic.testSave(cellPreview: self.preview, likeButton: self.likeButton)
	}
  @IBAction func downloadButton(button: UIButton){
    self.viewModal.managers.logic.savePhoto(image: wallpaperImageView.image!)
  }
  @IBAction func previewButton(button: UIButton){
    self.viewModal.managers.animation.preview(buttonsView: self.buttonsView)
    self.viewModal.managers.animation.bottomAddsView()
  }
}
