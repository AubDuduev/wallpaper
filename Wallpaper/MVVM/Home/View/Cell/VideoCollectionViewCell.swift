
import UIKit

class VideoCollectionViewCell: UICollectionViewCell, LoadNidoble {


  @IBOutlet weak var videoView  : VideoPlayerView!
  @IBOutlet weak var buttonsView: UIView!
  @IBOutlet weak var saveButton : UIButton!
  @IBOutlet weak var likeButton : UIButton!
  @IBOutlet weak var likeLabel  : UILabel!
  
  private var viewModal: HomeViewModal!
  private var preview  : DECPreview?
  
  public func configure(viewModal: HomeViewModal?, preview: DECPreview?){
    self.viewModal = viewModal
    self.preview   = preview
    self.viewModal.managers.present.countLike(likelabel: self.likeLabel)
    self.viewModal?.managers.setup.videoView(cell: self, stringURL: preview?.url)
    self.viewModal?.managers.setup.videoView(cell: self)
    self.viewModal.managers.setup.buttonsView(buttonsView: self.buttonsView)
    self.viewModal.managers.logic.testSave(cellPreview: self.preview, likeButton: self.likeButton)
    self.viewModal.managers.setup.preview(buttonsView: self.buttonsView)
    self.viewModal.managers.animation.bottomAddsView()
  }
  override func layoutSubviews() {
    super.layoutSubviews()
    self.viewModal?.managers.setup.videoView(cell: self)
  }
  @IBAction func likeButton(button: UIButton){
    self.viewModal.managers.logic.save(cellPreview: self.preview)
    self.viewModal.managers.logic.testSave(cellPreview: self.preview, likeButton: self.likeButton)
  }
  @IBAction func saveButton(button: UIButton){
    self.viewModal.managers.logic.saveVideo(urlString: preview?.url)
  }
  
  @IBAction func previewButton(button: UIButton){
    self.viewModal.managers.animation.preview(buttonsView: self.buttonsView)
    self.viewModal.managers.animation.bottomAddsView()
  }
}
