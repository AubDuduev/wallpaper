import UIKit

class HomeViewController: UIViewController {
	
	//MARK: - ViewModel
	public var viewModal: HomeViewModal!
	
	//MARK: - Public variable
  public var typePresentHomeData: TypePresentHomeData = .server
	
	//MARK: - Outlets
	@IBOutlet weak var homeCollection     : HomeCollection!
	@IBOutlet weak var homeCollectionView : UICollectionView!
  @IBOutlet weak var navBarView         : UIView!
  @IBOutlet weak var backButton         : UIButton!
  @IBOutlet weak var advertisingView    : UIView!
  @IBOutlet weak var bottomAddsView     : UIView!
  @IBOutlet weak var centerAddsView     : UIView!
  @IBOutlet weak var loadingWalpaperView: UIView!
	
	//MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initViewModal()
		self.viewModal.viewDidLoad()
	}
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
	private func initViewModal(){
		self.viewModal = HomeViewModal(viewController: self)
	}
}


