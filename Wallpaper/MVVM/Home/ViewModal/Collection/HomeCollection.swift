//
//  HomeCollection.swift
//  Wallpaper

import UIKit

class HomeCollection: NSObject {
  
  //MARK: - Variable
  public var collectionView: UICollectionView!
  public var viewModal     : HomeViewModal!
	public var previewData   : DECPreviewData!
  public var previews      : [DECPreview]!
}

//MARK: - Delegate CollectionView
extension HomeCollection: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
  }
  func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    self.viewModal.managers.logic.logicPresentNativeCenterAdds(show: true)
  }
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    
  }
}

//MARK: - DataSource CollectionView
extension HomeCollection: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    self.collectionView = collectionView
    
    switch self.viewModal.VC.typePresentHomeData {
      
      case .server:
        return self.previewData?.data?.previews?.count ?? 0
        
      case .local(_):
        return self.previews?.count ?? 0
        
      case .localLive(_):
        return self.previews?.count ?? 0
    }
		
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    switch self.viewModal.VC.typePresentHomeData {
      case .local:
        let cell = HomeCollectionCell().collectionCell(collectionView, indexPath: indexPath)
        cell.configure(viewModal: self.viewModal, preview: self.previews?[indexPath.row])
        return cell
        
      case .server:
        let cell = VideoCollectionViewCell().collectionCell(collectionView, indexPath: indexPath)
        cell.configure(viewModal: self.viewModal, preview: self.previewData.data.previews?[indexPath.row])
        return cell
        
      case .localLive:
        let cell = VideoCollectionViewCell().collectionCell(collectionView, indexPath: indexPath)
        cell.configure(viewModal: self.viewModal, preview: self.previews?[indexPath.row])
        return cell
    }
    
  }
}
//MARK: - DelegateFlowLayout  CollectionView
extension HomeCollection: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let width : CGFloat = collectionView.bounds.width
    let height: CGFloat = collectionView.bounds.height
    return .init(width: width, height: height)
  }
  
}


