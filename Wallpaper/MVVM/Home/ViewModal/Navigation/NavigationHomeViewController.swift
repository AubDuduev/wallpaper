
import UIKit

extension HomeViewController {
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    switch segue.ID() {
      case .HomeVC_NativeAddBottomVC:
        let nativeAdvertisingVC = (segue.destination as? NativeAdvertisingViewController)
        nativeAdvertisingVC?.presentationType = .HomeBottomView
      case .HomeVC_NativeAddCenterVC:
        let nativeAdvertisingVC = (segue.destination as? NativeAdvertisingViewController)
        nativeAdvertisingVC?.presentationType = .HomeCenterView
        nativeAdvertisingVC?.dissmissAdds = { [weak self] in
          guard let self = self else { return }
          self.viewModal.managers.logic.logicPresentNativeCenterAdds(show: false)
        }
      default:
      break
    }
  }
}
