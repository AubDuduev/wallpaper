
import SkeletonView
import SDWebImage
import UIKit

class PresentHome: VMManager {
  
  //MARK: - Public variable
  public var VM: HomeViewModal!
  
	//MARK: - Present HomeViewController
	public func collection(homePresentData: HomePresentData){
    self.VM.VC.homeCollection.previewData = homePresentData.previewData
    self.VM.VC.homeCollection.previews    = homePresentData.previews
    if homePresentData.indexPath == nil {
      self.VM.VC.homeCollectionView.reloadData()
    } else {
      self.VM.VC.homeCollectionView.reloadData()
      self.VM.VC.homeCollectionView.scrollToItem(at: homePresentData.indexPath,
                                                 at: .centeredVertically,
                                                 animated: false)
    }
	}
  
	//MARK: - Present HomeCollectionCell
	public func image(cell: HomeCollectionCell){
		guard let image = cell.preview?.previewURL else { return }
		guard let url   = self.VM.server.urls.get(type: .store(image)).URL else { return }
		cell.wallpaperImageView.isSkeletonable = true
		cell.wallpaperImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .set(.blueProject)))
		cell.wallpaperImageView.sd_setImage(with: url) { (image, error, _, url) in
			
			if let error = error {
				print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
			} else {
				//cell.wallpaperImageView.image = image!.sd_flippedImage(withHorizontal: true, vertical: true)
				cell.wallpaperImageView.hideSkeleton()
			}
		}
	}
  public func countLike(likelabel: UILabel){
    let count = Double.random(in: 4.0...5.0)
    likelabel.text = count.distanceType(.k)
  }
}
//MARK: - Initial
extension PresentHome {
  
  //MARK: - Inition
  convenience init(viewModal: HomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}

