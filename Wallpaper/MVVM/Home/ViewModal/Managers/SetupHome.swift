
import NVActivityIndicatorView
import UIKit
import SnapKit
import AnimatedGradientView
import AVKit

class SetupHome: VMManager {
  
  //MARK: - Public variable
  public var VM: HomeViewModal!
  
	//MARK: - Present HomeViewController
	public func homeCollection(){
		let collectionViewLayout = HomeCollectionViewLaytout()
		collectionViewLayout.sectionInset                  = .init(top: 0, left: 0, bottom: 0, right: 0)
		collectionViewLayout.sectionInsetReference         = .fromContentInset
		collectionViewLayout.minimumLineSpacing            = 0
		collectionViewLayout.minimumInteritemSpacing       = 0
		self.VM.VC.homeCollectionView.collectionViewLayout = collectionViewLayout
		self.VM.VC.homeCollection.viewModal                = self.VM
		self.VM.VC.homeCollectionView.reloadData()
	}
	public func startLoading(){
		let rect = CGRect(x: 0, y: 0, width: 100, height: 100)
		self.VM.loading = NVActivityIndicatorView(frame  : rect,
																							type   : .ballSpinFadeLoader,
																							color  : .white,
																							padding: 0)
		self.VM.loading.center = self.VM.VC.view.center
		self.VM.VC.view.addSubview(self.VM.loading)
		self.VM.loading.startAnimating()
	}
  public func navBar(){
    
    switch self.VM.VC.typePresentHomeData {
      
      //Прячем навигеишен бар
      case .server:
        self.VM.VC.navBarView.isHidden = true
        
      //Показываем навигеишен бар
      case .local:
        self.VM.VC.navBarView.isHidden = false
        
      //Показываем навигеишен бар
      case .localLive:
        self.VM.VC.navBarView.isHidden = false
      
    }
  }
	//MARK: - Present HomeCollectionCell
	public func buttonsView(buttonsView: UIView){
		buttonsView.cornerRadius(10, true)
	}
  public func preview(buttonsView: UIView){
    if !self.VM.previewIsState {
      buttonsView.transform = .identity
    } else {
      buttonsView.transform = CGAffineTransform(translationX: 100, y: 0)
    }
  }
  //MARK: -  VideoCollectionViewCell
  public func videoView(cell: VideoCollectionViewCell, stringURL: String?){
    guard let urlString = self.VM.server.urls.get(type: .store(stringURL)).string else { return }
    cell.videoView.isSkeletonable = true
    cell.videoView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: .black))
    cell.videoView.video(typeVideoURL: .network(urlString), typeVideo: .mp4)
    cell.videoView.controll.play()
    cell.videoView.controll.mute()
    self.addObserverReturnTimeVideoPalyer(cell: cell)
  }
  public func videoView(cell: VideoCollectionViewCell){
    cell.videoView.playerLayer.masksToBounds = true
    cell.videoView.videoGravity    = .resizeAspectFill
    cell.videoView.contentsGravity = .resizeAspectFill
  }
  private func addObserverReturnTimeVideoPalyer(cell: VideoCollectionViewCell){
    //Зацикливаем видео
    cell.videoView.controll.observerEnd = { isEnd in
      guard isEnd else { return }
      let slider: Float = 0
      let seconds = cell.videoView.player.duration()?.seconds ?? 0
      let value   = CMTimeValue(slider * seconds)
      let seeTime = CMTime(value: value, timescale: 1)
      cell.videoView.player.seek(to: seeTime)
      cell.videoView.controll.play()
    }
    //Сообщаем когда видео начинает вопроизводиться
    cell.videoView.controll.observerReady = { [weak self] isReady in
      
      //Срабатывает когда начинает воспроизводиться реклама
      guard let self = self, isReady else { return }
      //Убираем загрузку
      self.VM.loading.stopAnimating()
      
      //Если отображен банер с нативной рекламой убираем его через 2 секунды
      DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        //убираем блок рекламы
        self.VM.managers.logic.logicPresentNativeCenterAdds(show: false)
      }
    }
  }
  public func viewLoading(cell: VideoCollectionViewCell){
    let rect = CGRect(x: 0, y: 0, width: 44, height: 44)
    self.VM.loading = NVActivityIndicatorView(frame  : rect,
                                              type   : .ballSpinFadeLoader,
                                              color  : .white,
                                              padding: 0)
    cell.contentView.addSubview(self.VM.loading)
    self.VM.loading.snp.makeConstraints { (loading) in
      loading.center.equalTo(cell.contentView.center)
      loading.height.equalTo(100)
      loading.width.equalTo(100)
    }
    
    self.VM.loading.startAnimating()
  }
  public func loadingWalpaperView(){
    self.VM.VC.loadingWalpaperView.cornerRadius(15, true)
  }
}
//MARK: - Initial
extension SetupHome {
  
  //MARK: - Inition
  convenience init(viewModal: HomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}


