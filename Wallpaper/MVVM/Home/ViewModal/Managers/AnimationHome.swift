import UIKit

class AnimationHome: VMManager {
  
  //MARK: - Public variable
  public var VM: HomeViewModal!
  
  public func preview(buttonsView: UIView){
    
    
    UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .curveEaseInOut) {
      
      if self.VM.previewIsState {
        buttonsView.transform = .identity
        if let tabBar = self.VM.VC.tabBarController as? TabBarViewController {
          tabBar.tabBarView.transform = .identity
        }
        if let navBar = self.VM.VC.navigationController as? NavBarViewController {
          navBar.navBarView.transform = .identity
        }
        
        switch  self.VM.VC.typePresentHomeData {
          case .local:
            self.VM.VC.navBarView.transform = .identity
          case .localLive:
            self.VM.VC.navBarView.transform = .identity
          default:
            break
        }
        
      } else {
        buttonsView.transform = CGAffineTransform(translationX: 100, y: 0)
        if let tabBar = self.VM.VC.tabBarController as? TabBarViewController { 
          tabBar.tabBarView.transform = CGAffineTransform(translationX: 0, y: tabBar.tabBarView.bounds.height)
        }
        if let navBar = self.VM.VC.navigationController as? NavBarViewController {
          navBar.navBarView.transform = CGAffineTransform(translationX: 0, y: -navBar.navBarView.bounds.height)
        }
        switch  self.VM.VC.typePresentHomeData {
          case .local:
            self.VM.VC.navBarView.transform = CGAffineTransform(translationX: 0, y: -(self.VM.VC.navBarView.bounds.height * 2))
          case .localLive:
            self.VM.VC.navBarView.transform = CGAffineTransform(translationX: 0, y: -(self.VM.VC.navBarView.bounds.height * 2))
          default:
            break
        }
      }
      self.VM.previewIsState.toggle()
    }
  }
  public func bottomAddsView(){
    UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1.0, options: .curveEaseInOut) {
      if !self.VM.previewIsState {
        self.VM.VC.bottomAddsView.transform = .identity
      } else {
        let transform = self.VM.VC.view.frame.height / 2
        self.VM.VC.bottomAddsView.transform = CGAffineTransform(translationX: 0, y: transform)
      }
    }
  }
}
//MARK: - Initial
extension AnimationHome {
  
  //MARK: - Inition
  convenience init(viewModal: HomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}

