import UIKit

class ServerHome: VMManager {
  
	//MARK: - Public variable
	public var VM: HomeViewModal!
	
	public func getHomeData(completion: @escaping Clousure<DECPreviewData?>){
		//Request
		self.VM.server.request(requestType: GETFirstData()) { [weak self]  (serverResult) in
			guard let self = self else { return }
			//Response
			switch serverResult {
				case .error(let error):
					guard let error = error else { return }
					print("Error server data: class: ServerHome ->, function: getHomeData -> data: DECPreviewData ->, description: ", error.localizedDescription)
					AlertEK.dеfault(title: .error, message: .noJSON){
						self.getHomeData(completion: completion)
					}
				//Susses
				case .object(let object):
					let previewData = object as! DECPreviewData
					completion(previewData)
					print("Succesful data: class: ServerHome ->, function: getHomeData ->, data: DECPreviewData")
					
			}
		}
	}
}
//MARK: - Initial
extension ServerHome {
  
  //MARK: - Inition
  convenience init(viewModal: HomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}


