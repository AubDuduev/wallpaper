import Foundation
import UIKit
import SnapKit

class LogicHome: VMManager {
  
  //MARK: - Public variable
  public var VM: HomeViewModal!
  
	public func save(cellPreview: DECPreview?){
    guard let cellPreview = cellPreview, let name = cellPreview.url else { return }
		self.VM.coreData.fetchName(object: CDPreview(), key: .url, name: name) { (cdPreview) in
			if cdPreview != nil {
				self.VM.coreData.delete(object: cdPreview)
			} else {
				let newPreview = self.VM.coreData.create(object: CDPreview.self)
				newPreview.previewURL = cellPreview.previewURL
				newPreview.url        = cellPreview.url
				self.VM.coreData.edit()
			}
		}
	}
  public func testSave(cellPreview: DECPreview?, likeButton: UIButton){
    guard let cellPreview = cellPreview, let name = cellPreview.url else { return }
		self.VM.coreData.fetchName(object: CDPreview(), key: .url, name: name) { (preview) in
			if preview != nil {
        likeButton.tintColor = .red
			} else {
        likeButton.tintColor = .white
			}
		}
	}
  
  public func testNotification(){
    self.VM.notification.getNotificationSettings { [weak self] (succes) in
      DispatchQueue.main.async { [weak self] in
        guard let self = self else { return }
        if succes {
          print("Notification is presented" )
        } else {
          self.VM.managers.router.push(.NotificationVC)
        }
      }
    }
  }
  public func logicPresentAfterSavePhoto(){
    //Показываем рекламу если показывали обучающий экран
    if StoreProject.shared.getBool(key: .isSaveWalpaper) {
      
      if !GDPurchacesActions.shared.isPurchaces {
        
        self.VM.managers.router.push(.InterstitialVC)
        self.VM.returnInterstitialVC = {
          //Показываем экран удачного сохранения
          self.VM.managers.router.push(.SaveEndVC)
        }
      } else {
        //Показываем экран удачного сохранения
        self.VM.managers.router.push(.SaveEndVC)
      }
    //Показываем обучающий экран
    } else {
      self.VM.managers.router.push(.SaveWalpaperVC)
      StoreProject.shared.saveBool(key: .isSaveWalpaper, value: true)
    }
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      guard let self = self else { return }
      self.VM.loading.stopAnimating()
    }
  }
  public func savePhoto(image: UIImage){
    //Загрузка
    self.VM.managers.setup.startLoading()
    //Менеджер для сохронения фото
    PhotoManger.shared.save(image: image)
    //После сохранения вызывается
    PhotoManger.shared.completionSavePhoto = { [weak self] success in
      guard success else { return }
      //Проверяем показывали правила установки обоев
      self?.logicPresentAfterSavePhoto()
    }
  }
  public func saveVideo(urlString: String?){
    //Загрузка
    self.VM.managers.setup.startLoading()
    guard let urlString = self.VM.server.urls.get(type: .store(urlString)).string else { return }
    PhotoManger.shared.completionSaveVideo = { [weak self] (success) in
      if success {
        //Проверяем показывали правила установки обоев
        self?.logicPresentAfterSavePhoto()
      }
    }
    PhotoManger.shared.saveVideo(urlString: urlString)
  }
  public func choicePresentData(){
    switch self.VM.VC.typePresentHomeData {
      
      //Отоброжаем полученные данные с сервера
      case .server:
        self.VM.homeServerDataModal = .loading
        
      //Отоброжаем полученные данные с дургого контроллера (Detail)
      case .local(let homePresentData):
        self.VM.homeLocalDataModal = .loading(homePresentData)
        
      //Отоброжаем полученные данные с дургого контроллера (Detail) но эти обои живые
      case .localLive(let homePresentData):
        self.VM.homeLocalLiveDataModal = .loading(homePresentData)
    }
  }
  public func choicePresentNativeAdds(){
    
    guard !GDPurchacesActions.shared.isPurchaces else {
      self.VM.VC.bottomAddsView.isHidden      = true
      self.VM.VC.loadingWalpaperView.isHidden = true
      self.VM.VC.centerAddsView.isHidden      = true
      return
    }
    
    switch self.VM.VC.typePresentHomeData {
    
      //Отоброжаем полученные данные с сервера
      case .server:
        self.VM.VC.bottomAddsView.isHidden      = true
        self.VM.VC.centerAddsView.isHidden      = false
        self.VM.VC.loadingWalpaperView.isHidden = true
        
      //Отоброжаем полученные данные с дургого контроллера (Detail)
      case .local:
        self.VM.VC.bottomAddsView.isHidden      = false
        self.VM.VC.centerAddsView.isHidden      = true
        self.VM.VC.loadingWalpaperView.isHidden = true
        
      //Отоброжаем полученные данные с дургого контроллера (Detail) но эти обои живые
      case .localLive:
        self.VM.VC.bottomAddsView.isHidden      = false
        self.VM.VC.centerAddsView.isHidden      = true
        self.VM.VC.loadingWalpaperView.isHidden = true
    }
  }
  public func logicPresentNativeCenterAdds(show: Bool){
    
    guard !GDPurchacesActions.shared.isPurchaces else {
      self.VM.VC.centerAddsView.isHidden      = true
      self.VM.VC.loadingWalpaperView.isHidden = true
      self.VM.VC.bottomAddsView.isHidden      = true
      return
    }
    
    switch self.VM.VC.typePresentHomeData {
      //Отоброжаем полученные данные с сервера
      case .server:
        self.VM.VC.centerAddsView.isHidden      = !show
        self.VM.VC.loadingWalpaperView.isHidden = !show
        self.reloadAdvertising(show: show)
      //Отоброжаем полученные данные с дургого контроллера (Detail)
      case .local:
        print("")
        
      //Отоброжаем полученные данные с дургого контроллера (Detail) но эти обои живые
      case .localLive:
        print("")
    }
   
  }
  public func reloadAdvertising(show: Bool){
    guard show else { return }
    if let nsativeVC = self.VM.VC.children.filter({ ($0 as? NativeAdvertisingViewController) != nil }).first as? NativeAdvertisingViewController {
      nsativeVC.viewModal.managers.setup.adLoaderInit()
    }
  }
  public func logicBackDetail(){
    if !GDPurchacesActions.shared.isPurchaces {
      self.VM.VC.backButton.isEnabled = false
      self.VM.managers.router.push(.InterstitialVC)
      self.VM.returnInterstitialVC = { [weak self] in
        guard let self = self else { return }
        self.VM.VC.dismiss(animated: true)
      }
      DispatchQueue.main.asyncAfter(deadline: .now() + 7) { [weak self] in
        guard let self = self else { return }
        self.VM.VC.backButton.isEnabled = true
        self.VM.VC.dismiss(animated: true)
      }
    } else {
      self.VM.VC.dismiss(animated: true)
    }
  }
  public func share(){
    let controller = UIActivityViewController(activityItems: ["Поделись с друзями"], applicationActivities: nil)
    self.VM.VC.present(controller, animated: true, completion: nil)
  }
}
//MARK: - Initial
extension LogicHome {
  
  //MARK: - Inition
  convenience init(viewModal: HomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}
