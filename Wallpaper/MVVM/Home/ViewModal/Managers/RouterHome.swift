
import UIKit

class RouterHome: VMManager {
  
  //MARK: - Public variable
  public var VM: HomeViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .SaveEndVC:
        self.pushSaveEndVC()
      case .SaveWalpaperVC:
        self.pushSaveWalpaperVC()
      case .InterstitialVC:
        self.pushInterstitialVC()
      case .NativeAdsVC:
        self.pushNativeAddsVC()
      case .NotificationVC:
        self.pushNotificationVC()
    }
  }
  
  enum Push {
    case SaveEndVC
    case SaveWalpaperVC
    case InterstitialVC
    case NativeAdsVC
    case NotificationVC
  }
}
//MARK: - Private functions
extension RouterHome {
  
  private func pushSaveEndVC(){
    let saveEndVC = self.VM.VC.getVCForID(storyboardID     : .SaveEnd,
                                          vcID             : .SaveEndVC,
                                          transitionStyle  : .crossDissolve,
                                          presentationStyle: .fullScreen) as! SaveEndViewController
    self.VM.VC.present(saveEndVC, animated: true)
  }
  private func pushSaveWalpaperVC(){
    let saveWalpaperVC = self.VM.VC.getVCForID(storyboardID     : .SaveWalpaper,
                                               vcID             : .SaveWalpaperVC,
                                               transitionStyle  : .crossDissolve,
                                               presentationStyle: .fullScreen) as! SaveWalpaperViewController
    self.VM.VC.present(saveWalpaperVC, animated: true)
  }
  private func pushInterstitialVC(){
    let interstitialVC = self.VM.VC.getVCForID(storyboardID     : .Interstitial,
                                               vcID             : .InterstitialVC,
                                               transitionStyle  : .crossDissolve,
                                               presentationStyle: .fullScreen) as! InterstitialViewController
    interstitialVC.modalPresentationStyle  = .overCurrentContext
    interstitialVC.modalTransitionStyle    = .flipHorizontal
    interstitialVC.view.backgroundColor    = .clear
    interstitialVC.typeInterstitialPresent = .Save
    interstitialVC.dissAppearInterstitial = {
      self.VM.returnInterstitialVC?()
    }
    self.VM.VC.present(interstitialVC, animated: true)
  }
  private func pushNotificationVC(){
    let notificationVC = self.VM.VC.getVCForID(storyboardID     : .Notification,
                                               vcID             : .NotificationVC,
                                               transitionStyle  : .crossDissolve,
                                               presentationStyle: .fullScreen) as! NotificationViewController
    self.VM.VC.present(notificationVC, animated: true)
  }
  private func pushNativeAddsVC(){
    let nativeAdsVC = self.VM.VC.getVCForID(storyboardID     : .NativeAdvertising,
                                            vcID             : .NativeAdsVC,
                                            transitionStyle  : .crossDissolve,
                                            presentationStyle: .fullScreen) as! NativeAdvertisingViewController
    nativeAdsVC.presentationType = .HomeBottomView
    nativeAdsVC.modalPresentationStyle = .overCurrentContext
    nativeAdsVC.modalTransitionStyle   = .crossDissolve
    nativeAdsVC.view.backgroundColor   = .clear
    self.VM.VC.present(nativeAdsVC, animated: true)
  }
}
//MARK: - Initial
extension RouterHome {
  
  //MARK: - Inition
  convenience init(viewModal: HomeViewModal) {
    self.init()
    self.VM = viewModal
  }
}



