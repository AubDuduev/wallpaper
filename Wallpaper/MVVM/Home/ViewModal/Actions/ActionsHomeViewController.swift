
import UIKit

extension HomeViewController {
  
  @IBAction func backButton(button: UIButton){
    self.viewModal.managers.logic.logicBackDetail()
  }
  @IBAction func shareButton(button: UIButton){
    self.viewModal.managers.logic.share()
  }
}
