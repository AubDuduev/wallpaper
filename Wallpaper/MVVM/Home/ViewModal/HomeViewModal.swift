
import NVActivityIndicatorView
import Foundation

class HomeViewModal: VMManagers {
	
	public var homeServerDataModal: HomeServerDataModal = .loading {
		didSet{
			self.commonServerDataLogic()
		}
	}
  public var homeLocalDataModal: HomeLocalDataModal! {
    didSet{
      self.commonLocalDataLogic()
    }
  }
  public var homeLocalLiveDataModal: HomeLocalLiveDataModal! {
    didSet{
      self.commonLocalLiveDataLogic()
    }
  }
  //MARK: - Public variable
	public var loading              : NVActivityIndicatorView!
  public var managers             : HomeManagers!
  public var VC                   : HomeViewController!
	public let server               = Server()
	public let errorHandlerHomeData = ErrorHandlerHomeData()
	public let coreData             = CoreData()
  public var previewIsState       = false
  public var returnInterstitialVC : ClousureEmpty!
  public var returnHomeVC         : ClousureEmpty!
  public var dissmissAdds         : ClousureEmpty!
  public let notification         = GDSetupNotification()
  
  private var homePresentData: HomePresentData!
  
	public func viewDidLoad() {
    self.managers.setup.navBar()
		self.managers.setup.homeCollection()
    self.managers.logic.choicePresentNativeAdds()
    self.managers.setup.startLoading()
    self.managers.setup.loadingWalpaperView()
	}
  public func viewDidAppear() {
    self.managers.logic.choicePresentData()
    self.managers.logic.testNotification()
  }
	public func commonServerDataLogic(){
		
    switch self.homeServerDataModal {
			//1 - Загрузка
			case .loading:
				self.homeServerDataModal = .getData
				
			//2 - Получение данных
			case .getData:
				//Делаем запрос на сервер для получения данных
				self.managers.server.getHomeData { [weak self] (previewData) in
					self?.homeServerDataModal = .errorHandler(previewData)
				}
				
			//3 - Обрабка ошибок запроса
			case .errorHandler(let previewData):
				//Обрабатываем на ошибки полученные данные
				guard self.errorHandlerHomeData.check(previewData: previewData) else { return }
				var homePresentData = HomePresentData(previewData: previewData!)
        homePresentData.previewData.data.previews = homePresentData.previewData.data.previews.shuffled()
				self.homeServerDataModal = .presentData(homePresentData)
				
			//4 - Отоброжение данных на kонтроллере
			case .presentData(let homePresentData):
				self.managers.present.collection(homePresentData: homePresentData)
        
		}
	}
  public func commonLocalLiveDataLogic(){
    
    switch self.homeLocalLiveDataModal! {
      
      //1 - Получаем данные
      case .loading(let homePresentData):
        self.homeLocalLiveDataModal = .errorHandler(homePresentData)
        
      //2 - Проверяем на ошибки
      case .errorHandler(let homePresentData):
        guard let homePresentData = homePresentData else { return }
        guard homePresentData.previews != nil else { return }
        self.homeLocalLiveDataModal = .presentData(homePresentData)
      
      //3 - Презентуем данные
      case .presentData(let homePresentData):
        //презентуем
        self.managers?.present?.collection(homePresentData: homePresentData)
        self.homePresentData = homePresentData
        self.loading.stopAnimating()
        
    }
  }
  public func commonLocalDataLogic(){
    
    switch self.homeLocalDataModal! {
    
      //1 - Получаем данные
      case .loading(let homePresentData):
        self.homeLocalDataModal = .errorHandler(homePresentData)
      //2 - Проверяем на ошибки
      case .errorHandler(let homePresentData):
        guard let homePresentData = homePresentData else { return }
        guard homePresentData.previews != nil else { return }
        self.homeLocalDataModal = .presentData(homePresentData)
      
      //3 - Презентуем данные
      case .presentData(let homePresentData):
        //презентуем
        self.managers?.present?.collection(homePresentData: homePresentData)
        self.homePresentData = homePresentData
        self.loading.stopAnimating()
    }
  }
}
//MARK: - Initial
extension HomeViewModal {
  
  convenience init(viewController: HomeViewController) {
    self.init()
    self.VC       = viewController
    self.managers = HomeManagers(viewModal: self)
  }
}
