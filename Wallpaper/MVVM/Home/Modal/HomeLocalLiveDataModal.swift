
import UIKit

enum HomeLocalLiveDataModal {
	
  case loading(HomePresentData?)
  case errorHandler(HomePresentData?)
  case presentData(HomePresentData)
	
}

