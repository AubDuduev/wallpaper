
import Foundation

struct HomePresentData {
  
  var previewData : DECPreviewData!
  var mainPreview : DECMainPreview!
  var previews    : [DECPreview]!
  let indexPath   : IndexPath!
  
  init(previewData: DECPreviewData? = nil, previews: [DECPreview]? = nil, indexPath: IndexPath? = nil) {
    
    self.previewData = previewData
    self.mainPreview = previewData?.data
    self.indexPath   = indexPath 
    self.previews    = previews
  }
}

enum TypePresentHomeData {
  
  case server
  case local(HomePresentData?)
  case localLive(HomePresentData?)
}
