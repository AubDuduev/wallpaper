
import Foundation

class HomeManagers: VMManagers {
  
  let setup    : SetupHome!
  let server   : ServerHome!
  let present  : PresentHome!
  let logic    : LogicHome!
  let animation: AnimationHome!
  let router   : RouterHome!
  
  init(viewModal: HomeViewModal) {
    
    self.setup     = SetupHome(viewModal: viewModal)
    self.server    = ServerHome(viewModal: viewModal)
    self.present   = PresentHome(viewModal: viewModal)
    self.logic     = LogicHome(viewModal: viewModal)
    self.animation = AnimationHome(viewModal: viewModal)
    self.router    = RouterHome(viewModal: viewModal)
  }
}

