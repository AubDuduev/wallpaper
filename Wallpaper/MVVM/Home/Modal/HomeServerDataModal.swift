
import Foundation

enum HomeServerDataModal {
	
	case loading
	case getData
	case errorHandler(DECPreviewData?)
	case presentData(HomePresentData)
	
}


