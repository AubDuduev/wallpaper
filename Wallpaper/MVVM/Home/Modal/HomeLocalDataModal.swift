
import Foundation

enum HomeLocalDataModal {
  
  case loading(HomePresentData?)
  case errorHandler(HomePresentData?)
  case presentData(HomePresentData)

}
