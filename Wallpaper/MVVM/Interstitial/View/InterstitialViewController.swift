import UIKit

class InterstitialViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: InterstitialViewModal!
  
  //MARK: - Public variable
  public var typeInterstitialPresent         : TypeInterstitialPresent = .Save
  public var interstitialDidReceiveAd        :  ClousureEmpty!
  public var interstitialWillPresentScreen   :  ClousureEmpty!
  public var interstitialWillDismissScreen   :  ClousureEmpty!
  public var interstitialDidDismissScreen    :  ClousureEmpty!
  public var interstitialWillLeaveApplication:  ClousureEmpty!
  public var dissAppearInterstitial          :  ClousureEmpty!
  //MARK: - Outlets
  
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
  private func initViewModal(){
    self.viewModal = InterstitialViewModal(viewController: self)
  }
}
