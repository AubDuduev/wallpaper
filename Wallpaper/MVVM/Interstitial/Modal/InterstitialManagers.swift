
import Foundation

class InterstitialManagers: VMManagers {
  
  let setup    : SetupInterstitial!
  let server   : ServerInterstitial!
  let present  : PresentInterstitial!
  let logic    : LogicInterstitial!
  let animation: AnimationInterstitial!
  let router   : RouterInterstitial!
  
  init(viewModal: InterstitialViewModal) {
    
    self.setup     = SetupInterstitial(viewModal: viewModal)
    self.server    = ServerInterstitial(viewModal: viewModal)
    self.present   = PresentInterstitial(viewModal: viewModal)
    self.logic     = LogicInterstitial(viewModal: viewModal)
    self.animation = AnimationInterstitial(viewModal: viewModal)
    self.router    = RouterInterstitial(viewModal: viewModal)
  }
}

