
import UIKit

enum InterstitialModal {
	
	case loading
	case getData
	case errorHandler
	case presentData(Data)
	
	struct Data {
		
		
	}
}

enum TypeInterstitialPresent {
  case Save
  case Welcome
}
