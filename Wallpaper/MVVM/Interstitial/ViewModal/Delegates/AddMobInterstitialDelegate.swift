
import UIKit
import GoogleMobileAds

extension InterstitialViewController: GADVideoControllerDelegate {

	func videoControllerDidEndVideoPlayback(_ videoController: GADVideoController) {
	 
	}
}

extension InterstitialViewController: GADAdLoaderDelegate {

	func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
		print("\(adLoader) failed with error: \(error.localizedDescription)")
	}
}

extension InterstitialViewController: GADUnifiedNativeAdLoaderDelegate {

	func adLoader(_ adLoader: GADAdLoader, didReceive nativeAd: GADUnifiedNativeAd) {
		// Set ourselves as the native ad delegate to be notified of native ad events.
		nativeAd.delegate = self
		// Note: this should always be done after populating the ad views.

	}
}

// MARK: - GADUnifiedNativeAdDelegate implementation
extension InterstitialViewController: GADUnifiedNativeAdDelegate {

	func nativeAdDidRecordClick(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdDidRecordImpression(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdWillPresentScreen(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdWillDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdDidDismissScreen(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}

	func nativeAdWillLeaveApplication(_ nativeAd: GADUnifiedNativeAd) {
		print("\(#function) called")
	}
}
extension InterstitialViewController: GADInterstitialDelegate {
  
  func interstitialDidReceiveAd(_ ad: GADInterstitial) {
    print("interstitialDidReceiveAd")
    self.interstitialDidReceiveAd?()
  }
  
  /// Tells the delegate an ad request failed.
  func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
    print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    self.viewModal.managers.logic.dissmiss()
  }
  
  /// Tells the delegate that an interstitial will be presented.
  func interstitialWillPresentScreen(_ ad: GADInterstitial) {
    print("interstitialWillPresentScreen")
    self.interstitialWillPresentScreen?()
  }
  
  /// Tells the delegate the interstitial is to be animated off the screen.
  func interstitialWillDismissScreen(_ ad: GADInterstitial) {
    self.viewModal.managers.logic.dissmiss()
    print("interstitialWillDismissScreen")
  }
  
  /// Tells the delegate the interstitial had been animated off the screen.
  func interstitialDidDismissScreen(_ ad: GADInterstitial) {
    self.viewModal.managers.logic.dissmiss()
    print("interstitialDidDismissScreen")
  }
  
  /// Tells the delegate that a user click will open another app
  /// (such as the App Store), backgrounding the current app.
  func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
    self.viewModal.managers.logic.dissmiss()
    print("interstitialWillLeaveApplication")
  }
}
