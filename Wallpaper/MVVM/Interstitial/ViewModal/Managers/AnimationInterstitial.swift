import UIKit

class AnimationInterstitial: VMManager {
  
  //MARK: - Public variable
  public var VM: InterstitialViewModal!
  
  
}
//MARK: - Initial
extension AnimationInterstitial {
  
  //MARK: - Inition
  convenience init(viewModal: InterstitialViewModal) {
    self.init()
    self.VM = viewModal
  }
}

