
import UIKit

class SetupInterstitial: VMManager {
  
  //MARK: - Public variable
  public var VM: InterstitialViewModal!
  
  public func setupInterstitial(){
    GDAddMob.shared.setupInterstitial(viewController: self.VM.VC)
  }
}
//MARK: - Initial
extension SetupInterstitial {
  
  //MARK: - Inition
  convenience init(viewModal: InterstitialViewModal) {
    self.init()
    self.VM = viewModal
  }
}


