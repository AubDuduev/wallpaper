import UIKit

class ServerInterstitial: VMManager {
  
  //MARK: - Public variable
  public var VM: InterstitialViewModal!
  
  
}
//MARK: - Initial
extension ServerInterstitial {
  
  //MARK: - Inition
  convenience init(viewModal: InterstitialViewModal) {
    self.init()
    self.VM = viewModal
  }
}


