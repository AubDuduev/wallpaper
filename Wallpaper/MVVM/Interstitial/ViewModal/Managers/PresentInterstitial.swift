
import UIKit

class PresentInterstitial: VMManager {
  
  //MARK: - Public variable
  public var VM: InterstitialViewModal!
  
  
}
//MARK: - Initial
extension PresentInterstitial {
  
  //MARK: - Inition
  convenience init(viewModal: InterstitialViewModal) {
    self.init()
    self.VM = viewModal
  }
}

