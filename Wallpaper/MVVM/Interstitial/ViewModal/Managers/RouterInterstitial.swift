
import UIKit

class RouterInterstitial: VMManager {
  
  //MARK: - Public variable
  public var VM: InterstitialViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .NavBarVC:
        self.pushNavBarVC()
    }
  }
  
  enum Push {
    case NavBarVC
  }
}
//MARK: - Private functions
extension RouterInterstitial {
  
  private func pushNavBarVC(){
    let navBarVC = self.VM.VC.getVCForID(storyboardID     : .NavBar,
                                         vcID             : .NavBarVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! NavBarViewController
    self.VM.VC.present(navBarVC, animated: true)
  }
}
//MARK: - Initial
extension RouterInterstitial {
  
  //MARK: - Inition
  convenience init(viewModal: InterstitialViewModal) {
    self.init()
    self.VM = viewModal
  }
}



