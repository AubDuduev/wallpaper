import Foundation

class LogicInterstitial: VMManager {
  
  //MARK: - Public variable
  public var VM: InterstitialViewModal!
  
  public func typeInterstitialPresent(){
    
  }
  
  public func dissmiss(){
    switch self.VM.VC.typeInterstitialPresent {
      case .Save:
        self.VM.VC.dismiss(animated: true){ [weak self] in
          guard let self = self else { return }
          self.VM.VC.dissAppearInterstitial?()
        }
      case .Welcome:
        self.VM.managers.router.push(.NavBarVC)
    }
  }
}
//MARK: - Initial
extension LogicInterstitial {
  
  //MARK: - Inition
  convenience init(viewModal: InterstitialViewModal) {
    self.init()
    self.VM = viewModal
  }
}
