
import Foundation

class InterstitialViewModal: VMManagers {
	
	public var interstitialModal: InterstitialModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers: InterstitialManagers!
  public var VC      : InterstitialViewController!
  
  public func viewDidLoad() {
    self.managers.setup.setupInterstitial()
  }
  public func viewDidAppear() {
    GDAddMob.shared.presentInterstitial(viewController: self.VC)
  }
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension InterstitialViewModal {
  
  convenience init(viewController: InterstitialViewController) {
    self.init()
    self.VC       = viewController
    self.managers = InterstitialManagers(viewModal: self)
  }
}
