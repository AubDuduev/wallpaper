
import Foundation

class SaveEndViewModal: VMManagers {
	
	public var saveEndModal: SaveEndModal = .loading {
		didSet{
			self.commonLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers: SaveEndManagers!
  public var VC      : SaveEndViewController!
  
  public func viewDidLoad() {
    self.managers.setup.exitButton()
  }
  public func commonLogic(){
    
    //1 - Получаем данные
    
    //2 - Проверяем на ошибки
    
    //3 - Презентуем данные
  }
}
//MARK: - Initial
extension SaveEndViewModal {
  
  convenience init(viewController: SaveEndViewController) {
    self.init()
    self.VC       = viewController
    self.managers = SaveEndManagers(viewModal: self)
  }
}
