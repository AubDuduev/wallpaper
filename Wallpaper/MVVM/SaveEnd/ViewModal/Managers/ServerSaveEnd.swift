import UIKit

class ServerSaveEnd: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveEndViewModal!
  
  
}
//MARK: - Initial
extension ServerSaveEnd {
  
  //MARK: - Inition
  convenience init(viewModal: SaveEndViewModal) {
    self.init()
    self.VM = viewModal
  }
}


