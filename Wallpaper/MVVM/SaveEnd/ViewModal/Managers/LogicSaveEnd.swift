import Foundation
import UIKit

class LogicSaveEnd: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveEndViewModal!
  
  public func clouseImageAdds(){
    if !GDPurchacesActions.shared.isPurchaces {
      UIView.animate(withDuration: 1) { [weak self] in
        guard let self = self else { return }
        self.VM.VC.imageAddsView.alpha = 0
        self.VM.VC.exitButton.alpha    = 1
      }
    } else {
      self.dismiss()
    }
  }
  
  public func dismiss(){
   
    if !StoreProject.shared.getBool(key: .isRateUs) {
      self.VM.managers.router.push(.RateUsVC)
      StoreProject.shared.saveBool(key: .isRateUs, value: true)
    } else {
      self.VM.managers.router.push(.NavBarVC)
    }
  }
}
//MARK: - Initial
extension LogicSaveEnd {
  
  //MARK: - Inition
  convenience init(viewModal: SaveEndViewModal) {
    self.init()
    self.VM = viewModal
  }
}
