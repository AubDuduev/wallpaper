
import UIKit

class SetupSaveEnd: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveEndViewModal!
  
  public func exitButton(){
    self.VM.VC.exitButton.alpha = 0
  }
}
//MARK: - Initial
extension SetupSaveEnd {
  
  //MARK: - Inition
  convenience init(viewModal: SaveEndViewModal) {
    self.init()
    self.VM = viewModal
  }
}


