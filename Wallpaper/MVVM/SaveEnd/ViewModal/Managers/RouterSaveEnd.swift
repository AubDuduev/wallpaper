
import UIKit

class RouterSaveEnd: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveEndViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .RateUsVC:
        self.pushRateUs()
      case .NavBarVC:
        self.pushNavBarVC()
    }
  }
  
  enum Push {
    case RateUsVC
    case NavBarVC
  }
}
//MARK: - Private functions
extension RouterSaveEnd {
  
  private func pushRateUs(){
    let rateUSVC = self.VM.VC.getVCForID(storyboardID     : .RateUs,
                                         vcID             : .RateUsVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! RateUsViewController
    self.VM.VC.present(rateUSVC, animated: true)
  }
  private func pushNavBarVC(){
    let navBarVC = self.VM.VC.getVCForID(storyboardID     : .NavBar,
                                         vcID             : .NavBarVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! NavBarViewController
    self.VM.VC.present(navBarVC, animated: true)
  }
}
//MARK: - Initial
extension RouterSaveEnd {
  
  //MARK: - Inition
  convenience init(viewModal: SaveEndViewModal) {
    self.init()
    self.VM = viewModal
  }
}



