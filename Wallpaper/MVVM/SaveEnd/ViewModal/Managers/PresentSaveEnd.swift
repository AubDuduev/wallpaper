
import UIKit

class PresentSaveEnd: VMManager {
  
  //MARK: - Public variable
  public var VM: SaveEndViewModal!
  
  
}
//MARK: - Initial
extension PresentSaveEnd {
  
  //MARK: - Inition
  convenience init(viewModal: SaveEndViewModal) {
    self.init()
    self.VM = viewModal
  }
}

