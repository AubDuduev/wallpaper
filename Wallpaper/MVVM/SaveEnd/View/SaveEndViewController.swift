import UIKit

class SaveEndViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: SaveEndViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
  @IBOutlet weak var imageAddsView : UIView!
  @IBOutlet weak var nativeAddsView: UIView!
  @IBOutlet weak var exitButton    : UIButton!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  private func initViewModal(){
    self.viewModal = SaveEndViewModal(viewController: self)
  }
}
