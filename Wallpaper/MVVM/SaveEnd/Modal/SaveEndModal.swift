
import UIKit

enum SaveEndModal {
	
	case loading
	case getData
	case errorHandler
	case presentData(Data)
	
	
}

