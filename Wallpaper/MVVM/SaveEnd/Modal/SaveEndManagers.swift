
import Foundation

class SaveEndManagers: VMManagers {
  
  let setup    : SetupSaveEnd!
  let server   : ServerSaveEnd!
  let present  : PresentSaveEnd!
  let logic    : LogicSaveEnd!
  let animation: AnimationSaveEnd!
  let router   : RouterSaveEnd!
  
  init(viewModal: SaveEndViewModal) {
    
    self.setup     = SetupSaveEnd(viewModal: viewModal)
    self.server    = ServerSaveEnd(viewModal: viewModal)
    self.present   = PresentSaveEnd(viewModal: viewModal)
    self.logic     = LogicSaveEnd(viewModal: viewModal)
    self.animation = AnimationSaveEnd(viewModal: viewModal)
    self.router    = RouterSaveEnd(viewModal: viewModal)
  }
}

