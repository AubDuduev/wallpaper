import UIKit

class PurchacesViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: PurchacesViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
 
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  private func initViewModal(){
    self.viewModal = PurchacesViewModal(viewController: self)
  }
}
