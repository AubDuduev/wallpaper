
import Foundation
import Protocols

class PurchacesManagers: VMManagers {
  
  let setup    : SetupPurchaces!
  let server   : ServerPurchaces!
  let present  : PresentPurchaces!
  let logic    : LogicPurchaces!
  let animation: AnimationPurchaces!
  let router   : RouterPurchaces!
  
  init(viewModal: PurchacesViewModal) {
    
    self.setup     = SetupPurchaces(viewModal: viewModal)
    self.server    = ServerPurchaces(viewModal: viewModal)
    self.present   = PresentPurchaces(viewModal: viewModal)
    self.logic     = LogicPurchaces(viewModal: viewModal)
    self.animation = AnimationPurchaces(viewModal: viewModal)
    self.router    = RouterPurchaces(viewModal: viewModal)
  }
}

