import UIKit
import Protocols

class LogicPurchaces: VMManager {
  
  //MARK: - Public variable
  public var VM: PurchacesViewModal!
  
  public func purchacesActions(){
    GDPurchacesActions.shared.actions()
  }
  
  public func logicPurchaces(button: UIButton){
    let id = GDPurchacesProductID.allCases[button.tag]
    GDPurchacesActions.shared.pay(id: id)
  }
  public func restored(){
    GDPurchacesActions.shared.restored()
  }
}
//MARK: - Initial
extension LogicPurchaces {
  
  //MARK: - Inition
  convenience init(viewModal: PurchacesViewModal) {
    self.init()
    self.VM = viewModal
  }
}
