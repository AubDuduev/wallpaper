
import Foundation
import Protocols

class PurchacesViewModal: VMManagers {
	
	public var purchacesModal: PurchacesModal = .loading {
		didSet{
			self.purchacesModalLogic()
		}
	}
  
  //MARK: - Public variable
  public var managers: PurchacesManagers!
  public var VC      : PurchacesViewController!
  public let server  = Server()
  
  public func viewDidLoad() {
    self.managers.logic.purchacesActions()
  }
  
  public func purchacesModalLogic(){
    
    switch self.purchacesModal {
      //1 - Загрузка
      case .loading:
        print("")
      //2 - Получаем данные
      case .getData:
        print("")
      //3 - Проверяем на ошибки
      case .errorHandler:
        print("")
      //4 - Презентуем данные
      case .presentData(_):
        print("")
    }
  }
}
//MARK: - Initial
extension PurchacesViewModal {
  
  convenience init(viewController: PurchacesViewController) {
    self.init()
    self.VC       = viewController
    self.managers = PurchacesManagers(viewModal: self)
  }
}
