
import UIKit

extension PurchacesViewController {
  
  @IBAction func backButton(button: UIButton){
    self.dismiss(animated: true)
  }
  @IBAction func payButton(button: UIButton){
    self.viewModal.managers.logic.logicPurchaces(button: button)
  }
  @IBAction func restoredButton(button: UIButton){
    self.viewModal.managers.logic.restored()
  }
  @IBAction func continueButton(button: UIButton){
    self.viewModal.managers.logic.logicPurchaces(button: button)
  }
}
