
import UIKit
import NVActivityIndicatorView

class SetupDetail: VMManager {
  
  //MARK: - Public variable
  public var VM: DetailViewModal!
  
	public func startLoading(){
		let rect = CGRect(x: 0, y: 0, width: 250, height: 250)
		self.VM.loading = NVActivityIndicatorView(frame  : rect,
																							type   : .ballBeat,
																							color  : .black,
																							padding: 10)
		self.VM.loading.center = self.VM.VC.view.center
		self.VM.VC.view.addSubview(self.VM.loading)
		self.VM.loading.startAnimating()
	}
	public func detailCollection(){
		let collectionViewLayout = DetailCollectionViewLaytout()
		collectionViewLayout.sectionInset = .init(top: 0, left: 5, bottom: 0, right: 5)
		collectionViewLayout.sectionInsetReference   = .fromContentInset
		collectionViewLayout.minimumLineSpacing      = 5
		collectionViewLayout.minimumInteritemSpacing = 5
		self.VM.VC.detailCollectionView.collectionViewLayout = collectionViewLayout
		self.VM.VC.detailCollection.collectionView           = self.VM.VC.detailCollectionView
		self.VM.VC.detailCollection.viewModal                = self.VM
	}
	
	//MARK: - Public DetailCollectionCell
	public func image(cell: DetailCollectionCell){
		cell.previewImageView.cornerRadius(20, true)
    cell.cornerRadius(20, true)
		cell.commonView.cornerRadius(20, true)
	}
	
}
//MARK: - Initial
extension SetupDetail {
  
  //MARK: - Inition
  convenience init(viewModal: DetailViewModal) {
    self.init()
    self.VM = viewModal
  }
}


