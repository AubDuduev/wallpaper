import UIKit

class ServerDetail: VMManager {
  
  //MARK: - Public variable
  public var VM: DetailViewModal!
  
	public func getDetail(endPath: String?, completion: @escaping Clousure<DECPreviewData?>){
		//Request
		self.VM.server.request(requestType: GETCategoryDetail(), data: endPath) { [weak self]  (serverResult) in
			guard let self = self else { return }
			//Response
			switch serverResult {
				case .error(let error):
					guard let error = error else { return }
					print("Error server data: class: ServerDetail ->, function: getDetail -> data: DECCategoryDetail ->, description: ", error.localizedDescription)
					AlertEK.dеfault(title: .error, message: .noJSON){
						self.getDetail(endPath: endPath, completion: completion)
					}
				//Succes
				case .object(let object):
					let categoryDetail = object as? DECPreviewData
					completion(categoryDetail)
					print("Succesful data: class: ServerDetail ->, function: getDetail ->, data: DECCategoryDetail")
					
			}
		}
	}
}
//MARK: - Initial
extension ServerDetail {
  
  //MARK: - Inition
  convenience init(viewModal: DetailViewModal) {
    self.init()
    self.VM = viewModal
  }
}


