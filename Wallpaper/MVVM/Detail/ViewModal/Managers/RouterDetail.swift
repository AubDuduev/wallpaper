
import UIKit

class RouterDetail: VMManager {
  
  //MARK: - Public variable
  public var VM: DetailViewModal!
  
  public func push(_ type: Push){
    
    switch type {
      case .HomeVC(let typePresentHomeData):
        self.pushHomeVC(typePresentHomeData: typePresentHomeData)
      case .InterstitialVC:
        self.pushInterstitialVC()
    }
  }
  
  enum Push {
    case HomeVC(TypePresentHomeData)
    case InterstitialVC
  }
}
//MARK: - Private functions
extension RouterDetail {
  
  private func pushHomeVC(typePresentHomeData: TypePresentHomeData){
    let homeVC = self.VM.VC.getVCForID(storyboardID     : .Home,
                                       vcID             : .HomeVC,
                                       transitionStyle  : .crossDissolve,
                                       presentationStyle: .fullScreen) as! HomeViewController
    homeVC.typePresentHomeData = typePresentHomeData
    self.VM.VC.present(homeVC, animated: true)
  }
  private func pushInterstitialVC(){
    let interstitialVC = self.VM.VC.getVCForID(storyboardID     : .Interstitial,
                                               vcID             : .InterstitialVC,
                                               transitionStyle  : .crossDissolve,
                                               presentationStyle: .fullScreen) as! InterstitialViewController
    interstitialVC.modalPresentationStyle = .overCurrentContext
    interstitialVC.modalTransitionStyle   = .flipHorizontal
    interstitialVC.view.backgroundColor   = .clear
    interstitialVC.dissAppearInterstitial = {
     
    }
    self.VM.VC.present(interstitialVC, animated: true)
  }
}
//MARK: - Initial
extension RouterDetail {
  
  //MARK: - Inition
  convenience init(viewModal: DetailViewModal) {
    self.init()
    self.VM = viewModal
  }
}



