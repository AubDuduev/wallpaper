
import Foundation
import UIKit

class LogicDetail: VMManager {
  
  //MARK: - Public variable
  public var VM: DetailViewModal!
  
	public func choosePresentData(){
		switch self.VM.VC.detailGetData.dataType! {
			case .local:
				self.VM.presentLocalDataModal = .loading
			case .server:
				self.VM.presentServerDataModal = .loading
		}
	}
  public func pushHomeVC(previewData: DECPreviewData, indexPath: IndexPath){
    let homePresentData = HomePresentData(previews : previewData.data.previews,
                                          indexPath: indexPath)
    var typePresentHomeData: TypePresentHomeData!
    
    switch self.VM.VC.detailGetData.detailTitle {
      case .live:
        typePresentHomeData = TypePresentHomeData.localLive(homePresentData)
      default:
        typePresentHomeData = TypePresentHomeData.local(homePresentData)
    }
    
    self.VM.managers.router.push(.HomeVC(typePresentHomeData))
  }
  public func indexPathAdvertising(presentData: DetailPresentData){
    let countAdvertising = (presentData.previews.count / 6)
    var indexPathRow = 6
    for _ in 0...countAdvertising {
      self.VM.indexPathAdvertising.append(indexPathRow)
      indexPathRow += 7
    }
  }
  public func prepareForReuse(image: UIImageView){
    image.isSkeletonable = true
    image.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
  }
}
//MARK: - Initial
extension LogicDetail {
  
  //MARK: - Inition
  convenience init(viewModal: DetailViewModal) {
    self.init()
    self.VM = viewModal
  }
}
