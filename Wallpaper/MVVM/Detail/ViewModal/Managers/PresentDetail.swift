
import UIKit

class PresentDetail: VMManager {
  
  //MARK: - Public variable
  public var VM: DetailViewModal!
  
  
  //MARK: - DetailViewController
  public func titleNavBarLabel(){
    self.VM.VC.titleNavBarLabel.text = self.VM.VC.detailGetData.title
  }
  
  //MARK: - DetailCollectionCell
  public func image(cell: DetailCollectionCell){
    guard let image = cell.preview?.previewURL else { return }
    guard let url   = self.VM.server.urls.get(type: .store(image)).URL else { return }
    cell.previewImageView.isSkeletonable = true
    cell.previewImageView.showAnimatedGradientSkeleton(usingGradient: .init(baseColor: #colorLiteral(red: 0.05793874711, green: 0.05753285438, blue: 0.05825470388, alpha: 1)))
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
      cell.previewImageView.sd_setImage(with: url) { (image, error, _, url) in
        
        if let error = error {
          
          print("Error load image , Class: ", "Localized: \(error.localizedDescription)")
        } else {
          //cell.previewImageView.image = image!.sd_flippedImage(withHorizontal: true, vertical: true)
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            cell.previewImageView.hideSkeleton()
          }
        }
      }
    }
  }
  
  public func collection(presentData: DetailPresentData){
    self.VM.VC.detailCollection.presentData = presentData
    self.VM.VC.detailCollectionView.reloadData()
  }
  
}
//MARK: - Initial
extension PresentDetail {
  
  //MARK: - Inition
  convenience init(viewModal: DetailViewModal) {
    self.init()
    self.VM = viewModal
  }
}

