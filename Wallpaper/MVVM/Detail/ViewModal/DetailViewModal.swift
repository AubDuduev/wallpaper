
import Foundation
import NVActivityIndicatorView

class DetailViewModal: VMManagers {
	
	public var presentServerDataModal: PresentServerDataModal = .loading {
		didSet{
			self.presentServerData()
		}
	}
	public var presentLocalDataModal: PresentLocalDataModal = .loading {
		didSet{
			self.presentLocalData()
		}
	}
  //MARK: - Public variable
	public var loading       : NVActivityIndicatorView!
  public var managers      : DetailManagers!
  public var VC            : DetailViewController!
  public let server        = Server()
	public let errorHandler  = ErrorHandlerDetailCategory()
	public let coreData      = CoreData()
  public var indexPathAdvertising = [Int]()
	
	public func viewDidLoad() {
		self.managers.setup.detailCollection()
		self.managers.present.titleNavBarLabel()
		self.managers.logic.choosePresentData()
	}
  
	public func presentServerData(){
		
		switch self.presentServerDataModal {
			//1 - Загрузка
			case .loading:
				self.managers.setup.startLoading()
				self.presentServerDataModal = .getData(self.VC.detailGetData.wallpaper.link)
				
			//2 - Получение данных
			case .getData(let endPath):
				//Делаем запрос на сервер для получения данных
				self.managers.server.getDetail(endPath: endPath){ [weak self] (mainPreview) in
					self?.presentServerDataModal = .errorHandler(mainPreview)
				}
				
			//3 - Обрабка ошибок запроса
			case .errorHandler(let previewData):
				//Обрабатываем на ошибки полученные данные
				guard self.errorHandler.check(mainPreview: previewData) else { return }
        let presentData = DetailPresentData(previewData: previewData, previews: nil)
				self.presentServerDataModal = .presentData(presentData)
				
			//4 - Отоброжение данных на kонтроллере
			case .presentData(let presentData):
        self.managers.logic.indexPathAdvertising(presentData: presentData)
        self.managers.present.collection(presentData: presentData)
				self.loading.stopAnimating()
        
		}
	}
	public func presentLocalData(){
		
		switch self.presentLocalDataModal {
			//1 - Загрузка
			case .loading:
				self.managers.setup.startLoading()
				self.presentLocalDataModal = .getData
				
			//2 - Получение данных
			case .getData:
				//Делаем запрос на сервер для получения данных
				self.coreData.fetchArray(object: CDPreview.self) { (previews) in
					let previews = previews.filter{ $0.previewURL != nil }
					self.presentLocalDataModal = .errorHandler(previews)
				}
				
			//3 - Обрабка ошибок запроса
			case .errorHandler(let previews):
				//Обрабатываем на ошибки полученные данные
				guard previews != nil else { return }
				let presentData = DetailPresentData(previewData: nil, previews: previews!)
				self.presentLocalDataModal = .presentData(presentData)
				
			//4 - Отоброжение данных на kонтроллере
			case .presentData(let presentData):
        self.managers.logic.indexPathAdvertising(presentData: presentData)
        self.managers.present.collection(presentData: presentData)
				self.loading.stopAnimating()
		}
	}
}
//MARK: - Initial
extension DetailViewModal {
  
  convenience init(viewController: DetailViewController) {
    self.init()
    self.VC       = viewController
    self.managers = DetailManagers(viewModal: self)
  }
}
