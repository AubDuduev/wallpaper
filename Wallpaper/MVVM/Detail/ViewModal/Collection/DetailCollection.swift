
import UIKit

class DetailCollection : NSObject {
	
	//MARK: - Variable
	public var collectionView: UICollectionView!
	public var viewModal     : DetailViewModal!
  public var presentData   : DetailPresentData!
}

//MARK: - Delegate CollectionView
extension DetailCollection: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let previewData = self.presentData.previewData else { return }
    self.viewModal.managers.logic.pushHomeVC(previewData: previewData, indexPath: indexPath)
	}
}

//MARK: - DataSource CollectionView
extension DetailCollection: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		self.collectionView = collectionView
    return self.presentData?.previews?.count ?? 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    switch true {
      case self.viewModal.indexPathAdvertising.contains(indexPath.row) && !GDPurchacesActions.shared.isPurchaces:
        let cell = NativeAddCollectionCell().collectionCell(collectionView, indexPath: indexPath)
        cell.configure(viewController: self.viewModal.VC)
        return cell
      default:
        let cell = DetailCollectionCell().collectionCell(collectionView, indexPath: indexPath)
        cell.configure(viewModal: viewModal, preview: self.presentData?.previews?[indexPath.row])
        return cell
    }
	}
}
//MARK: - DelegateFlowLayout  CollectionView
extension DetailCollection: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    switch true {
      case self.viewModal.indexPathAdvertising.contains(indexPath.row) && !GDPurchacesActions.shared.isPurchaces:
        let width : CGFloat = collectionView.bounds.width - 10
        let height: CGFloat = 320
        return .init(width: width, height: height)
      default:
        let width : CGFloat = (collectionView.bounds.width / 3) - 7
        let height: CGFloat = width + (width / 2)
        return .init(width: width, height: height)
    }
  }
}



