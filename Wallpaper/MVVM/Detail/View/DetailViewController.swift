import UIKit

class DetailViewController: UIViewController {
	
	//MARK: - ViewModel
	public var viewModal: DetailViewModal!
	
	//MARK: - Public variable
	public var detailGetData: DetailGetData!
	
	//MARK: - Outlets
	@IBOutlet weak var titleNavBarLabel    : UILabel!
	@IBOutlet weak var detailCollectionView: UICollectionView!
	@IBOutlet weak var detailCollection    : DetailCollection!
	
	//MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initViewModal()
		self.viewModal.viewDidLoad()
	}
	private func initViewModal(){
		self.viewModal = DetailViewModal(viewController: self)
	}
}
