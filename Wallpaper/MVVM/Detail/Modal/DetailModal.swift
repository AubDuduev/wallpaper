
import Foundation

enum PresentServerDataModal {
	
	case loading
	case getData(String?)
	case errorHandler(DECPreviewData?)
	case presentData(DetailPresentData)
	
	
}
enum PresentLocalDataModal {
	
	case loading
	case getData
	case errorHandler([CDPreview]?)
	case presentData(DetailPresentData)
	
}
struct DetailPresentData {
	
	let previewData: DECPreviewData!
	let previews   : [DECPreview]!
	
	init(previewData: DECPreviewData?, previews: [CDPreview]?){
		self.previewData = previewData
    if previewData == nil {
      self.previews = previews?.compactMap{ DECPreview(preview: $0)}
    } else {
      self.previews = previewData?.data.previews
    }
	}
}

struct DetailGetData {
	
	var title: String! {
		get {
			switch dataType! {
				case .local:
					return "Estimate"
				case .server:
					return wallpaper.title
			}
			
		}
	}
	let wallpaper  : DECWallpaper!
	let dataType   : DetailDataType!
  let detailTitle: DetailTitle!
	
  init?(wallpaper: DECWallpaper?, dataType: DetailDataType, detailTitle: DetailTitle?) {
		self.wallpaper   = wallpaper
		self.dataType    = dataType
    self.detailTitle = detailTitle
	}
}
enum DetailTitle: String {
  
  case live  = "live categories"
  case other
  
  static func title(title: String?) -> DetailTitle? {
    guard let title = title else { return .other }
    return DetailTitle(rawValue: title.lowercased())
  }
  static func category(category: GDCategory?) -> DetailTitle? {
    guard let alias = category?.alias else { return .other }
    switch alias {
      case .categoryLive:
        return .live
      default:
        return .other
    }
  }
}
enum DetailDataType {
	
	case local
	case server
}
