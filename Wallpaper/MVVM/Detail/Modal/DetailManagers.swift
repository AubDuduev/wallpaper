
import Foundation

class DetailManagers: VMManagers {
  
  let setup    : SetupDetail!
  let server   : ServerDetail!
  let present  : PresentDetail!
  let logic    : LogicDetail!
  let animation: AnimationDetail!
  let router   : RouterDetail!
  
  init(viewModal: DetailViewModal) {
    
    self.setup     = SetupDetail(viewModal: viewModal)
    self.server    = ServerDetail(viewModal: viewModal)
    self.present   = PresentDetail(viewModal: viewModal)
    self.logic     = LogicDetail(viewModal: viewModal)
    self.animation = AnimationDetail(viewModal: viewModal)
    self.router    = RouterDetail(viewModal: viewModal)
  }
}

