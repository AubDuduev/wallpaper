
import UIKit

class NavBarViewController: UINavigationController {
	
	//MARK: - ViewModel
	public var viewModal: NavBarViewModal!
	
	//MARK: - Public variable
	public let safeAreaTopView = SafeAreaTopView().loadNib()
	public let navBarView      = NavBarView().loadNib()
	
	//MARK: - Outlets
	
	
	//MARK: - LifeCycle
	override func viewDidLoad() {
		super.viewDidLoad()
		self.initViewModal()
		self.viewModal.viewDidLoad()
	}
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		self.viewModal.viewDidLayoutSubviews()
	}
	private func initViewModal(){
		self.viewModal = NavBarViewModal(navBarViewController: self)
	}
}
