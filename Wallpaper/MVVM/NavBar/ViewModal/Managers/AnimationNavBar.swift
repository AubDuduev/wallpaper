import UIKit

class AnimationNavBar: VMManager {
  
  //MARK: - Public variable
  public var VM: NavBarViewModal!
  
  
}
//MARK: - Initial
extension AnimationNavBar {
  
  //MARK: - Inition
  convenience init(viewModal: NavBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}

