
import UIKit

class SetupNavBar: VMManager {
  
  //MARK: - Public variable
  public var VM: NavBarViewModal!
  
}
//MARK: - Private functions
extension SetupNavBar {
  
  public func setDelegates(){
    //self.tabbarVC.delegate = self.tabbarVC
  }
	public func navBar(){
		self.VM.navbarVC.navigationBar.isHidden = true
	}
  public func safeAreaTopView(){
    self.VM.navbarVC.safeAreaTopView.translatesAutoresizingMaskIntoConstraints = false
		self.VM.navbarVC.safeAreaTopView.topAnchor.constraint(equalTo: self.VM.navbarVC.view.topAnchor).isActive = true
    self.VM.navbarVC.safeAreaTopView.leadingAnchor.constraint(equalTo: self.VM.navbarVC.view.leadingAnchor).isActive = true
    self.VM.navbarVC.safeAreaTopView.trailingAnchor.constraint(equalTo: self.VM.navbarVC.view.trailingAnchor).isActive = true
    self.VM.navbarVC.safeAreaTopView.bottomAnchor.constraint(equalTo: self.VM.navbarVC.navBarView.topAnchor).isActive = true
    self.VM.navbarVC.safeAreaTopView.setup()
  }
  public func setupNavigationBarView(){
    self.VM.navbarVC.navBarView.translatesAutoresizingMaskIntoConstraints = false
    self.VM.navbarVC.navBarView.trailingAnchor.constraint(equalTo: self.VM.navbarVC.view.trailingAnchor).isActive = true
    self.VM.navbarVC.navBarView.leadingAnchor.constraint(equalTo: self.VM.navbarVC.view.leadingAnchor).isActive = true
    self.VM.navbarVC.navBarView.topAnchor.constraint(equalTo: self.VM.navbarVC.view.safeAreaLayoutGuide.topAnchor).isActive = true
    self.VM.navbarVC.navBarView.heightAnchor.constraint(equalToConstant: 45).isActive = true
    self.VM.navbarVC.navBarView.setup()
  }
  public func addedAllView(){
    self.VM.navbarVC.view.addSubview(self.VM.navbarVC.navBarView)
    self.VM.navbarVC.view.addSubview(self.VM.navbarVC.safeAreaTopView)
  }
}
//MARK: - Initial
extension SetupNavBar {
  
  //MARK: - Inition
  convenience init(viewModal: NavBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}


