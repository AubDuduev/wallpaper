
import UIKit

class PresentNavBar: VMManager {
  
  //MARK: - Public variable
  public var VM: NavBarViewModal!
  
  
}
//MARK: - Initial
extension PresentNavBar {
  
  //MARK: - Inition
  convenience init(viewModal: NavBarViewModal) {
    self.init()
    self.VM = viewModal
  }
}

