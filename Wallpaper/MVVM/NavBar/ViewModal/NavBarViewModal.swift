
import Foundation

class NavBarViewModal: VMManagers {
  
  //MARK: - Public variable
  public var managers: NavBarManagers!
  public var navbarVC: NavBarViewController!
  
	public func viewDidLoad(){
		//self.managers.setup.setDelegates()
		//self.managers.setup.addedAllView()
		//self.managers.setup.navBar()
	}
	public func viewDidLayoutSubviews() {
		//self.managers.setup.setupNavigationBarView()
		//self.managers.setup.safeAreaTopView()
	}
}
//MARK: - Initial
extension NavBarViewModal {
  
  convenience init(navBarViewController: NavBarViewController!) {
    self.init()
    self.navbarVC = navBarViewController
    self.managers = NavBarManagers(viewModal: self)
  }
}
