
import Foundation

class NavBarManagers: VMManagers {
  
  let setup    : SetupNavBar!
  let server   : ServerNavBar!
  let present  : PresentNavBar!
  let logic    : LogicNavBar!
  let animation: AnimationNavBar!
  let router   : RouterNavBar!
  
  init(viewModal: NavBarViewModal) {
    
    self.setup     = SetupNavBar(viewModal: viewModal)
    self.server    = ServerNavBar(viewModal: viewModal)
    self.present   = PresentNavBar(viewModal: viewModal)
    self.logic     = LogicNavBar(viewModal: viewModal)
    self.animation = AnimationNavBar(viewModal: viewModal)
    self.router    = RouterNavBar(viewModal: viewModal)
  }
}

