import UIKit

class LoadingViewController: UIViewController {
  
  //MARK: - ViewModel
  public var viewModal: LoadingViewModal!
  
  //MARK: - Public variable
  
  
  //MARK: - Outlets
  @IBOutlet weak var activityView: UIView!
  
  //MARK: - Constraint
  @IBOutlet weak var topLogoConstraint: NSLayoutConstraint!
  
  //MARK: - LifeCycle
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initViewModal()
    self.viewModal.viewDidLoad()
  }
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    self.viewModal.viewDidAppear()
  }
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    self.viewModal.viewDidLayoutSubviews()
  }
  private func initViewModal(){
    self.viewModal = LoadingViewModal(viewController: self)
  }
}
