
import Foundation
import NVActivityIndicatorView

class LoadingViewModal: VMManagers {
  
  //MARK: - Public variable
  public var managers            : LoadingManagers!
  public var VC                  : LoadingViewController!
  public var returnInterstitialVC: ClousureEmpty!
  public var loading             : NVActivityIndicatorView!
  
	public func viewDidAppear() {
    self.managers.logic.logicPushVC()
	}
  public func viewDidLoad() {
    self.managers.setup.setupLoading()
  }
  public func viewDidLayoutSubviews() {
    self.managers.setup.setConstraint()
  }
}
//MARK: - Initial
extension LoadingViewModal {
  
  convenience init(viewController: LoadingViewController) {
    self.init()
    self.VC       = viewController
    self.managers = LoadingManagers(viewModal: self)
  }
}
