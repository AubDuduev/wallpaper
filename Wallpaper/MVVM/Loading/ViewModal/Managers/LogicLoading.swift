import Foundation

class LogicLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  public func logicPushVC(){
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
      guard let self = self else { return }
      if GDPurchacesActions.shared.isPurchaces {
        self.VM.managers.router.push(.NavBarVC)
      } else {
        self.VM.managers.router.push(.WelcomeVC)
      }
    }
  }
}
//MARK: - Initial
extension LogicLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}
