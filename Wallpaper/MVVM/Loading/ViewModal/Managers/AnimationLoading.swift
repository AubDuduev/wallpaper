import UIKit

class AnimationLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  
}
//MARK: - Initial
extension AnimationLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}

