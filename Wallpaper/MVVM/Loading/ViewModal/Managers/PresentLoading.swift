
import UIKit

class PresentLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  
}
//MARK: - Initial
extension PresentLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}

