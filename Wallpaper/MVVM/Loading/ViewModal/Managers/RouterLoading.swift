
import UIKit

class RouterLoading: VMManager {
	
	//MARK: - Public variable
	public var VM: LoadingViewModal!
	
	public func push(_ type: Push){
		
		switch type {
      case .WelcomeVC:
        self.pushWelcomeVC()
      case .InterstitialVC:
        self.pushInterstitialVC()
      case .NavBarVC:
        self.pushNavBarVC()
		}
	}
	
	enum Push {
    case WelcomeVC
    case InterstitialVC
    case NavBarVC
	}
}
//MARK: - Private functions
extension RouterLoading {
	
  private func pushWelcomeVC(){
    let welcomeVC = self.VM.VC.getVCForID(storyboardID     : .Welcome,
                                          vcID             : .WelcomeVC,
                                          transitionStyle  : .crossDissolve,
                                          presentationStyle: .fullScreen) as! WelcomeViewController
    welcomeVC.view.backgroundColor = .black
    self.VM.VC.present(welcomeVC, animated: true)
  }
  private func pushInterstitialVC(){
    let interstitialVC = self.VM.VC.getVCForID(storyboardID     : .Interstitial,
                                               vcID             : .InterstitialVC,
                                               transitionStyle  : .crossDissolve,
                                               presentationStyle: .fullScreen) as! InterstitialViewController
    interstitialVC.modalPresentationStyle = .overCurrentContext
    interstitialVC.modalTransitionStyle   = .flipHorizontal
    interstitialVC.view.backgroundColor   = .clear
    interstitialVC.dissAppearInterstitial = {
      self.VM.managers.logic.logicPushVC()
    }
    self.VM.VC.present(interstitialVC, animated: true)
  }
  private func pushNavBarVC(){
    let navBarVC = self.VM.VC.getVCForID(storyboardID     : .NavBar,
                                         vcID             : .NavBarVC,
                                         transitionStyle  : .crossDissolve,
                                         presentationStyle: .fullScreen) as! NavBarViewController
    UIApplication.shared.windows.first?.rootViewController = navBarVC
    UIApplication.shared.windows.first?.makeKey()
    self.VM.VC.present(navBarVC, animated: true)
  }
}
//MARK: - Initial
extension RouterLoading {
	
	//MARK: - Inition
	convenience init(viewModal: LoadingViewModal) {
		self.init()
		self.VM = viewModal
	}
}



