
import UIKit
import NVActivityIndicatorView
import SnapKit

class SetupLoading: VMManager {
  
  //MARK: - Public variable
  public var VM: LoadingViewModal!
  
  public func setupLoading(){
    let rect = CGRect(x: 0, y: 0, width: 44, height: 44)
    self.VM.loading = NVActivityIndicatorView(frame  : rect,
                                              type   : .ballSpinFadeLoader,
                                              color  : .white,
                                              padding: 0)
    self.VM.VC.activityView.addSubview(self.VM.loading)
    self.VM.loading.snp.makeConstraints { (loading) in
      loading.edges.equalTo(self.VM.VC.activityView)
    }
    
    self.VM.loading.startAnimating()
  }
  public func setConstraint(){
    let loadingConstraints = LoadingConstraints(topLogoConstraint: self.VM.VC.topLogoConstraint)
    let loadingSetConstraint = LoadingSetConstraint(loadingConstraints: loadingConstraints)
    let device = loadingSetConstraint.setConstraint()
    device.setup()
  }
}
//MARK: - Initial
extension SetupLoading {
  
  //MARK: - Inition
  convenience init(viewModal: LoadingViewModal) {
    self.init()
    self.VM = viewModal
  }
}


