
import UIKit
import Foundation

class LoadingConstraints {
  
  public let topLogoConstraint: NSLayoutConstraint!
  
  init(topLogoConstraint: NSLayoutConstraint) {
    self.topLogoConstraint = topLogoConstraint
  }
}
