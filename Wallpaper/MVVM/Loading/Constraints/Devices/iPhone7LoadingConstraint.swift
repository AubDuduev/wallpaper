
import Foundation

class iPhone7LoadingConstraint: Constraintoble {
  
  private let loadingConstraints: LoadingConstraints!
  
  func setup() {
    self.loadingConstraints.topLogoConstraint.constant = 108
  }
  
  init(loadingConstraints: LoadingConstraints) {
    self.loadingConstraints = loadingConstraints
  }
}
