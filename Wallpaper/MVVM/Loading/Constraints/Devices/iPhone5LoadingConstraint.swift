import Foundation

class iPhone5LoadingConstraint: Constraintoble {
  
  private let loadingConstraints: LoadingConstraints!
  
  func setup() {
    self.loadingConstraints.topLogoConstraint.constant = 104
  }
  
  init(loadingConstraints: LoadingConstraints) {
    self.loadingConstraints = loadingConstraints
  }
}
