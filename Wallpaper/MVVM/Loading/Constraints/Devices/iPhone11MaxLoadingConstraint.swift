
import Foundation

class iPhone11MaxLoadingConstraint: Constraintoble {
  
  private let loadingConstraints: LoadingConstraints!
  
  func setup() {
    self.loadingConstraints.topLogoConstraint.constant = 246
  }
  
  init(loadingConstraints: LoadingConstraints) {
    self.loadingConstraints = loadingConstraints
  }
}
