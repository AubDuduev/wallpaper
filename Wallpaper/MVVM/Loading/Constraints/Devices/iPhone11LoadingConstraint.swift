
import Foundation

class iPhone11LoadingConstraint: Constraintoble {
  
  private let loadingConstraints: LoadingConstraints!
  
  func setup() {
    self.loadingConstraints.topLogoConstraint.constant = 225
  }
  
  init(loadingConstraints: LoadingConstraints) {
    self.loadingConstraints = loadingConstraints
  }
}
