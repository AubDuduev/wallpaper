
import UIKit
import Foundation

class LoadingSetConstraint {
  
  
  private let loadingConstraints: LoadingConstraints!
  private var constraint        : Constraintoble!
  
  public func setConstraint() -> Constraintoble {
    switch GDDevice.shared.type() {
      
      //1 - iPhone 5
      case .iPhone5:
        self.constraint = iPhone5LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //2 - iPad 7
      case  .iPad7:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //3 - iPad Air
      case  .iPadAir:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //4 - iPadPro 9Inch
      case  .iPadPro9Inch:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //5 - iPadPro 11Inch
      case .iPadPro11Inch:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //6 - iPadPro 12Inch
      case  .iPadPro12Inch:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //7 - iPad Mini
      case  .iPadMini:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //8 - iPad 2
      case  .iPad2:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //9 - iPhone 7
      case .iPhone7:
        self.constraint = iPhone7LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //10 - iPhone X
      case .iPhoneX:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //11 - iPhone 11 MAX
      case .iPhone11ProMax:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //12 - iPhone Plus
      case .iPhone6Plus:
        self.constraint = iPhone7LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //13 - iPhone 12ProMax
      case .iPhone12ProMax:
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
      //9 - Error
      default:
        print(GDDevice.shared.type().realDevice)
        self.constraint = iPhone11LoadingConstraint(loadingConstraints: self.loadingConstraints)
        return constraint
    }
  }
  
  init(loadingConstraints: LoadingConstraints) {
    self.loadingConstraints = loadingConstraints
  }
}
