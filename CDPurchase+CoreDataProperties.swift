//
//  CDPurchase+CoreDataProperties.swift
//  Wallpaper
//
//  Created by Senior Developer on 01.02.2021.
//
//

import Foundation
import CoreData


extension CDPurchase {
  
  @nonobjc public class func fetchRequest() -> NSFetchRequest<CDPurchase> {
    return NSFetchRequest<CDPurchase>(entityName: "CDPurchase")
  }
  
  @NSManaged public var date: Date?
  @NSManaged public var periood: Int16
  
}
